(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["home-home-module"],{

/***/ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js":
/*!********************************************************************!*\
  !*** ./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js ***!
  \********************************************************************/
/*! exports provided: HTTP_INTERCEPTORS, HttpBackend, HttpClient, HttpClientJsonpModule, HttpClientModule, HttpClientXsrfModule, HttpErrorResponse, HttpEventType, HttpHandler, HttpHeaderResponse, HttpHeaders, HttpParams, HttpRequest, HttpResponse, HttpResponseBase, HttpUrlEncodingCodec, HttpXhrBackend, HttpXsrfTokenExtractor, JsonpClientBackend, JsonpInterceptor, XhrFactory, ɵHttpInterceptingHandler, ɵangular_packages_common_http_http_a, ɵangular_packages_common_http_http_b, ɵangular_packages_common_http_http_c, ɵangular_packages_common_http_http_d, ɵangular_packages_common_http_http_e, ɵangular_packages_common_http_http_f, ɵangular_packages_common_http_http_g, ɵangular_packages_common_http_http_h */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HTTP_INTERCEPTORS", function() { return HTTP_INTERCEPTORS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HttpBackend", function() { return HttpBackend; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HttpClient", function() { return HttpClient; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HttpClientJsonpModule", function() { return HttpClientJsonpModule; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HttpClientModule", function() { return HttpClientModule; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HttpClientXsrfModule", function() { return HttpClientXsrfModule; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HttpErrorResponse", function() { return HttpErrorResponse; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HttpEventType", function() { return HttpEventType; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HttpHandler", function() { return HttpHandler; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HttpHeaderResponse", function() { return HttpHeaderResponse; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HttpHeaders", function() { return HttpHeaders; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HttpParams", function() { return HttpParams; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HttpRequest", function() { return HttpRequest; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HttpResponse", function() { return HttpResponse; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HttpResponseBase", function() { return HttpResponseBase; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HttpUrlEncodingCodec", function() { return HttpUrlEncodingCodec; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HttpXhrBackend", function() { return HttpXhrBackend; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HttpXsrfTokenExtractor", function() { return HttpXsrfTokenExtractor; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "JsonpClientBackend", function() { return JsonpClientBackend; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "JsonpInterceptor", function() { return JsonpInterceptor; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "XhrFactory", function() { return XhrFactory; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ɵHttpInterceptingHandler", function() { return HttpInterceptingHandler; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ɵangular_packages_common_http_http_a", function() { return NoopInterceptor; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ɵangular_packages_common_http_http_b", function() { return JsonpCallbackContext; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ɵangular_packages_common_http_http_c", function() { return jsonpCallbackContext; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ɵangular_packages_common_http_http_d", function() { return BrowserXhr; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ɵangular_packages_common_http_http_e", function() { return XSRF_COOKIE_NAME; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ɵangular_packages_common_http_http_f", function() { return XSRF_HEADER_NAME; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ɵangular_packages_common_http_http_g", function() { return HttpXsrfCookieExtractor; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ɵangular_packages_common_http_http_h", function() { return HttpXsrfInterceptor; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/**
 * @license Angular v10.0.14
 * (c) 2010-2020 Google LLC. https://angular.io/
 * License: MIT
 */






/**
 * @license
 * Copyright Google LLC All Rights Reserved.
 *
 * Use of this source code is governed by an MIT-style license that can be
 * found in the LICENSE file at https://angular.io/license
 */
/**
 * Transforms an `HttpRequest` into a stream of `HttpEvent`s, one of which will likely be a
 * `HttpResponse`.
 *
 * `HttpHandler` is injectable. When injected, the handler instance dispatches requests to the
 * first interceptor in the chain, which dispatches to the second, etc, eventually reaching the
 * `HttpBackend`.
 *
 * In an `HttpInterceptor`, the `HttpHandler` parameter is the next interceptor in the chain.
 *
 * @publicApi
 */

class HttpHandler {
}
/**
 * A final `HttpHandler` which will dispatch the request via browser HTTP APIs to a backend.
 *
 * Interceptors sit between the `HttpClient` interface and the `HttpBackend`.
 *
 * When injected, `HttpBackend` dispatches requests directly to the backend, without going
 * through the interceptor chain.
 *
 * @publicApi
 */
class HttpBackend {
}

/**
 * @license
 * Copyright Google LLC All Rights Reserved.
 *
 * Use of this source code is governed by an MIT-style license that can be
 * found in the LICENSE file at https://angular.io/license
 */
/**
 * Represents the header configuration options for an HTTP request.
 * Instances are immutable. Modifying methods return a cloned
 * instance with the change. The original object is never changed.
 *
 * @publicApi
 */
class HttpHeaders {
    /**  Constructs a new HTTP header object with the given values.*/
    constructor(headers) {
        /**
         * Internal map of lowercased header names to the normalized
         * form of the name (the form seen first).
         */
        this.normalizedNames = new Map();
        /**
         * Queued updates to be materialized the next initialization.
         */
        this.lazyUpdate = null;
        if (!headers) {
            this.headers = new Map();
        }
        else if (typeof headers === 'string') {
            this.lazyInit = () => {
                this.headers = new Map();
                headers.split('\n').forEach(line => {
                    const index = line.indexOf(':');
                    if (index > 0) {
                        const name = line.slice(0, index);
                        const key = name.toLowerCase();
                        const value = line.slice(index + 1).trim();
                        this.maybeSetNormalizedName(name, key);
                        if (this.headers.has(key)) {
                            this.headers.get(key).push(value);
                        }
                        else {
                            this.headers.set(key, [value]);
                        }
                    }
                });
            };
        }
        else {
            this.lazyInit = () => {
                this.headers = new Map();
                Object.keys(headers).forEach(name => {
                    let values = headers[name];
                    const key = name.toLowerCase();
                    if (typeof values === 'string') {
                        values = [values];
                    }
                    if (values.length > 0) {
                        this.headers.set(key, values);
                        this.maybeSetNormalizedName(name, key);
                    }
                });
            };
        }
    }
    /**
     * Checks for existence of a given header.
     *
     * @param name The header name to check for existence.
     *
     * @returns True if the header exists, false otherwise.
     */
    has(name) {
        this.init();
        return this.headers.has(name.toLowerCase());
    }
    /**
     * Retrieves the first value of a given header.
     *
     * @param name The header name.
     *
     * @returns The value string if the header exists, null otherwise
     */
    get(name) {
        this.init();
        const values = this.headers.get(name.toLowerCase());
        return values && values.length > 0 ? values[0] : null;
    }
    /**
     * Retrieves the names of the headers.
     *
     * @returns A list of header names.
     */
    keys() {
        this.init();
        return Array.from(this.normalizedNames.values());
    }
    /**
     * Retrieves a list of values for a given header.
     *
     * @param name The header name from which to retrieve values.
     *
     * @returns A string of values if the header exists, null otherwise.
     */
    getAll(name) {
        this.init();
        return this.headers.get(name.toLowerCase()) || null;
    }
    /**
     * Appends a new value to the existing set of values for a header
     * and returns them in a clone of the original instance.
     *
     * @param name The header name for which to append the values.
     * @param value The value to append.
     *
     * @returns A clone of the HTTP headers object with the value appended to the given header.
     */
    append(name, value) {
        return this.clone({ name, value, op: 'a' });
    }
    /**
     * Sets or modifies a value for a given header in a clone of the original instance.
     * If the header already exists, its value is replaced with the given value
     * in the returned object.
     *
     * @param name The header name.
     * @param value The value or values to set or overide for the given header.
     *
     * @returns A clone of the HTTP headers object with the newly set header value.
     */
    set(name, value) {
        return this.clone({ name, value, op: 's' });
    }
    /**
     * Deletes values for a given header in a clone of the original instance.
     *
     * @param name The header name.
     * @param value The value or values to delete for the given header.
     *
     * @returns A clone of the HTTP headers object with the given value deleted.
     */
    delete(name, value) {
        return this.clone({ name, value, op: 'd' });
    }
    maybeSetNormalizedName(name, lcName) {
        if (!this.normalizedNames.has(lcName)) {
            this.normalizedNames.set(lcName, name);
        }
    }
    init() {
        if (!!this.lazyInit) {
            if (this.lazyInit instanceof HttpHeaders) {
                this.copyFrom(this.lazyInit);
            }
            else {
                this.lazyInit();
            }
            this.lazyInit = null;
            if (!!this.lazyUpdate) {
                this.lazyUpdate.forEach(update => this.applyUpdate(update));
                this.lazyUpdate = null;
            }
        }
    }
    copyFrom(other) {
        other.init();
        Array.from(other.headers.keys()).forEach(key => {
            this.headers.set(key, other.headers.get(key));
            this.normalizedNames.set(key, other.normalizedNames.get(key));
        });
    }
    clone(update) {
        const clone = new HttpHeaders();
        clone.lazyInit =
            (!!this.lazyInit && this.lazyInit instanceof HttpHeaders) ? this.lazyInit : this;
        clone.lazyUpdate = (this.lazyUpdate || []).concat([update]);
        return clone;
    }
    applyUpdate(update) {
        const key = update.name.toLowerCase();
        switch (update.op) {
            case 'a':
            case 's':
                let value = update.value;
                if (typeof value === 'string') {
                    value = [value];
                }
                if (value.length === 0) {
                    return;
                }
                this.maybeSetNormalizedName(update.name, key);
                const base = (update.op === 'a' ? this.headers.get(key) : undefined) || [];
                base.push(...value);
                this.headers.set(key, base);
                break;
            case 'd':
                const toDelete = update.value;
                if (!toDelete) {
                    this.headers.delete(key);
                    this.normalizedNames.delete(key);
                }
                else {
                    let existing = this.headers.get(key);
                    if (!existing) {
                        return;
                    }
                    existing = existing.filter(value => toDelete.indexOf(value) === -1);
                    if (existing.length === 0) {
                        this.headers.delete(key);
                        this.normalizedNames.delete(key);
                    }
                    else {
                        this.headers.set(key, existing);
                    }
                }
                break;
        }
    }
    /**
     * @internal
     */
    forEach(fn) {
        this.init();
        Array.from(this.normalizedNames.keys())
            .forEach(key => fn(this.normalizedNames.get(key), this.headers.get(key)));
    }
}

/**
 * @license
 * Copyright Google LLC All Rights Reserved.
 *
 * Use of this source code is governed by an MIT-style license that can be
 * found in the LICENSE file at https://angular.io/license
 */
/**
 * Provides encoding and decoding of URL parameter and query-string values.
 *
 * Serializes and parses URL parameter keys and values to encode and decode them.
 * If you pass URL query parameters without encoding,
 * the query parameters can be misinterpreted at the receiving end.
 *
 *
 * @publicApi
 */
class HttpUrlEncodingCodec {
    /**
     * Encodes a key name for a URL parameter or query-string.
     * @param key The key name.
     * @returns The encoded key name.
     */
    encodeKey(key) {
        return standardEncoding(key);
    }
    /**
     * Encodes the value of a URL parameter or query-string.
     * @param value The value.
     * @returns The encoded value.
     */
    encodeValue(value) {
        return standardEncoding(value);
    }
    /**
     * Decodes an encoded URL parameter or query-string key.
     * @param key The encoded key name.
     * @returns The decoded key name.
     */
    decodeKey(key) {
        return decodeURIComponent(key);
    }
    /**
     * Decodes an encoded URL parameter or query-string value.
     * @param value The encoded value.
     * @returns The decoded value.
     */
    decodeValue(value) {
        return decodeURIComponent(value);
    }
}
function paramParser(rawParams, codec) {
    const map = new Map();
    if (rawParams.length > 0) {
        const params = rawParams.split('&');
        params.forEach((param) => {
            const eqIdx = param.indexOf('=');
            const [key, val] = eqIdx == -1 ?
                [codec.decodeKey(param), ''] :
                [codec.decodeKey(param.slice(0, eqIdx)), codec.decodeValue(param.slice(eqIdx + 1))];
            const list = map.get(key) || [];
            list.push(val);
            map.set(key, list);
        });
    }
    return map;
}
function standardEncoding(v) {
    return encodeURIComponent(v)
        .replace(/%40/gi, '@')
        .replace(/%3A/gi, ':')
        .replace(/%24/gi, '$')
        .replace(/%2C/gi, ',')
        .replace(/%3B/gi, ';')
        .replace(/%2B/gi, '+')
        .replace(/%3D/gi, '=')
        .replace(/%3F/gi, '?')
        .replace(/%2F/gi, '/');
}
/**
 * An HTTP request/response body that represents serialized parameters,
 * per the MIME type `application/x-www-form-urlencoded`.
 *
 * This class is immutable; all mutation operations return a new instance.
 *
 * @publicApi
 */
class HttpParams {
    constructor(options = {}) {
        this.updates = null;
        this.cloneFrom = null;
        this.encoder = options.encoder || new HttpUrlEncodingCodec();
        if (!!options.fromString) {
            if (!!options.fromObject) {
                throw new Error(`Cannot specify both fromString and fromObject.`);
            }
            this.map = paramParser(options.fromString, this.encoder);
        }
        else if (!!options.fromObject) {
            this.map = new Map();
            Object.keys(options.fromObject).forEach(key => {
                const value = options.fromObject[key];
                this.map.set(key, Array.isArray(value) ? value : [value]);
            });
        }
        else {
            this.map = null;
        }
    }
    /**
     * Reports whether the body includes one or more values for a given parameter.
     * @param param The parameter name.
     * @returns True if the parameter has one or more values,
     * false if it has no value or is not present.
     */
    has(param) {
        this.init();
        return this.map.has(param);
    }
    /**
     * Retrieves the first value for a parameter.
     * @param param The parameter name.
     * @returns The first value of the given parameter,
     * or `null` if the parameter is not present.
     */
    get(param) {
        this.init();
        const res = this.map.get(param);
        return !!res ? res[0] : null;
    }
    /**
     * Retrieves all values for a  parameter.
     * @param param The parameter name.
     * @returns All values in a string array,
     * or `null` if the parameter not present.
     */
    getAll(param) {
        this.init();
        return this.map.get(param) || null;
    }
    /**
     * Retrieves all the parameters for this body.
     * @returns The parameter names in a string array.
     */
    keys() {
        this.init();
        return Array.from(this.map.keys());
    }
    /**
     * Appends a new value to existing values for a parameter.
     * @param param The parameter name.
     * @param value The new value to add.
     * @return A new body with the appended value.
     */
    append(param, value) {
        return this.clone({ param, value, op: 'a' });
    }
    /**
     * Replaces the value for a parameter.
     * @param param The parameter name.
     * @param value The new value.
     * @return A new body with the new value.
     */
    set(param, value) {
        return this.clone({ param, value, op: 's' });
    }
    /**
     * Removes a given value or all values from a parameter.
     * @param param The parameter name.
     * @param value The value to remove, if provided.
     * @return A new body with the given value removed, or with all values
     * removed if no value is specified.
     */
    delete(param, value) {
        return this.clone({ param, value, op: 'd' });
    }
    /**
     * Serializes the body to an encoded string, where key-value pairs (separated by `=`) are
     * separated by `&`s.
     */
    toString() {
        this.init();
        return this.keys()
            .map(key => {
            const eKey = this.encoder.encodeKey(key);
            // `a: ['1']` produces `'a=1'`
            // `b: []` produces `''`
            // `c: ['1', '2']` produces `'c=1&c=2'`
            return this.map.get(key).map(value => eKey + '=' + this.encoder.encodeValue(value))
                .join('&');
        })
            // filter out empty values because `b: []` produces `''`
            // which results in `a=1&&c=1&c=2` instead of `a=1&c=1&c=2` if we don't
            .filter(param => param !== '')
            .join('&');
    }
    clone(update) {
        const clone = new HttpParams({ encoder: this.encoder });
        clone.cloneFrom = this.cloneFrom || this;
        clone.updates = (this.updates || []).concat([update]);
        return clone;
    }
    init() {
        if (this.map === null) {
            this.map = new Map();
        }
        if (this.cloneFrom !== null) {
            this.cloneFrom.init();
            this.cloneFrom.keys().forEach(key => this.map.set(key, this.cloneFrom.map.get(key)));
            this.updates.forEach(update => {
                switch (update.op) {
                    case 'a':
                    case 's':
                        const base = (update.op === 'a' ? this.map.get(update.param) : undefined) || [];
                        base.push(update.value);
                        this.map.set(update.param, base);
                        break;
                    case 'd':
                        if (update.value !== undefined) {
                            let base = this.map.get(update.param) || [];
                            const idx = base.indexOf(update.value);
                            if (idx !== -1) {
                                base.splice(idx, 1);
                            }
                            if (base.length > 0) {
                                this.map.set(update.param, base);
                            }
                            else {
                                this.map.delete(update.param);
                            }
                        }
                        else {
                            this.map.delete(update.param);
                            break;
                        }
                }
            });
            this.cloneFrom = this.updates = null;
        }
    }
}

/**
 * @license
 * Copyright Google LLC All Rights Reserved.
 *
 * Use of this source code is governed by an MIT-style license that can be
 * found in the LICENSE file at https://angular.io/license
 */
/**
 * Determine whether the given HTTP method may include a body.
 */
function mightHaveBody(method) {
    switch (method) {
        case 'DELETE':
        case 'GET':
        case 'HEAD':
        case 'OPTIONS':
        case 'JSONP':
            return false;
        default:
            return true;
    }
}
/**
 * Safely assert whether the given value is an ArrayBuffer.
 *
 * In some execution environments ArrayBuffer is not defined.
 */
function isArrayBuffer(value) {
    return typeof ArrayBuffer !== 'undefined' && value instanceof ArrayBuffer;
}
/**
 * Safely assert whether the given value is a Blob.
 *
 * In some execution environments Blob is not defined.
 */
function isBlob(value) {
    return typeof Blob !== 'undefined' && value instanceof Blob;
}
/**
 * Safely assert whether the given value is a FormData instance.
 *
 * In some execution environments FormData is not defined.
 */
function isFormData(value) {
    return typeof FormData !== 'undefined' && value instanceof FormData;
}
/**
 * An outgoing HTTP request with an optional typed body.
 *
 * `HttpRequest` represents an outgoing request, including URL, method,
 * headers, body, and other request configuration options. Instances should be
 * assumed to be immutable. To modify a `HttpRequest`, the `clone`
 * method should be used.
 *
 * @publicApi
 */
class HttpRequest {
    constructor(method, url, third, fourth) {
        this.url = url;
        /**
         * The request body, or `null` if one isn't set.
         *
         * Bodies are not enforced to be immutable, as they can include a reference to any
         * user-defined data type. However, interceptors should take care to preserve
         * idempotence by treating them as such.
         */
        this.body = null;
        /**
         * Whether this request should be made in a way that exposes progress events.
         *
         * Progress events are expensive (change detection runs on each event) and so
         * they should only be requested if the consumer intends to monitor them.
         */
        this.reportProgress = false;
        /**
         * Whether this request should be sent with outgoing credentials (cookies).
         */
        this.withCredentials = false;
        /**
         * The expected response type of the server.
         *
         * This is used to parse the response appropriately before returning it to
         * the requestee.
         */
        this.responseType = 'json';
        this.method = method.toUpperCase();
        // Next, need to figure out which argument holds the HttpRequestInit
        // options, if any.
        let options;
        // Check whether a body argument is expected. The only valid way to omit
        // the body argument is to use a known no-body method like GET.
        if (mightHaveBody(this.method) || !!fourth) {
            // Body is the third argument, options are the fourth.
            this.body = (third !== undefined) ? third : null;
            options = fourth;
        }
        else {
            // No body required, options are the third argument. The body stays null.
            options = third;
        }
        // If options have been passed, interpret them.
        if (options) {
            // Normalize reportProgress and withCredentials.
            this.reportProgress = !!options.reportProgress;
            this.withCredentials = !!options.withCredentials;
            // Override default response type of 'json' if one is provided.
            if (!!options.responseType) {
                this.responseType = options.responseType;
            }
            // Override headers if they're provided.
            if (!!options.headers) {
                this.headers = options.headers;
            }
            if (!!options.params) {
                this.params = options.params;
            }
        }
        // If no headers have been passed in, construct a new HttpHeaders instance.
        if (!this.headers) {
            this.headers = new HttpHeaders();
        }
        // If no parameters have been passed in, construct a new HttpUrlEncodedParams instance.
        if (!this.params) {
            this.params = new HttpParams();
            this.urlWithParams = url;
        }
        else {
            // Encode the parameters to a string in preparation for inclusion in the URL.
            const params = this.params.toString();
            if (params.length === 0) {
                // No parameters, the visible URL is just the URL given at creation time.
                this.urlWithParams = url;
            }
            else {
                // Does the URL already have query parameters? Look for '?'.
                const qIdx = url.indexOf('?');
                // There are 3 cases to handle:
                // 1) No existing parameters -> append '?' followed by params.
                // 2) '?' exists and is followed by existing query string ->
                //    append '&' followed by params.
                // 3) '?' exists at the end of the url -> append params directly.
                // This basically amounts to determining the character, if any, with
                // which to join the URL and parameters.
                const sep = qIdx === -1 ? '?' : (qIdx < url.length - 1 ? '&' : '');
                this.urlWithParams = url + sep + params;
            }
        }
    }
    /**
     * Transform the free-form body into a serialized format suitable for
     * transmission to the server.
     */
    serializeBody() {
        // If no body is present, no need to serialize it.
        if (this.body === null) {
            return null;
        }
        // Check whether the body is already in a serialized form. If so,
        // it can just be returned directly.
        if (isArrayBuffer(this.body) || isBlob(this.body) || isFormData(this.body) ||
            typeof this.body === 'string') {
            return this.body;
        }
        // Check whether the body is an instance of HttpUrlEncodedParams.
        if (this.body instanceof HttpParams) {
            return this.body.toString();
        }
        // Check whether the body is an object or array, and serialize with JSON if so.
        if (typeof this.body === 'object' || typeof this.body === 'boolean' ||
            Array.isArray(this.body)) {
            return JSON.stringify(this.body);
        }
        // Fall back on toString() for everything else.
        return this.body.toString();
    }
    /**
     * Examine the body and attempt to infer an appropriate MIME type
     * for it.
     *
     * If no such type can be inferred, this method will return `null`.
     */
    detectContentTypeHeader() {
        // An empty body has no content type.
        if (this.body === null) {
            return null;
        }
        // FormData bodies rely on the browser's content type assignment.
        if (isFormData(this.body)) {
            return null;
        }
        // Blobs usually have their own content type. If it doesn't, then
        // no type can be inferred.
        if (isBlob(this.body)) {
            return this.body.type || null;
        }
        // Array buffers have unknown contents and thus no type can be inferred.
        if (isArrayBuffer(this.body)) {
            return null;
        }
        // Technically, strings could be a form of JSON data, but it's safe enough
        // to assume they're plain strings.
        if (typeof this.body === 'string') {
            return 'text/plain';
        }
        // `HttpUrlEncodedParams` has its own content-type.
        if (this.body instanceof HttpParams) {
            return 'application/x-www-form-urlencoded;charset=UTF-8';
        }
        // Arrays, objects, and numbers will be encoded as JSON.
        if (typeof this.body === 'object' || typeof this.body === 'number' ||
            Array.isArray(this.body)) {
            return 'application/json';
        }
        // No type could be inferred.
        return null;
    }
    clone(update = {}) {
        // For method, url, and responseType, take the current value unless
        // it is overridden in the update hash.
        const method = update.method || this.method;
        const url = update.url || this.url;
        const responseType = update.responseType || this.responseType;
        // The body is somewhat special - a `null` value in update.body means
        // whatever current body is present is being overridden with an empty
        // body, whereas an `undefined` value in update.body implies no
        // override.
        const body = (update.body !== undefined) ? update.body : this.body;
        // Carefully handle the boolean options to differentiate between
        // `false` and `undefined` in the update args.
        const withCredentials = (update.withCredentials !== undefined) ? update.withCredentials : this.withCredentials;
        const reportProgress = (update.reportProgress !== undefined) ? update.reportProgress : this.reportProgress;
        // Headers and params may be appended to if `setHeaders` or
        // `setParams` are used.
        let headers = update.headers || this.headers;
        let params = update.params || this.params;
        // Check whether the caller has asked to add headers.
        if (update.setHeaders !== undefined) {
            // Set every requested header.
            headers =
                Object.keys(update.setHeaders)
                    .reduce((headers, name) => headers.set(name, update.setHeaders[name]), headers);
        }
        // Check whether the caller has asked to set params.
        if (update.setParams) {
            // Set every requested param.
            params = Object.keys(update.setParams)
                .reduce((params, param) => params.set(param, update.setParams[param]), params);
        }
        // Finally, construct the new HttpRequest using the pieces from above.
        return new HttpRequest(method, url, body, {
            params,
            headers,
            reportProgress,
            responseType,
            withCredentials,
        });
    }
}

/**
 * @license
 * Copyright Google LLC All Rights Reserved.
 *
 * Use of this source code is governed by an MIT-style license that can be
 * found in the LICENSE file at https://angular.io/license
 */
/**
 * Type enumeration for the different kinds of `HttpEvent`.
 *
 * @publicApi
 */
var HttpEventType;
(function (HttpEventType) {
    /**
     * The request was sent out over the wire.
     */
    HttpEventType[HttpEventType["Sent"] = 0] = "Sent";
    /**
     * An upload progress event was received.
     */
    HttpEventType[HttpEventType["UploadProgress"] = 1] = "UploadProgress";
    /**
     * The response status code and headers were received.
     */
    HttpEventType[HttpEventType["ResponseHeader"] = 2] = "ResponseHeader";
    /**
     * A download progress event was received.
     */
    HttpEventType[HttpEventType["DownloadProgress"] = 3] = "DownloadProgress";
    /**
     * The full response including the body was received.
     */
    HttpEventType[HttpEventType["Response"] = 4] = "Response";
    /**
     * A custom event from an interceptor or a backend.
     */
    HttpEventType[HttpEventType["User"] = 5] = "User";
})(HttpEventType || (HttpEventType = {}));
/**
 * Base class for both `HttpResponse` and `HttpHeaderResponse`.
 *
 * @publicApi
 */
class HttpResponseBase {
    /**
     * Super-constructor for all responses.
     *
     * The single parameter accepted is an initialization hash. Any properties
     * of the response passed there will override the default values.
     */
    constructor(init, defaultStatus = 200, defaultStatusText = 'OK') {
        // If the hash has values passed, use them to initialize the response.
        // Otherwise use the default values.
        this.headers = init.headers || new HttpHeaders();
        this.status = init.status !== undefined ? init.status : defaultStatus;
        this.statusText = init.statusText || defaultStatusText;
        this.url = init.url || null;
        // Cache the ok value to avoid defining a getter.
        this.ok = this.status >= 200 && this.status < 300;
    }
}
/**
 * A partial HTTP response which only includes the status and header data,
 * but no response body.
 *
 * `HttpHeaderResponse` is a `HttpEvent` available on the response
 * event stream, only when progress events are requested.
 *
 * @publicApi
 */
class HttpHeaderResponse extends HttpResponseBase {
    /**
     * Create a new `HttpHeaderResponse` with the given parameters.
     */
    constructor(init = {}) {
        super(init);
        this.type = HttpEventType.ResponseHeader;
    }
    /**
     * Copy this `HttpHeaderResponse`, overriding its contents with the
     * given parameter hash.
     */
    clone(update = {}) {
        // Perform a straightforward initialization of the new HttpHeaderResponse,
        // overriding the current parameters with new ones if given.
        return new HttpHeaderResponse({
            headers: update.headers || this.headers,
            status: update.status !== undefined ? update.status : this.status,
            statusText: update.statusText || this.statusText,
            url: update.url || this.url || undefined,
        });
    }
}
/**
 * A full HTTP response, including a typed response body (which may be `null`
 * if one was not returned).
 *
 * `HttpResponse` is a `HttpEvent` available on the response event
 * stream.
 *
 * @publicApi
 */
class HttpResponse extends HttpResponseBase {
    /**
     * Construct a new `HttpResponse`.
     */
    constructor(init = {}) {
        super(init);
        this.type = HttpEventType.Response;
        this.body = init.body !== undefined ? init.body : null;
    }
    clone(update = {}) {
        return new HttpResponse({
            body: (update.body !== undefined) ? update.body : this.body,
            headers: update.headers || this.headers,
            status: (update.status !== undefined) ? update.status : this.status,
            statusText: update.statusText || this.statusText,
            url: update.url || this.url || undefined,
        });
    }
}
/**
 * A response that represents an error or failure, either from a
 * non-successful HTTP status, an error while executing the request,
 * or some other failure which occurred during the parsing of the response.
 *
 * Any error returned on the `Observable` response stream will be
 * wrapped in an `HttpErrorResponse` to provide additional context about
 * the state of the HTTP layer when the error occurred. The error property
 * will contain either a wrapped Error object or the error response returned
 * from the server.
 *
 * @publicApi
 */
class HttpErrorResponse extends HttpResponseBase {
    constructor(init) {
        // Initialize with a default status of 0 / Unknown Error.
        super(init, 0, 'Unknown Error');
        this.name = 'HttpErrorResponse';
        /**
         * Errors are never okay, even when the status code is in the 2xx success range.
         */
        this.ok = false;
        // If the response was successful, then this was a parse error. Otherwise, it was
        // a protocol-level failure of some sort. Either the request failed in transit
        // or the server returned an unsuccessful status code.
        if (this.status >= 200 && this.status < 300) {
            this.message = `Http failure during parsing for ${init.url || '(unknown url)'}`;
        }
        else {
            this.message = `Http failure response for ${init.url || '(unknown url)'}: ${init.status} ${init.statusText}`;
        }
        this.error = init.error || null;
    }
}

/**
 * @license
 * Copyright Google LLC All Rights Reserved.
 *
 * Use of this source code is governed by an MIT-style license that can be
 * found in the LICENSE file at https://angular.io/license
 */
/**
 * Constructs an instance of `HttpRequestOptions<T>` from a source `HttpMethodOptions` and
 * the given `body`. This function clones the object and adds the body.
 *
 * Note that the `responseType` *options* value is a String that identifies the
 * single data type of the response.
 * A single overload version of the method handles each response type.
 * The value of `responseType` cannot be a union, as the combined signature could imply.
 *
 */
function addBody(options, body) {
    return {
        body,
        headers: options.headers,
        observe: options.observe,
        params: options.params,
        reportProgress: options.reportProgress,
        responseType: options.responseType,
        withCredentials: options.withCredentials,
    };
}
/**
 * Performs HTTP requests.
 * This service is available as an injectable class, with methods to perform HTTP requests.
 * Each request method has multiple signatures, and the return type varies based on
 * the signature that is called (mainly the values of `observe` and `responseType`).
 *
 * Note that the `responseType` *options* value is a String that identifies the
 * single data type of the response.
 * A single overload version of the method handles each response type.
 * The value of `responseType` cannot be a union, as the combined signature could imply.

 *
 * @usageNotes
 * Sample HTTP requests for the [Tour of Heroes](/tutorial/toh-pt0) application.
 *
 * ### HTTP Request Example
 *
 * ```
 *  // GET heroes whose name contains search term
 * searchHeroes(term: string): observable<Hero[]>{
 *
 *  const params = new HttpParams({fromString: 'name=term'});
 *    return this.httpClient.request('GET', this.heroesUrl, {responseType:'json', params});
 * }
 * ```
 * ### JSONP Example
 * ```
 * requestJsonp(url, callback = 'callback') {
 *  return this.httpClient.jsonp(this.heroesURL, callback);
 * }
 * ```
 *
 * ### PATCH Example
 * ```
 * // PATCH one of the heroes' name
 * patchHero (id: number, heroName: string): Observable<{}> {
 * const url = `${this.heroesUrl}/${id}`;   // PATCH api/heroes/42
 *  return this.httpClient.patch(url, {name: heroName}, httpOptions)
 *    .pipe(catchError(this.handleError('patchHero')));
 * }
 * ```
 *
 * @see [HTTP Guide](guide/http)
 *
 * @publicApi
 */
class HttpClient {
    constructor(handler) {
        this.handler = handler;
    }
    /**
     * Constructs an observable for a generic HTTP request that, when subscribed,
     * fires the request through the chain of registered interceptors and on to the
     * server.
     *
     * You can pass an `HttpRequest` directly as the only parameter. In this case,
     * the call returns an observable of the raw `HttpEvent` stream.
     *
     * Alternatively you can pass an HTTP method as the first parameter,
     * a URL string as the second, and an options hash containing the request body as the third.
     * See `addBody()`. In this case, the specified `responseType` and `observe` options determine the
     * type of returned observable.
     *   * The `responseType` value determines how a successful response body is parsed.
     *   * If `responseType` is the default `json`, you can pass a type interface for the resulting
     * object as a type parameter to the call.
     *
     * The `observe` value determines the return type, according to what you are interested in
     * observing.
     *   * An `observe` value of events returns an observable of the raw `HttpEvent` stream, including
     * progress events by default.
     *   * An `observe` value of response returns an observable of `HttpResponse<T>`,
     * where the `T` parameter depends on the `responseType` and any optionally provided type
     * parameter.
     *   * An `observe` value of body returns an observable of `<T>` with the same `T` body type.
     *
     */
    request(first, url, options = {}) {
        let req;
        // First, check whether the primary argument is an instance of `HttpRequest`.
        if (first instanceof HttpRequest) {
            // It is. The other arguments must be undefined (per the signatures) and can be
            // ignored.
            req = first;
        }
        else {
            // It's a string, so it represents a URL. Construct a request based on it,
            // and incorporate the remaining arguments (assuming `GET` unless a method is
            // provided.
            // Figure out the headers.
            let headers = undefined;
            if (options.headers instanceof HttpHeaders) {
                headers = options.headers;
            }
            else {
                headers = new HttpHeaders(options.headers);
            }
            // Sort out parameters.
            let params = undefined;
            if (!!options.params) {
                if (options.params instanceof HttpParams) {
                    params = options.params;
                }
                else {
                    params = new HttpParams({ fromObject: options.params });
                }
            }
            // Construct the request.
            req = new HttpRequest(first, url, (options.body !== undefined ? options.body : null), {
                headers,
                params,
                reportProgress: options.reportProgress,
                // By default, JSON is assumed to be returned for all calls.
                responseType: options.responseType || 'json',
                withCredentials: options.withCredentials,
            });
        }
        // Start with an Observable.of() the initial request, and run the handler (which
        // includes all interceptors) inside a concatMap(). This way, the handler runs
        // inside an Observable chain, which causes interceptors to be re-run on every
        // subscription (this also makes retries re-run the handler, including interceptors).
        const events$ = Object(rxjs__WEBPACK_IMPORTED_MODULE_1__["of"])(req).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["concatMap"])((req) => this.handler.handle(req)));
        // If coming via the API signature which accepts a previously constructed HttpRequest,
        // the only option is to get the event stream. Otherwise, return the event stream if
        // that is what was requested.
        if (first instanceof HttpRequest || options.observe === 'events') {
            return events$;
        }
        // The requested stream contains either the full response or the body. In either
        // case, the first step is to filter the event stream to extract a stream of
        // responses(s).
        const res$ = events$.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["filter"])((event) => event instanceof HttpResponse));
        // Decide which stream to return.
        switch (options.observe || 'body') {
            case 'body':
                // The requested stream is the body. Map the response stream to the response
                // body. This could be done more simply, but a misbehaving interceptor might
                // transform the response body into a different format and ignore the requested
                // responseType. Guard against this by validating that the response is of the
                // requested type.
                switch (req.responseType) {
                    case 'arraybuffer':
                        return res$.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])((res) => {
                            // Validate that the body is an ArrayBuffer.
                            if (res.body !== null && !(res.body instanceof ArrayBuffer)) {
                                throw new Error('Response is not an ArrayBuffer.');
                            }
                            return res.body;
                        }));
                    case 'blob':
                        return res$.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])((res) => {
                            // Validate that the body is a Blob.
                            if (res.body !== null && !(res.body instanceof Blob)) {
                                throw new Error('Response is not a Blob.');
                            }
                            return res.body;
                        }));
                    case 'text':
                        return res$.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])((res) => {
                            // Validate that the body is a string.
                            if (res.body !== null && typeof res.body !== 'string') {
                                throw new Error('Response is not a string.');
                            }
                            return res.body;
                        }));
                    case 'json':
                    default:
                        // No validation needed for JSON responses, as they can be of any type.
                        return res$.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])((res) => res.body));
                }
            case 'response':
                // The response stream was requested directly, so return it.
                return res$;
            default:
                // Guard against new future observe types being added.
                throw new Error(`Unreachable: unhandled observe type ${options.observe}}`);
        }
    }
    /**
     * Constructs an observable that, when subscribed, causes the configured
     * `DELETE` request to execute on the server. See the individual overloads for
     * details on the return type.
     *
     * @param url     The endpoint URL.
     * @param options The HTTP options to send with the request.
     *
     */
    delete(url, options = {}) {
        return this.request('DELETE', url, options);
    }
    /**
     * Constructs an observable that, when subscribed, causes the configured
     * `GET` request to execute on the server. See the individual overloads for
     * details on the return type.
     */
    get(url, options = {}) {
        return this.request('GET', url, options);
    }
    /**
     * Constructs an observable that, when subscribed, causes the configured
     * `HEAD` request to execute on the server. The `HEAD` method returns
     * meta information about the resource without transferring the
     * resource itself. See the individual overloads for
     * details on the return type.
     */
    head(url, options = {}) {
        return this.request('HEAD', url, options);
    }
    /**
     * Constructs an `Observable` that, when subscribed, causes a request with the special method
     * `JSONP` to be dispatched via the interceptor pipeline.
     * The [JSONP pattern](https://en.wikipedia.org/wiki/JSONP) works around limitations of certain
     * API endpoints that don't support newer,
     * and preferable [CORS](https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS) protocol.
     * JSONP treats the endpoint API as a JavaScript file and tricks the browser to process the
     * requests even if the API endpoint is not located on the same domain (origin) as the client-side
     * application making the request.
     * The endpoint API must support JSONP callback for JSONP requests to work.
     * The resource API returns the JSON response wrapped in a callback function.
     * You can pass the callback function name as one of the query parameters.
     * Note that JSONP requests can only be used with `GET` requests.
     *
     * @param url The resource URL.
     * @param callbackParam The callback function name.
     *
     */
    jsonp(url, callbackParam) {
        return this.request('JSONP', url, {
            params: new HttpParams().append(callbackParam, 'JSONP_CALLBACK'),
            observe: 'body',
            responseType: 'json',
        });
    }
    /**
     * Constructs an `Observable` that, when subscribed, causes the configured
     * `OPTIONS` request to execute on the server. This method allows the client
     * to determine the supported HTTP methods and other capabilites of an endpoint,
     * without implying a resource action. See the individual overloads for
     * details on the return type.
     */
    options(url, options = {}) {
        return this.request('OPTIONS', url, options);
    }
    /**
     * Constructs an observable that, when subscribed, causes the configured
     * `PATCH` request to execute on the server. See the individual overloads for
     * details on the return type.
     */
    patch(url, body, options = {}) {
        return this.request('PATCH', url, addBody(options, body));
    }
    /**
     * Constructs an observable that, when subscribed, causes the configured
     * `POST` request to execute on the server. The server responds with the location of
     * the replaced resource. See the individual overloads for
     * details on the return type.
     */
    post(url, body, options = {}) {
        return this.request('POST', url, addBody(options, body));
    }
    /**
     * Constructs an observable that, when subscribed, causes the configured
     * `PUT` request to execute on the server. The `PUT` method replaces an existing resource
     * with a new set of values.
     * See the individual overloads for details on the return type.
     */
    put(url, body, options = {}) {
        return this.request('PUT', url, addBody(options, body));
    }
}
HttpClient.ɵfac = function HttpClient_Factory(t) { return new (t || HttpClient)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](HttpHandler)); };
HttpClient.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({ token: HttpClient, factory: HttpClient.ɵfac });
HttpClient.ctorParameters = () => [
    { type: HttpHandler }
];
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](HttpClient, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"]
    }], function () { return [{ type: HttpHandler }]; }, null); })();

/**
 * @license
 * Copyright Google LLC All Rights Reserved.
 *
 * Use of this source code is governed by an MIT-style license that can be
 * found in the LICENSE file at https://angular.io/license
 */
/**
 * `HttpHandler` which applies an `HttpInterceptor` to an `HttpRequest`.
 *
 *
 */
class HttpInterceptorHandler {
    constructor(next, interceptor) {
        this.next = next;
        this.interceptor = interceptor;
    }
    handle(req) {
        return this.interceptor.intercept(req, this.next);
    }
}
/**
 * A multi-provider token that represents the array of registered
 * `HttpInterceptor` objects.
 *
 * @publicApi
 */
const HTTP_INTERCEPTORS = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["InjectionToken"]('HTTP_INTERCEPTORS');
class NoopInterceptor {
    intercept(req, next) {
        return next.handle(req);
    }
}
NoopInterceptor.ɵfac = function NoopInterceptor_Factory(t) { return new (t || NoopInterceptor)(); };
NoopInterceptor.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({ token: NoopInterceptor, factory: NoopInterceptor.ɵfac });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](NoopInterceptor, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"]
    }], null, null); })();

/**
 * @license
 * Copyright Google LLC All Rights Reserved.
 *
 * Use of this source code is governed by an MIT-style license that can be
 * found in the LICENSE file at https://angular.io/license
 */
// Every request made through JSONP needs a callback name that's unique across the
// whole page. Each request is assigned an id and the callback name is constructed
// from that. The next id to be assigned is tracked in a global variable here that
// is shared among all applications on the page.
let nextRequestId = 0;
// Error text given when a JSONP script is injected, but doesn't invoke the callback
// passed in its URL.
const JSONP_ERR_NO_CALLBACK = 'JSONP injected script did not invoke callback.';
// Error text given when a request is passed to the JsonpClientBackend that doesn't
// have a request method JSONP.
const JSONP_ERR_WRONG_METHOD = 'JSONP requests must use JSONP request method.';
const JSONP_ERR_WRONG_RESPONSE_TYPE = 'JSONP requests must use Json response type.';
/**
 * DI token/abstract type representing a map of JSONP callbacks.
 *
 * In the browser, this should always be the `window` object.
 *
 *
 */
class JsonpCallbackContext {
}
/**
 * Processes an `HttpRequest` with the JSONP method,
 * by performing JSONP style requests.
 * @see `HttpHandler`
 * @see `HttpXhrBackend`
 *
 * @publicApi
 */
class JsonpClientBackend {
    constructor(callbackMap, document) {
        this.callbackMap = callbackMap;
        this.document = document;
    }
    /**
     * Get the name of the next callback method, by incrementing the global `nextRequestId`.
     */
    nextCallback() {
        return `ng_jsonp_callback_${nextRequestId++}`;
    }
    /**
     * Processes a JSONP request and returns an event stream of the results.
     * @param req The request object.
     * @returns An observable of the response events.
     *
     */
    handle(req) {
        // Firstly, check both the method and response type. If either doesn't match
        // then the request was improperly routed here and cannot be handled.
        if (req.method !== 'JSONP') {
            throw new Error(JSONP_ERR_WRONG_METHOD);
        }
        else if (req.responseType !== 'json') {
            throw new Error(JSONP_ERR_WRONG_RESPONSE_TYPE);
        }
        // Everything else happens inside the Observable boundary.
        return new rxjs__WEBPACK_IMPORTED_MODULE_1__["Observable"]((observer) => {
            // The first step to make a request is to generate the callback name, and replace the
            // callback placeholder in the URL with the name. Care has to be taken here to ensure
            // a trailing &, if matched, gets inserted back into the URL in the correct place.
            const callback = this.nextCallback();
            const url = req.urlWithParams.replace(/=JSONP_CALLBACK(&|$)/, `=${callback}$1`);
            // Construct the <script> tag and point it at the URL.
            const node = this.document.createElement('script');
            node.src = url;
            // A JSONP request requires waiting for multiple callbacks. These variables
            // are closed over and track state across those callbacks.
            // The response object, if one has been received, or null otherwise.
            let body = null;
            // Whether the response callback has been called.
            let finished = false;
            // Whether the request has been cancelled (and thus any other callbacks)
            // should be ignored.
            let cancelled = false;
            // Set the response callback in this.callbackMap (which will be the window
            // object in the browser. The script being loaded via the <script> tag will
            // eventually call this callback.
            this.callbackMap[callback] = (data) => {
                // Data has been received from the JSONP script. Firstly, delete this callback.
                delete this.callbackMap[callback];
                // Next, make sure the request wasn't cancelled in the meantime.
                if (cancelled) {
                    return;
                }
                // Set state to indicate data was received.
                body = data;
                finished = true;
            };
            // cleanup() is a utility closure that removes the <script> from the page and
            // the response callback from the window. This logic is used in both the
            // success, error, and cancellation paths, so it's extracted out for convenience.
            const cleanup = () => {
                // Remove the <script> tag if it's still on the page.
                if (node.parentNode) {
                    node.parentNode.removeChild(node);
                }
                // Remove the response callback from the callbackMap (window object in the
                // browser).
                delete this.callbackMap[callback];
            };
            // onLoad() is the success callback which runs after the response callback
            // if the JSONP script loads successfully. The event itself is unimportant.
            // If something went wrong, onLoad() may run without the response callback
            // having been invoked.
            const onLoad = (event) => {
                // Do nothing if the request has been cancelled.
                if (cancelled) {
                    return;
                }
                // Cleanup the page.
                cleanup();
                // Check whether the response callback has run.
                if (!finished) {
                    // It hasn't, something went wrong with the request. Return an error via
                    // the Observable error path. All JSONP errors have status 0.
                    observer.error(new HttpErrorResponse({
                        url,
                        status: 0,
                        statusText: 'JSONP Error',
                        error: new Error(JSONP_ERR_NO_CALLBACK),
                    }));
                    return;
                }
                // Success. body either contains the response body or null if none was
                // returned.
                observer.next(new HttpResponse({
                    body,
                    status: 200,
                    statusText: 'OK',
                    url,
                }));
                // Complete the stream, the response is over.
                observer.complete();
            };
            // onError() is the error callback, which runs if the script returned generates
            // a Javascript error. It emits the error via the Observable error channel as
            // a HttpErrorResponse.
            const onError = (error) => {
                // If the request was already cancelled, no need to emit anything.
                if (cancelled) {
                    return;
                }
                cleanup();
                // Wrap the error in a HttpErrorResponse.
                observer.error(new HttpErrorResponse({
                    error,
                    status: 0,
                    statusText: 'JSONP Error',
                    url,
                }));
            };
            // Subscribe to both the success (load) and error events on the <script> tag,
            // and add it to the page.
            node.addEventListener('load', onLoad);
            node.addEventListener('error', onError);
            this.document.body.appendChild(node);
            // The request has now been successfully sent.
            observer.next({ type: HttpEventType.Sent });
            // Cancellation handler.
            return () => {
                // Track the cancellation so event listeners won't do anything even if already scheduled.
                cancelled = true;
                // Remove the event listeners so they won't run if the events later fire.
                node.removeEventListener('load', onLoad);
                node.removeEventListener('error', onError);
                // And finally, clean up the page.
                cleanup();
            };
        });
    }
}
JsonpClientBackend.ɵfac = function JsonpClientBackend_Factory(t) { return new (t || JsonpClientBackend)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](JsonpCallbackContext), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_angular_common__WEBPACK_IMPORTED_MODULE_3__["DOCUMENT"])); };
JsonpClientBackend.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({ token: JsonpClientBackend, factory: JsonpClientBackend.ɵfac });
JsonpClientBackend.ctorParameters = () => [
    { type: JsonpCallbackContext },
    { type: undefined, decorators: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"], args: [_angular_common__WEBPACK_IMPORTED_MODULE_3__["DOCUMENT"],] }] }
];
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](JsonpClientBackend, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"]
    }], function () { return [{ type: JsonpCallbackContext }, { type: undefined, decorators: [{
                type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"],
                args: [_angular_common__WEBPACK_IMPORTED_MODULE_3__["DOCUMENT"]]
            }] }]; }, null); })();
/**
 * Identifies requests with the method JSONP and
 * shifts them to the `JsonpClientBackend`.
 *
 * @see `HttpInterceptor`
 *
 * @publicApi
 */
class JsonpInterceptor {
    constructor(jsonp) {
        this.jsonp = jsonp;
    }
    /**
     * Identifies and handles a given JSONP request.
     * @param req The outgoing request object to handle.
     * @param next The next interceptor in the chain, or the backend
     * if no interceptors remain in the chain.
     * @returns An observable of the event stream.
     */
    intercept(req, next) {
        if (req.method === 'JSONP') {
            return this.jsonp.handle(req);
        }
        // Fall through for normal HTTP requests.
        return next.handle(req);
    }
}
JsonpInterceptor.ɵfac = function JsonpInterceptor_Factory(t) { return new (t || JsonpInterceptor)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](JsonpClientBackend)); };
JsonpInterceptor.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({ token: JsonpInterceptor, factory: JsonpInterceptor.ɵfac });
JsonpInterceptor.ctorParameters = () => [
    { type: JsonpClientBackend }
];
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](JsonpInterceptor, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"]
    }], function () { return [{ type: JsonpClientBackend }]; }, null); })();

/**
 * @license
 * Copyright Google LLC All Rights Reserved.
 *
 * Use of this source code is governed by an MIT-style license that can be
 * found in the LICENSE file at https://angular.io/license
 */
const XSSI_PREFIX = /^\)\]\}',?\n/;
/**
 * Determine an appropriate URL for the response, by checking either
 * XMLHttpRequest.responseURL or the X-Request-URL header.
 */
function getResponseUrl(xhr) {
    if ('responseURL' in xhr && xhr.responseURL) {
        return xhr.responseURL;
    }
    if (/^X-Request-URL:/m.test(xhr.getAllResponseHeaders())) {
        return xhr.getResponseHeader('X-Request-URL');
    }
    return null;
}
/**
 * A wrapper around the `XMLHttpRequest` constructor.
 *
 * @publicApi
 */
class XhrFactory {
}
/**
 * A factory for `HttpXhrBackend` that uses the `XMLHttpRequest` browser API.
 *
 */
class BrowserXhr {
    constructor() { }
    build() {
        return (new XMLHttpRequest());
    }
}
BrowserXhr.ɵfac = function BrowserXhr_Factory(t) { return new (t || BrowserXhr)(); };
BrowserXhr.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({ token: BrowserXhr, factory: BrowserXhr.ɵfac });
BrowserXhr.ctorParameters = () => [];
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](BrowserXhr, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"]
    }], function () { return []; }, null); })();
/**
 * Uses `XMLHttpRequest` to send requests to a backend server.
 * @see `HttpHandler`
 * @see `JsonpClientBackend`
 *
 * @publicApi
 */
class HttpXhrBackend {
    constructor(xhrFactory) {
        this.xhrFactory = xhrFactory;
    }
    /**
     * Processes a request and returns a stream of response events.
     * @param req The request object.
     * @returns An observable of the response events.
     */
    handle(req) {
        // Quick check to give a better error message when a user attempts to use
        // HttpClient.jsonp() without installing the JsonpClientModule
        if (req.method === 'JSONP') {
            throw new Error(`Attempted to construct Jsonp request without JsonpClientModule installed.`);
        }
        // Everything happens on Observable subscription.
        return new rxjs__WEBPACK_IMPORTED_MODULE_1__["Observable"]((observer) => {
            // Start by setting up the XHR object with request method, URL, and withCredentials flag.
            const xhr = this.xhrFactory.build();
            xhr.open(req.method, req.urlWithParams);
            if (!!req.withCredentials) {
                xhr.withCredentials = true;
            }
            // Add all the requested headers.
            req.headers.forEach((name, values) => xhr.setRequestHeader(name, values.join(',')));
            // Add an Accept header if one isn't present already.
            if (!req.headers.has('Accept')) {
                xhr.setRequestHeader('Accept', 'application/json, text/plain, */*');
            }
            // Auto-detect the Content-Type header if one isn't present already.
            if (!req.headers.has('Content-Type')) {
                const detectedType = req.detectContentTypeHeader();
                // Sometimes Content-Type detection fails.
                if (detectedType !== null) {
                    xhr.setRequestHeader('Content-Type', detectedType);
                }
            }
            // Set the responseType if one was requested.
            if (req.responseType) {
                const responseType = req.responseType.toLowerCase();
                // JSON responses need to be processed as text. This is because if the server
                // returns an XSSI-prefixed JSON response, the browser will fail to parse it,
                // xhr.response will be null, and xhr.responseText cannot be accessed to
                // retrieve the prefixed JSON data in order to strip the prefix. Thus, all JSON
                // is parsed by first requesting text and then applying JSON.parse.
                xhr.responseType = ((responseType !== 'json') ? responseType : 'text');
            }
            // Serialize the request body if one is present. If not, this will be set to null.
            const reqBody = req.serializeBody();
            // If progress events are enabled, response headers will be delivered
            // in two events - the HttpHeaderResponse event and the full HttpResponse
            // event. However, since response headers don't change in between these
            // two events, it doesn't make sense to parse them twice. So headerResponse
            // caches the data extracted from the response whenever it's first parsed,
            // to ensure parsing isn't duplicated.
            let headerResponse = null;
            // partialFromXhr extracts the HttpHeaderResponse from the current XMLHttpRequest
            // state, and memoizes it into headerResponse.
            const partialFromXhr = () => {
                if (headerResponse !== null) {
                    return headerResponse;
                }
                // Read status and normalize an IE9 bug (http://bugs.jquery.com/ticket/1450).
                const status = xhr.status === 1223 ? 204 : xhr.status;
                const statusText = xhr.statusText || 'OK';
                // Parse headers from XMLHttpRequest - this step is lazy.
                const headers = new HttpHeaders(xhr.getAllResponseHeaders());
                // Read the response URL from the XMLHttpResponse instance and fall back on the
                // request URL.
                const url = getResponseUrl(xhr) || req.url;
                // Construct the HttpHeaderResponse and memoize it.
                headerResponse = new HttpHeaderResponse({ headers, status, statusText, url });
                return headerResponse;
            };
            // Next, a few closures are defined for the various events which XMLHttpRequest can
            // emit. This allows them to be unregistered as event listeners later.
            // First up is the load event, which represents a response being fully available.
            const onLoad = () => {
                // Read response state from the memoized partial data.
                let { headers, status, statusText, url } = partialFromXhr();
                // The body will be read out if present.
                let body = null;
                if (status !== 204) {
                    // Use XMLHttpRequest.response if set, responseText otherwise.
                    body = (typeof xhr.response === 'undefined') ? xhr.responseText : xhr.response;
                }
                // Normalize another potential bug (this one comes from CORS).
                if (status === 0) {
                    status = !!body ? 200 : 0;
                }
                // ok determines whether the response will be transmitted on the event or
                // error channel. Unsuccessful status codes (not 2xx) will always be errors,
                // but a successful status code can still result in an error if the user
                // asked for JSON data and the body cannot be parsed as such.
                let ok = status >= 200 && status < 300;
                // Check whether the body needs to be parsed as JSON (in many cases the browser
                // will have done that already).
                if (req.responseType === 'json' && typeof body === 'string') {
                    // Save the original body, before attempting XSSI prefix stripping.
                    const originalBody = body;
                    body = body.replace(XSSI_PREFIX, '');
                    try {
                        // Attempt the parse. If it fails, a parse error should be delivered to the user.
                        body = body !== '' ? JSON.parse(body) : null;
                    }
                    catch (error) {
                        // Since the JSON.parse failed, it's reasonable to assume this might not have been a
                        // JSON response. Restore the original body (including any XSSI prefix) to deliver
                        // a better error response.
                        body = originalBody;
                        // If this was an error request to begin with, leave it as a string, it probably
                        // just isn't JSON. Otherwise, deliver the parsing error to the user.
                        if (ok) {
                            // Even though the response status was 2xx, this is still an error.
                            ok = false;
                            // The parse error contains the text of the body that failed to parse.
                            body = { error, text: body };
                        }
                    }
                }
                if (ok) {
                    // A successful response is delivered on the event stream.
                    observer.next(new HttpResponse({
                        body,
                        headers,
                        status,
                        statusText,
                        url: url || undefined,
                    }));
                    // The full body has been received and delivered, no further events
                    // are possible. This request is complete.
                    observer.complete();
                }
                else {
                    // An unsuccessful request is delivered on the error channel.
                    observer.error(new HttpErrorResponse({
                        // The error in this case is the response body (error from the server).
                        error: body,
                        headers,
                        status,
                        statusText,
                        url: url || undefined,
                    }));
                }
            };
            // The onError callback is called when something goes wrong at the network level.
            // Connection timeout, DNS error, offline, etc. These are actual errors, and are
            // transmitted on the error channel.
            const onError = (error) => {
                const { url } = partialFromXhr();
                const res = new HttpErrorResponse({
                    error,
                    status: xhr.status || 0,
                    statusText: xhr.statusText || 'Unknown Error',
                    url: url || undefined,
                });
                observer.error(res);
            };
            // The sentHeaders flag tracks whether the HttpResponseHeaders event
            // has been sent on the stream. This is necessary to track if progress
            // is enabled since the event will be sent on only the first download
            // progerss event.
            let sentHeaders = false;
            // The download progress event handler, which is only registered if
            // progress events are enabled.
            const onDownProgress = (event) => {
                // Send the HttpResponseHeaders event if it hasn't been sent already.
                if (!sentHeaders) {
                    observer.next(partialFromXhr());
                    sentHeaders = true;
                }
                // Start building the download progress event to deliver on the response
                // event stream.
                let progressEvent = {
                    type: HttpEventType.DownloadProgress,
                    loaded: event.loaded,
                };
                // Set the total number of bytes in the event if it's available.
                if (event.lengthComputable) {
                    progressEvent.total = event.total;
                }
                // If the request was for text content and a partial response is
                // available on XMLHttpRequest, include it in the progress event
                // to allow for streaming reads.
                if (req.responseType === 'text' && !!xhr.responseText) {
                    progressEvent.partialText = xhr.responseText;
                }
                // Finally, fire the event.
                observer.next(progressEvent);
            };
            // The upload progress event handler, which is only registered if
            // progress events are enabled.
            const onUpProgress = (event) => {
                // Upload progress events are simpler. Begin building the progress
                // event.
                let progress = {
                    type: HttpEventType.UploadProgress,
                    loaded: event.loaded,
                };
                // If the total number of bytes being uploaded is available, include
                // it.
                if (event.lengthComputable) {
                    progress.total = event.total;
                }
                // Send the event.
                observer.next(progress);
            };
            // By default, register for load and error events.
            xhr.addEventListener('load', onLoad);
            xhr.addEventListener('error', onError);
            // Progress events are only enabled if requested.
            if (req.reportProgress) {
                // Download progress is always enabled if requested.
                xhr.addEventListener('progress', onDownProgress);
                // Upload progress depends on whether there is a body to upload.
                if (reqBody !== null && xhr.upload) {
                    xhr.upload.addEventListener('progress', onUpProgress);
                }
            }
            // Fire the request, and notify the event stream that it was fired.
            xhr.send(reqBody);
            observer.next({ type: HttpEventType.Sent });
            // This is the return from the Observable function, which is the
            // request cancellation handler.
            return () => {
                // On a cancellation, remove all registered event listeners.
                xhr.removeEventListener('error', onError);
                xhr.removeEventListener('load', onLoad);
                if (req.reportProgress) {
                    xhr.removeEventListener('progress', onDownProgress);
                    if (reqBody !== null && xhr.upload) {
                        xhr.upload.removeEventListener('progress', onUpProgress);
                    }
                }
                // Finally, abort the in-flight request.
                if (xhr.readyState !== xhr.DONE) {
                    xhr.abort();
                }
            };
        });
    }
}
HttpXhrBackend.ɵfac = function HttpXhrBackend_Factory(t) { return new (t || HttpXhrBackend)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](XhrFactory)); };
HttpXhrBackend.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({ token: HttpXhrBackend, factory: HttpXhrBackend.ɵfac });
HttpXhrBackend.ctorParameters = () => [
    { type: XhrFactory }
];
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](HttpXhrBackend, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"]
    }], function () { return [{ type: XhrFactory }]; }, null); })();

/**
 * @license
 * Copyright Google LLC All Rights Reserved.
 *
 * Use of this source code is governed by an MIT-style license that can be
 * found in the LICENSE file at https://angular.io/license
 */
const XSRF_COOKIE_NAME = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["InjectionToken"]('XSRF_COOKIE_NAME');
const XSRF_HEADER_NAME = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["InjectionToken"]('XSRF_HEADER_NAME');
/**
 * Retrieves the current XSRF token to use with the next outgoing request.
 *
 * @publicApi
 */
class HttpXsrfTokenExtractor {
}
/**
 * `HttpXsrfTokenExtractor` which retrieves the token from a cookie.
 */
class HttpXsrfCookieExtractor {
    constructor(doc, platform, cookieName) {
        this.doc = doc;
        this.platform = platform;
        this.cookieName = cookieName;
        this.lastCookieString = '';
        this.lastToken = null;
        /**
         * @internal for testing
         */
        this.parseCount = 0;
    }
    getToken() {
        if (this.platform === 'server') {
            return null;
        }
        const cookieString = this.doc.cookie || '';
        if (cookieString !== this.lastCookieString) {
            this.parseCount++;
            this.lastToken = Object(_angular_common__WEBPACK_IMPORTED_MODULE_3__["ɵparseCookieValue"])(cookieString, this.cookieName);
            this.lastCookieString = cookieString;
        }
        return this.lastToken;
    }
}
HttpXsrfCookieExtractor.ɵfac = function HttpXsrfCookieExtractor_Factory(t) { return new (t || HttpXsrfCookieExtractor)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_angular_common__WEBPACK_IMPORTED_MODULE_3__["DOCUMENT"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["PLATFORM_ID"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](XSRF_COOKIE_NAME)); };
HttpXsrfCookieExtractor.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({ token: HttpXsrfCookieExtractor, factory: HttpXsrfCookieExtractor.ɵfac });
HttpXsrfCookieExtractor.ctorParameters = () => [
    { type: undefined, decorators: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"], args: [_angular_common__WEBPACK_IMPORTED_MODULE_3__["DOCUMENT"],] }] },
    { type: String, decorators: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"], args: [_angular_core__WEBPACK_IMPORTED_MODULE_0__["PLATFORM_ID"],] }] },
    { type: String, decorators: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"], args: [XSRF_COOKIE_NAME,] }] }
];
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](HttpXsrfCookieExtractor, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"]
    }], function () { return [{ type: undefined, decorators: [{
                type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"],
                args: [_angular_common__WEBPACK_IMPORTED_MODULE_3__["DOCUMENT"]]
            }] }, { type: String, decorators: [{
                type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"],
                args: [_angular_core__WEBPACK_IMPORTED_MODULE_0__["PLATFORM_ID"]]
            }] }, { type: String, decorators: [{
                type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"],
                args: [XSRF_COOKIE_NAME]
            }] }]; }, null); })();
/**
 * `HttpInterceptor` which adds an XSRF token to eligible outgoing requests.
 */
class HttpXsrfInterceptor {
    constructor(tokenService, headerName) {
        this.tokenService = tokenService;
        this.headerName = headerName;
    }
    intercept(req, next) {
        const lcUrl = req.url.toLowerCase();
        // Skip both non-mutating requests and absolute URLs.
        // Non-mutating requests don't require a token, and absolute URLs require special handling
        // anyway as the cookie set
        // on our origin is not the same as the token expected by another origin.
        if (req.method === 'GET' || req.method === 'HEAD' || lcUrl.startsWith('http://') ||
            lcUrl.startsWith('https://')) {
            return next.handle(req);
        }
        const token = this.tokenService.getToken();
        // Be careful not to overwrite an existing header of the same name.
        if (token !== null && !req.headers.has(this.headerName)) {
            req = req.clone({ headers: req.headers.set(this.headerName, token) });
        }
        return next.handle(req);
    }
}
HttpXsrfInterceptor.ɵfac = function HttpXsrfInterceptor_Factory(t) { return new (t || HttpXsrfInterceptor)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](HttpXsrfTokenExtractor), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](XSRF_HEADER_NAME)); };
HttpXsrfInterceptor.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({ token: HttpXsrfInterceptor, factory: HttpXsrfInterceptor.ɵfac });
HttpXsrfInterceptor.ctorParameters = () => [
    { type: HttpXsrfTokenExtractor },
    { type: String, decorators: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"], args: [XSRF_HEADER_NAME,] }] }
];
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](HttpXsrfInterceptor, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"]
    }], function () { return [{ type: HttpXsrfTokenExtractor }, { type: String, decorators: [{
                type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"],
                args: [XSRF_HEADER_NAME]
            }] }]; }, null); })();

/**
 * @license
 * Copyright Google LLC All Rights Reserved.
 *
 * Use of this source code is governed by an MIT-style license that can be
 * found in the LICENSE file at https://angular.io/license
 */
/**
 * An injectable `HttpHandler` that applies multiple interceptors
 * to a request before passing it to the given `HttpBackend`.
 *
 * The interceptors are loaded lazily from the injector, to allow
 * interceptors to themselves inject classes depending indirectly
 * on `HttpInterceptingHandler` itself.
 * @see `HttpInterceptor`
 */
class HttpInterceptingHandler {
    constructor(backend, injector) {
        this.backend = backend;
        this.injector = injector;
        this.chain = null;
    }
    handle(req) {
        if (this.chain === null) {
            const interceptors = this.injector.get(HTTP_INTERCEPTORS, []);
            this.chain = interceptors.reduceRight((next, interceptor) => new HttpInterceptorHandler(next, interceptor), this.backend);
        }
        return this.chain.handle(req);
    }
}
HttpInterceptingHandler.ɵfac = function HttpInterceptingHandler_Factory(t) { return new (t || HttpInterceptingHandler)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](HttpBackend), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injector"])); };
HttpInterceptingHandler.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({ token: HttpInterceptingHandler, factory: HttpInterceptingHandler.ɵfac });
HttpInterceptingHandler.ctorParameters = () => [
    { type: HttpBackend },
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injector"] }
];
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](HttpInterceptingHandler, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"]
    }], function () { return [{ type: HttpBackend }, { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injector"] }]; }, null); })();
/**
 * Constructs an `HttpHandler` that applies interceptors
 * to a request before passing it to the given `HttpBackend`.
 *
 * Use as a factory function within `HttpClientModule`.
 *
 *
 */
function interceptingHandler(backend, interceptors = []) {
    if (!interceptors) {
        return backend;
    }
    return interceptors.reduceRight((next, interceptor) => new HttpInterceptorHandler(next, interceptor), backend);
}
/**
 * Factory function that determines where to store JSONP callbacks.
 *
 * Ordinarily JSONP callbacks are stored on the `window` object, but this may not exist
 * in test environments. In that case, callbacks are stored on an anonymous object instead.
 *
 *
 */
function jsonpCallbackContext() {
    if (typeof window === 'object') {
        return window;
    }
    return {};
}
/**
 * Configures XSRF protection support for outgoing requests.
 *
 * For a server that supports a cookie-based XSRF protection system,
 * use directly to configure XSRF protection with the correct
 * cookie and header names.
 *
 * If no names are supplied, the default cookie name is `XSRF-TOKEN`
 * and the default header name is `X-XSRF-TOKEN`.
 *
 * @publicApi
 */
class HttpClientXsrfModule {
    /**
     * Disable the default XSRF protection.
     */
    static disable() {
        return {
            ngModule: HttpClientXsrfModule,
            providers: [
                { provide: HttpXsrfInterceptor, useClass: NoopInterceptor },
            ],
        };
    }
    /**
     * Configure XSRF protection.
     * @param options An object that can specify either or both
     * cookie name or header name.
     * - Cookie name default is `XSRF-TOKEN`.
     * - Header name default is `X-XSRF-TOKEN`.
     *
     */
    static withOptions(options = {}) {
        return {
            ngModule: HttpClientXsrfModule,
            providers: [
                options.cookieName ? { provide: XSRF_COOKIE_NAME, useValue: options.cookieName } : [],
                options.headerName ? { provide: XSRF_HEADER_NAME, useValue: options.headerName } : [],
            ],
        };
    }
}
HttpClientXsrfModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({ type: HttpClientXsrfModule });
HttpClientXsrfModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({ factory: function HttpClientXsrfModule_Factory(t) { return new (t || HttpClientXsrfModule)(); }, providers: [
        HttpXsrfInterceptor,
        { provide: HTTP_INTERCEPTORS, useExisting: HttpXsrfInterceptor, multi: true },
        { provide: HttpXsrfTokenExtractor, useClass: HttpXsrfCookieExtractor },
        { provide: XSRF_COOKIE_NAME, useValue: 'XSRF-TOKEN' },
        { provide: XSRF_HEADER_NAME, useValue: 'X-XSRF-TOKEN' },
    ] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](HttpClientXsrfModule, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"],
        args: [{
                providers: [
                    HttpXsrfInterceptor,
                    { provide: HTTP_INTERCEPTORS, useExisting: HttpXsrfInterceptor, multi: true },
                    { provide: HttpXsrfTokenExtractor, useClass: HttpXsrfCookieExtractor },
                    { provide: XSRF_COOKIE_NAME, useValue: 'XSRF-TOKEN' },
                    { provide: XSRF_HEADER_NAME, useValue: 'X-XSRF-TOKEN' },
                ]
            }]
    }], null, null); })();
/**
 * Configures the [dependency injector](guide/glossary#injector) for `HttpClient`
 * with supporting services for XSRF. Automatically imported by `HttpClientModule`.
 *
 * You can add interceptors to the chain behind `HttpClient` by binding them to the
 * multiprovider for built-in [DI token](guide/glossary#di-token) `HTTP_INTERCEPTORS`.
 *
 * @publicApi
 */
class HttpClientModule {
}
HttpClientModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({ type: HttpClientModule });
HttpClientModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({ factory: function HttpClientModule_Factory(t) { return new (t || HttpClientModule)(); }, providers: [
        HttpClient,
        { provide: HttpHandler, useClass: HttpInterceptingHandler },
        HttpXhrBackend,
        { provide: HttpBackend, useExisting: HttpXhrBackend },
        BrowserXhr,
        { provide: XhrFactory, useExisting: BrowserXhr },
    ], imports: [[
            HttpClientXsrfModule.withOptions({
                cookieName: 'XSRF-TOKEN',
                headerName: 'X-XSRF-TOKEN'
            }),
        ]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](HttpClientModule, { imports: [HttpClientXsrfModule] }); })();
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](HttpClientModule, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"],
        args: [{
                /**
                 * Optional configuration for XSRF protection.
                 */
                imports: [
                    HttpClientXsrfModule.withOptions({
                        cookieName: 'XSRF-TOKEN',
                        headerName: 'X-XSRF-TOKEN'
                    }),
                ],
                /**
                 * Configures the [dependency injector](guide/glossary#injector) where it is imported
                 * with supporting services for HTTP communications.
                 */
                providers: [
                    HttpClient,
                    { provide: HttpHandler, useClass: HttpInterceptingHandler },
                    HttpXhrBackend,
                    { provide: HttpBackend, useExisting: HttpXhrBackend },
                    BrowserXhr,
                    { provide: XhrFactory, useExisting: BrowserXhr },
                ]
            }]
    }], null, null); })();
/**
 * Configures the [dependency injector](guide/glossary#injector) for `HttpClient`
 * with supporting services for JSONP.
 * Without this module, Jsonp requests reach the backend
 * with method JSONP, where they are rejected.
 *
 * You can add interceptors to the chain behind `HttpClient` by binding them to the
 * multiprovider for built-in [DI token](guide/glossary#di-token) `HTTP_INTERCEPTORS`.
 *
 * @publicApi
 */
class HttpClientJsonpModule {
}
HttpClientJsonpModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({ type: HttpClientJsonpModule });
HttpClientJsonpModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({ factory: function HttpClientJsonpModule_Factory(t) { return new (t || HttpClientJsonpModule)(); }, providers: [
        JsonpClientBackend,
        { provide: JsonpCallbackContext, useFactory: jsonpCallbackContext },
        { provide: HTTP_INTERCEPTORS, useClass: JsonpInterceptor, multi: true },
    ] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](HttpClientJsonpModule, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"],
        args: [{
                providers: [
                    JsonpClientBackend,
                    { provide: JsonpCallbackContext, useFactory: jsonpCallbackContext },
                    { provide: HTTP_INTERCEPTORS, useClass: JsonpInterceptor, multi: true },
                ]
            }]
    }], null, null); })();

/**
 * @license
 * Copyright Google LLC All Rights Reserved.
 *
 * Use of this source code is governed by an MIT-style license that can be
 * found in the LICENSE file at https://angular.io/license
 */

/**
 * @license
 * Copyright Google LLC All Rights Reserved.
 *
 * Use of this source code is governed by an MIT-style license that can be
 * found in the LICENSE file at https://angular.io/license
 */

/**
 * Generated bundle index. Do not edit.
 */



//# sourceMappingURL=http.js.map

/***/ }),

/***/ "./node_modules/@ionic-native/android-permissions/__ivy_ngcc__/ngx/index.js":
/*!**********************************************************************************!*\
  !*** ./node_modules/@ionic-native/android-permissions/__ivy_ngcc__/ngx/index.js ***!
  \**********************************************************************************/
/*! exports provided: AndroidPermissions */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AndroidPermissions", function() { return AndroidPermissions; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _ionic_native_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic-native/core */ "./node_modules/@ionic-native/core/__ivy_ngcc__/index.js");




var AndroidPermissions = /** @class */ (function (_super) {
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"])(AndroidPermissions, _super);
    function AndroidPermissions() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.PERMISSION = {
            ACCESS_CHECKIN_PROPERTIES: 'android.permission.ACCESS_CHECKIN_PROPERTIES',
            ACCESS_COARSE_LOCATION: 'android.permission.ACCESS_COARSE_LOCATION',
            ACCESS_FINE_LOCATION: 'android.permission.ACCESS_FINE_LOCATION',
            ACCESS_LOCATION_EXTRA_COMMANDS: 'android.permission.ACCESS_LOCATION_EXTRA_COMMANDS',
            ACCESS_MOCK_LOCATION: 'android.permission.ACCESS_MOCK_LOCATION',
            ACCESS_NETWORK_STATE: 'android.permission.ACCESS_NETWORK_STATE',
            ACCESS_SURFACE_FLINGER: 'android.permission.ACCESS_SURFACE_FLINGER',
            ACCESS_WIFI_STATE: 'android.permission.ACCESS_WIFI_STATE',
            ACCOUNT_MANAGER: 'android.permission.ACCOUNT_MANAGER',
            ADD_VOICEMAIL: 'com.android.voicemail.permission.ADD_VOICEMAIL',
            AUTHENTICATE_ACCOUNTS: 'android.permission.AUTHENTICATE_ACCOUNTS',
            BATTERY_STATS: 'android.permission.BATTERY_STATS',
            BIND_ACCESSIBILITY_SERVICE: 'android.permission.BIND_ACCESSIBILITY_SERVICE',
            BIND_APPWIDGET: 'android.permission.BIND_APPWIDGET',
            BIND_CARRIER_MESSAGING_SERVICE: 'android.permission.BIND_CARRIER_MESSAGING_SERVICE',
            BIND_DEVICE_ADMIN: 'android.permission.BIND_DEVICE_ADMIN',
            BIND_DREAM_SERVICE: 'android.permission.BIND_DREAM_SERVICE',
            BIND_INPUT_METHOD: 'android.permission.BIND_INPUT_METHOD',
            BIND_NFC_SERVICE: 'android.permission.BIND_NFC_SERVICE',
            BIND_NOTIFICATION_LISTENER_SERVICE: 'android.permission.BIND_NOTIFICATION_LISTENER_SERVICE',
            BIND_PRINT_SERVICE: 'android.permission.BIND_PRINT_SERVICE',
            BIND_REMOTEVIEWS: 'android.permission.BIND_REMOTEVIEWS',
            BIND_TEXT_SERVICE: 'android.permission.BIND_TEXT_SERVICE',
            BIND_TV_INPUT: 'android.permission.BIND_TV_INPUT',
            BIND_VOICE_INTERACTION: 'android.permission.BIND_VOICE_INTERACTION',
            BIND_VPN_SERVICE: 'android.permission.BIND_VPN_SERVICE',
            BIND_WALLPAPER: 'android.permission.BIND_WALLPAPER',
            BLUETOOTH: 'android.permission.BLUETOOTH',
            BLUETOOTH_ADMIN: 'android.permission.BLUETOOTH_ADMIN',
            BLUETOOTH_PRIVILEGED: 'android.permission.BLUETOOTH_PRIVILEGED',
            BODY_SENSORS: 'android.permission.BODY_SENSORS',
            BRICK: 'android.permission.BRICK',
            BROADCAST_PACKAGE_REMOVED: 'android.permission.BROADCAST_PACKAGE_REMOVED',
            BROADCAST_SMS: 'android.permission.BROADCAST_SMS',
            BROADCAST_STICKY: 'android.permission.BROADCAST_STICKY',
            BROADCAST_WAP_PUSH: 'android.permission.BROADCAST_WAP_PUSH',
            CALL_PHONE: 'android.permission.CALL_PHONE',
            CALL_PRIVILEGED: 'android.permission.CALL_PRIVILEGED',
            CAMERA: 'android.permission.CAMERA',
            CAPTURE_AUDIO_OUTPUT: 'android.permission.CAPTURE_AUDIO_OUTPUT',
            CAPTURE_SECURE_VIDEO_OUTPUT: 'android.permission.CAPTURE_SECURE_VIDEO_OUTPUT',
            CAPTURE_VIDEO_OUTPUT: 'android.permission.CAPTURE_VIDEO_OUTPUT',
            CHANGE_COMPONENT_ENABLED_STATE: 'android.permission.CHANGE_COMPONENT_ENABLED_STATE',
            CHANGE_CONFIGURATION: 'android.permission.CHANGE_CONFIGURATION',
            CHANGE_NETWORK_STATE: 'android.permission.CHANGE_NETWORK_STATE',
            CHANGE_WIFI_MULTICAST_STATE: 'android.permission.CHANGE_WIFI_MULTICAST_STATE',
            CHANGE_WIFI_STATE: 'android.permission.CHANGE_WIFI_STATE',
            CLEAR_APP_CACHE: 'android.permission.CLEAR_APP_CACHE',
            CLEAR_APP_USER_DATA: 'android.permission.CLEAR_APP_USER_DATA',
            CONTROL_LOCATION_UPDATES: 'android.permission.CONTROL_LOCATION_UPDATES',
            DELETE_CACHE_FILES: 'android.permission.DELETE_CACHE_FILES',
            DELETE_PACKAGES: 'android.permission.DELETE_PACKAGES',
            DEVICE_POWER: 'android.permission.DEVICE_POWER',
            DIAGNOSTIC: 'android.permission.DIAGNOSTIC',
            DISABLE_KEYGUARD: 'android.permission.DISABLE_KEYGUARD',
            DUMP: 'android.permission.DUMP',
            EXPAND_STATUS_BAR: 'android.permission.EXPAND_STATUS_BAR',
            FACTORY_TEST: 'android.permission.FACTORY_TEST',
            FLASHLIGHT: 'android.permission.FLASHLIGHT',
            FORCE_BACK: 'android.permission.FORCE_BACK',
            GET_ACCOUNTS: 'android.permission.GET_ACCOUNTS',
            GET_PACKAGE_SIZE: 'android.permission.GET_PACKAGE_SIZE',
            GET_TASKS: 'android.permission.GET_TASKS',
            GET_TOP_ACTIVITY_INFO: 'android.permission.GET_TOP_ACTIVITY_INFO',
            GLOBAL_SEARCH: 'android.permission.GLOBAL_SEARCH',
            HARDWARE_TEST: 'android.permission.HARDWARE_TEST',
            INJECT_EVENTS: 'android.permission.INJECT_EVENTS',
            INSTALL_LOCATION_PROVIDER: 'android.permission.INSTALL_LOCATION_PROVIDER',
            INSTALL_PACKAGES: 'android.permission.INSTALL_PACKAGES',
            INSTALL_SHORTCUT: 'com.android.launcher.permission.INSTALL_SHORTCUT',
            INTERNAL_SYSTEM_WINDOW: 'android.permission.INTERNAL_SYSTEM_WINDOW',
            INTERNET: 'android.permission.INTERNET',
            KILL_BACKGROUND_PROCESSES: 'android.permission.KILL_BACKGROUND_PROCESSES',
            LOCATION_HARDWARE: 'android.permission.LOCATION_HARDWARE',
            MANAGE_ACCOUNTS: 'android.permission.MANAGE_ACCOUNTS',
            MANAGE_APP_TOKENS: 'android.permission.MANAGE_APP_TOKENS',
            MANAGE_DOCUMENTS: 'android.permission.MANAGE_DOCUMENTS',
            MASTER_CLEAR: 'android.permission.MASTER_CLEAR',
            MEDIA_CONTENT_CONTROL: 'android.permission.MEDIA_CONTENT_CONTROL',
            MODIFY_AUDIO_SETTINGS: 'android.permission.MODIFY_AUDIO_SETTINGS',
            MODIFY_PHONE_STATE: 'android.permission.MODIFY_PHONE_STATE',
            MOUNT_FORMAT_FILESYSTEMS: 'android.permission.MOUNT_FORMAT_FILESYSTEMS',
            MOUNT_UNMOUNT_FILESYSTEMS: 'android.permission.MOUNT_UNMOUNT_FILESYSTEMS',
            NFC: 'android.permission.NFC',
            PERSISTENT_ACTIVITY: 'android.permission.PERSISTENT_ACTIVITY',
            PROCESS_OUTGOING_CALLS: 'android.permission.PROCESS_OUTGOING_CALLS',
            READ_CALENDAR: 'android.permission.READ_CALENDAR',
            READ_CALL_LOG: 'android.permission.READ_CALL_LOG',
            READ_CONTACTS: 'android.permission.READ_CONTACTS',
            READ_EXTERNAL_STORAGE: 'android.permission.READ_EXTERNAL_STORAGE',
            READ_FRAME_BUFFER: 'android.permission.READ_FRAME_BUFFER',
            READ_HISTORY_BOOKMARKS: 'com.android.browser.permission.READ_HISTORY_BOOKMARKS',
            READ_INPUT_STATE: 'android.permission.READ_INPUT_STATE',
            READ_LOGS: 'android.permission.READ_LOGS',
            READ_PHONE_STATE: 'android.permission.READ_PHONE_STATE',
            READ_PROFILE: 'android.permission.READ_PROFILE',
            READ_SMS: 'android.permission.READ_SMS',
            READ_SOCIAL_STREAM: 'android.permission.READ_SOCIAL_STREAM',
            READ_SYNC_SETTINGS: 'android.permission.READ_SYNC_SETTINGS',
            READ_SYNC_STATS: 'android.permission.READ_SYNC_STATS',
            READ_USER_DICTIONARY: 'android.permission.READ_USER_DICTIONARY',
            READ_VOICEMAIL: 'com.android.voicemail.permission.READ_VOICEMAIL',
            REBOOT: 'android.permission.REBOOT',
            RECEIVE_BOOT_COMPLETED: 'android.permission.RECEIVE_BOOT_COMPLETED',
            RECEIVE_MMS: 'android.permission.RECEIVE_MMS',
            RECEIVE_SMS: 'android.permission.RECEIVE_SMS',
            RECEIVE_WAP_PUSH: 'android.permission.RECEIVE_WAP_PUSH',
            RECORD_AUDIO: 'android.permission.RECORD_AUDIO',
            REORDER_TASKS: 'android.permission.REORDER_TASKS',
            RESTART_PACKAGES: 'android.permission.RESTART_PACKAGES',
            SEND_RESPOND_VIA_MESSAGE: 'android.permission.SEND_RESPOND_VIA_MESSAGE',
            SEND_SMS: 'android.permission.SEND_SMS',
            SET_ACTIVITY_WATCHER: 'android.permission.SET_ACTIVITY_WATCHER',
            SET_ALARM: 'com.android.alarm.permission.SET_ALARM',
            SET_ALWAYS_FINISH: 'android.permission.SET_ALWAYS_FINISH',
            SET_ANIMATION_SCALE: 'android.permission.SET_ANIMATION_SCALE',
            SET_DEBUG_APP: 'android.permission.SET_DEBUG_APP',
            SET_ORIENTATION: 'android.permission.SET_ORIENTATION',
            SET_POINTER_SPEED: 'android.permission.SET_POINTER_SPEED',
            SET_PREFERRED_APPLICATIONS: 'android.permission.SET_PREFERRED_APPLICATIONS',
            SET_PROCESS_LIMIT: 'android.permission.SET_PROCESS_LIMIT',
            SET_TIME: 'android.permission.SET_TIME',
            SET_TIME_ZONE: 'android.permission.SET_TIME_ZONE',
            SET_WALLPAPER: 'android.permission.SET_WALLPAPER',
            SET_WALLPAPER_HINTS: 'android.permission.SET_WALLPAPER_HINTS',
            SIGNAL_PERSISTENT_PROCESSES: 'android.permission.SIGNAL_PERSISTENT_PROCESSES',
            STATUS_BAR: 'android.permission.STATUS_BAR',
            SUBSCRIBED_FEEDS_READ: 'android.permission.SUBSCRIBED_FEEDS_READ',
            SUBSCRIBED_FEEDS_WRITE: 'android.permission.SUBSCRIBED_FEEDS_WRITE',
            SYSTEM_ALERT_WINDOW: 'android.permission.SYSTEM_ALERT_WINDOW',
            TRANSMIT_IR: 'android.permission.TRANSMIT_IR',
            UNINSTALL_SHORTCUT: 'com.android.launcher.permission.UNINSTALL_SHORTCUT',
            UPDATE_DEVICE_STATS: 'android.permission.UPDATE_DEVICE_STATS',
            USE_CREDENTIALS: 'android.permission.USE_CREDENTIALS',
            USE_SIP: 'android.permission.USE_SIP',
            VIBRATE: 'android.permission.VIBRATE',
            WAKE_LOCK: 'android.permission.WAKE_LOCK',
            WRITE_APN_SETTINGS: 'android.permission.WRITE_APN_SETTINGS',
            WRITE_CALENDAR: 'android.permission.WRITE_CALENDAR',
            WRITE_CALL_LOG: 'android.permission.WRITE_CALL_LOG',
            WRITE_CONTACTS: 'android.permission.WRITE_CONTACTS',
            WRITE_EXTERNAL_STORAGE: 'android.permission.WRITE_EXTERNAL_STORAGE',
            WRITE_GSERVICES: 'android.permission.WRITE_GSERVICES',
            WRITE_HISTORY_BOOKMARKS: 'com.android.browser.permission.WRITE_HISTORY_BOOKMARKS',
            WRITE_PROFILE: 'android.permission.WRITE_PROFILE',
            WRITE_SECURE_SETTINGS: 'android.permission.WRITE_SECURE_SETTINGS',
            WRITE_SETTINGS: 'android.permission.WRITE_SETTINGS',
            WRITE_SMS: 'android.permission.WRITE_SMS',
            WRITE_SOCIAL_STREAM: 'android.permission.WRITE_SOCIAL_STREAM',
            WRITE_SYNC_SETTINGS: 'android.permission.WRITE_SYNC_SETTINGS',
            WRITE_USER_DICTIONARY: 'android.permission.WRITE_USER_DICTIONARY',
            WRITE_VOICEMAIL: 'com.android.voicemail.permission.WRITE_VOICEMAIL',
        };
        return _this;
    }
    AndroidPermissions.prototype.checkPermission = function (permission) { return Object(_ionic_native_core__WEBPACK_IMPORTED_MODULE_2__["cordova"])(this, "checkPermission", {}, arguments); };
    AndroidPermissions.prototype.requestPermission = function (permission) { return Object(_ionic_native_core__WEBPACK_IMPORTED_MODULE_2__["cordova"])(this, "requestPermission", {}, arguments); };
    AndroidPermissions.prototype.requestPermissions = function (permissions) { return Object(_ionic_native_core__WEBPACK_IMPORTED_MODULE_2__["cordova"])(this, "requestPermissions", {}, arguments); };
    AndroidPermissions.prototype.hasPermission = function (permission) { return Object(_ionic_native_core__WEBPACK_IMPORTED_MODULE_2__["cordova"])(this, "hasPermission", {}, arguments); };
    AndroidPermissions.pluginName = "AndroidPermissions";
    AndroidPermissions.plugin = "cordova-plugin-android-permissions";
    AndroidPermissions.pluginRef = "cordova.plugins.permissions";
    AndroidPermissions.repo = "https://github.com/NeoLSN/cordova-plugin-android-permissions";
    AndroidPermissions.platforms = ["Android"];
AndroidPermissions.ɵfac = function AndroidPermissions_Factory(t) { return ɵAndroidPermissions_BaseFactory(t || AndroidPermissions); };
AndroidPermissions.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineInjectable"]({ token: AndroidPermissions, factory: function (t) { return AndroidPermissions.ɵfac(t); } });
var ɵAndroidPermissions_BaseFactory = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetInheritedFactory"](AndroidPermissions);
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵsetClassMetadata"](AndroidPermissions, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"]
    }], null, null); })();
    return AndroidPermissions;
}(_ionic_native_core__WEBPACK_IMPORTED_MODULE_2__["IonicNativePlugin"]));


//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3NyYy9AaW9uaWMtbmF0aXZlL3BsdWdpbnMvYW5kcm9pZC1wZXJtaXNzaW9ucy9uZ3gvaW5kZXgudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0MsT0FBTyw4QkFBc0MsTUFBTSxvQkFBb0IsQ0FBQzs7QUFDeEU7QUFHMEIsSUFrQ2Msc0NBQWlCO0FBQUM7QUFFOUM7QUFDRCxRQUZULGdCQUFVLEdBQVE7QUFDcEIsWUFBSSx5QkFBeUIsRUFBRSw4Q0FBOEM7QUFDN0UsWUFBSSxzQkFBc0IsRUFBRSwyQ0FBMkM7QUFDdkUsWUFBSSxvQkFBb0IsRUFBRSx5Q0FBeUM7QUFDbkUsWUFBSSw4QkFBOEIsRUFBRSxtREFBbUQ7QUFDdkYsWUFBSSxvQkFBb0IsRUFBRSx5Q0FBeUM7QUFDbkUsWUFBSSxvQkFBb0IsRUFBRSx5Q0FBeUM7QUFDbkUsWUFBSSxzQkFBc0IsRUFBRSwyQ0FBMkM7QUFDdkUsWUFBSSxpQkFBaUIsRUFBRSxzQ0FBc0M7QUFDN0QsWUFBSSxlQUFlLEVBQUUsb0NBQW9DO0FBQ3pELFlBQUksYUFBYSxFQUFFLGdEQUFnRDtBQUNuRSxZQUFJLHFCQUFxQixFQUFFLDBDQUEwQztBQUNyRSxZQUFJLGFBQWEsRUFBRSxrQ0FBa0M7QUFDckQsWUFBSSwwQkFBMEIsRUFBRSwrQ0FBK0M7QUFDL0UsWUFBSSxjQUFjLEVBQUUsbUNBQW1DO0FBQ3ZELFlBQUksOEJBQThCLEVBQUUsbURBQW1EO0FBQ3ZGLFlBQUksaUJBQWlCLEVBQUUsc0NBQXNDO0FBQzdELFlBQUksa0JBQWtCLEVBQUUsdUNBQXVDO0FBQy9ELFlBQUksaUJBQWlCLEVBQUUsc0NBQXNDO0FBQzdELFlBQUksZ0JBQWdCLEVBQUUscUNBQXFDO0FBQzNELFlBQUksa0NBQWtDLEVBQUUsdURBQXVEO0FBQy9GLFlBQUksa0JBQWtCLEVBQUUsdUNBQXVDO0FBQy9ELFlBQUksZ0JBQWdCLEVBQUUscUNBQXFDO0FBQzNELFlBQUksaUJBQWlCLEVBQUUsc0NBQXNDO0FBQzdELFlBQUksYUFBYSxFQUFFLGtDQUFrQztBQUNyRCxZQUFJLHNCQUFzQixFQUFFLDJDQUEyQztBQUN2RSxZQUFJLGdCQUFnQixFQUFFLHFDQUFxQztBQUMzRCxZQUFJLGNBQWMsRUFBRSxtQ0FBbUM7QUFDdkQsWUFBSSxTQUFTLEVBQUUsOEJBQThCO0FBQzdDLFlBQUksZUFBZSxFQUFFLG9DQUFvQztBQUN6RCxZQUFJLG9CQUFvQixFQUFFLHlDQUF5QztBQUNuRSxZQUFJLFlBQVksRUFBRSxpQ0FBaUM7QUFDbkQsWUFBSSxLQUFLLEVBQUUsMEJBQTBCO0FBQ3JDLFlBQUkseUJBQXlCLEVBQUUsOENBQThDO0FBQzdFLFlBQUksYUFBYSxFQUFFLGtDQUFrQztBQUNyRCxZQUFJLGdCQUFnQixFQUFFLHFDQUFxQztBQUMzRCxZQUFJLGtCQUFrQixFQUFFLHVDQUF1QztBQUMvRCxZQUFJLFVBQVUsRUFBRSwrQkFBK0I7QUFDL0MsWUFBSSxlQUFlLEVBQUUsb0NBQW9DO0FBQ3pELFlBQUksTUFBTSxFQUFFLDJCQUEyQjtBQUN2QyxZQUFJLG9CQUFvQixFQUFFLHlDQUF5QztBQUNuRSxZQUFJLDJCQUEyQixFQUFFLGdEQUFnRDtBQUNqRixZQUFJLG9CQUFvQixFQUFFLHlDQUF5QztBQUNuRSxZQUFJLDhCQUE4QixFQUFFLG1EQUFtRDtBQUN2RixZQUFJLG9CQUFvQixFQUFFLHlDQUF5QztBQUNuRSxZQUFJLG9CQUFvQixFQUFFLHlDQUF5QztBQUNuRSxZQUFJLDJCQUEyQixFQUFFLGdEQUFnRDtBQUNqRixZQUFJLGlCQUFpQixFQUFFLHNDQUFzQztBQUM3RCxZQUFJLGVBQWUsRUFBRSxvQ0FBb0M7QUFDekQsWUFBSSxtQkFBbUIsRUFBRSx3Q0FBd0M7QUFDakUsWUFBSSx3QkFBd0IsRUFBRSw2Q0FBNkM7QUFDM0UsWUFBSSxrQkFBa0IsRUFBRSx1Q0FBdUM7QUFDL0QsWUFBSSxlQUFlLEVBQUUsb0NBQW9DO0FBQ3pELFlBQUksWUFBWSxFQUFFLGlDQUFpQztBQUNuRCxZQUFJLFVBQVUsRUFBRSwrQkFBK0I7QUFDL0MsWUFBSSxnQkFBZ0IsRUFBRSxxQ0FBcUM7QUFDM0QsWUFBSSxJQUFJLEVBQUUseUJBQXlCO0FBQ25DLFlBQUksaUJBQWlCLEVBQUUsc0NBQXNDO0FBQzdELFlBQUksWUFBWSxFQUFFLGlDQUFpQztBQUNuRCxZQUFJLFVBQVUsRUFBRSwrQkFBK0I7QUFDL0MsWUFBSSxVQUFVLEVBQUUsK0JBQStCO0FBQy9DLFlBQUksWUFBWSxFQUFFLGlDQUFpQztBQUNuRCxZQUFJLGdCQUFnQixFQUFFLHFDQUFxQztBQUMzRCxZQUFJLFNBQVMsRUFBRSw4QkFBOEI7QUFDN0MsWUFBSSxxQkFBcUIsRUFBRSwwQ0FBMEM7QUFDckUsWUFBSSxhQUFhLEVBQUUsa0NBQWtDO0FBQ3JELFlBQUksYUFBYSxFQUFFLGtDQUFrQztBQUNyRCxZQUFJLGFBQWEsRUFBRSxrQ0FBa0M7QUFDckQsWUFBSSx5QkFBeUIsRUFBRSw4Q0FBOEM7QUFDN0UsWUFBSSxnQkFBZ0IsRUFBRSxxQ0FBcUM7QUFDM0QsWUFBSSxnQkFBZ0IsRUFBRSxrREFBa0Q7QUFDeEUsWUFBSSxzQkFBc0IsRUFBRSwyQ0FBMkM7QUFDdkUsWUFBSSxRQUFRLEVBQUUsNkJBQTZCO0FBQzNDLFlBQUkseUJBQXlCLEVBQUUsOENBQThDO0FBQzdFLFlBQUksaUJBQWlCLEVBQUUsc0NBQXNDO0FBQzdELFlBQUksZUFBZSxFQUFFLG9DQUFvQztBQUN6RCxZQUFJLGlCQUFpQixFQUFFLHNDQUFzQztBQUM3RCxZQUFJLGdCQUFnQixFQUFFLHFDQUFxQztBQUMzRCxZQUFJLFlBQVksRUFBRSxpQ0FBaUM7QUFDbkQsWUFBSSxxQkFBcUIsRUFBRSwwQ0FBMEM7QUFDckUsWUFBSSxxQkFBcUIsRUFBRSwwQ0FBMEM7QUFDckUsWUFBSSxrQkFBa0IsRUFBRSx1Q0FBdUM7QUFDL0QsWUFBSSx3QkFBd0IsRUFBRSw2Q0FBNkM7QUFDM0UsWUFBSSx5QkFBeUIsRUFBRSw4Q0FBOEM7QUFDN0UsWUFBSSxHQUFHLEVBQUUsd0JBQXdCO0FBQ2pDLFlBQUksbUJBQW1CLEVBQUUsd0NBQXdDO0FBQ2pFLFlBQUksc0JBQXNCLEVBQUUsMkNBQTJDO0FBQ3ZFLFlBQUksYUFBYSxFQUFFLGtDQUFrQztBQUNyRCxZQUFJLGFBQWEsRUFBRSxrQ0FBa0M7QUFDckQsWUFBSSxhQUFhLEVBQUUsa0NBQWtDO0FBQ3JELFlBQUkscUJBQXFCLEVBQUUsMENBQTBDO0FBQ3JFLFlBQUksaUJBQWlCLEVBQUUsc0NBQXNDO0FBQzdELFlBQUksc0JBQXNCLEVBQUUsdURBQXVEO0FBQ25GLFlBQUksZ0JBQWdCLEVBQUUscUNBQXFDO0FBQzNELFlBQUksU0FBUyxFQUFFLDhCQUE4QjtBQUM3QyxZQUFJLGdCQUFnQixFQUFFLHFDQUFxQztBQUMzRCxZQUFJLFlBQVksRUFBRSxpQ0FBaUM7QUFDbkQsWUFBSSxRQUFRLEVBQUUsNkJBQTZCO0FBQzNDLFlBQUksa0JBQWtCLEVBQUUsdUNBQXVDO0FBQy9ELFlBQUksa0JBQWtCLEVBQUUsdUNBQXVDO0FBQy9ELFlBQUksZUFBZSxFQUFFLG9DQUFvQztBQUN6RCxZQUFJLG9CQUFvQixFQUFFLHlDQUF5QztBQUNuRSxZQUFJLGNBQWMsRUFBRSxpREFBaUQ7QUFDckUsWUFBSSxNQUFNLEVBQUUsMkJBQTJCO0FBQ3ZDLFlBQUksc0JBQXNCLEVBQUUsMkNBQTJDO0FBQ3ZFLFlBQUksV0FBVyxFQUFFLGdDQUFnQztBQUNqRCxZQUFJLFdBQVcsRUFBRSxnQ0FBZ0M7QUFDakQsWUFBSSxnQkFBZ0IsRUFBRSxxQ0FBcUM7QUFDM0QsWUFBSSxZQUFZLEVBQUUsaUNBQWlDO0FBQ25ELFlBQUksYUFBYSxFQUFFLGtDQUFrQztBQUNyRCxZQUFJLGdCQUFnQixFQUFFLHFDQUFxQztBQUMzRCxZQUFJLHdCQUF3QixFQUFFLDZDQUE2QztBQUMzRSxZQUFJLFFBQVEsRUFBRSw2QkFBNkI7QUFDM0MsWUFBSSxvQkFBb0IsRUFBRSx5Q0FBeUM7QUFDbkUsWUFBSSxTQUFTLEVBQUUsd0NBQXdDO0FBQ3ZELFlBQUksaUJBQWlCLEVBQUUsc0NBQXNDO0FBQzdELFlBQUksbUJBQW1CLEVBQUUsd0NBQXdDO0FBQ2pFLFlBQUksYUFBYSxFQUFFLGtDQUFrQztBQUNyRCxZQUFJLGVBQWUsRUFBRSxvQ0FBb0M7QUFDekQsWUFBSSxpQkFBaUIsRUFBRSxzQ0FBc0M7QUFDN0QsWUFBSSwwQkFBMEIsRUFBRSwrQ0FBK0M7QUFDL0UsWUFBSSxpQkFBaUIsRUFBRSxzQ0FBc0M7QUFDN0QsWUFBSSxRQUFRLEVBQUUsNkJBQTZCO0FBQzNDLFlBQUksYUFBYSxFQUFFLGtDQUFrQztBQUNyRCxZQUFJLGFBQWEsRUFBRSxrQ0FBa0M7QUFDckQsWUFBSSxtQkFBbUIsRUFBRSx3Q0FBd0M7QUFDakUsWUFBSSwyQkFBMkIsRUFBRSxnREFBZ0Q7QUFDakYsWUFBSSxVQUFVLEVBQUUsK0JBQStCO0FBQy9DLFlBQUkscUJBQXFCLEVBQUUsMENBQTBDO0FBQ3JFLFlBQUksc0JBQXNCLEVBQUUsMkNBQTJDO0FBQ3ZFLFlBQUksbUJBQW1CLEVBQUUsd0NBQXdDO0FBQ2pFLFlBQUksV0FBVyxFQUFFLGdDQUFnQztBQUNqRCxZQUFJLGtCQUFrQixFQUFFLG9EQUFvRDtBQUM1RSxZQUFJLG1CQUFtQixFQUFFLHdDQUF3QztBQUNqRSxZQUFJLGVBQWUsRUFBRSxvQ0FBb0M7QUFDekQsWUFBSSxPQUFPLEVBQUUsNEJBQTRCO0FBQ3pDLFlBQUksT0FBTyxFQUFFLDRCQUE0QjtBQUN6QyxZQUFJLFNBQVMsRUFBRSw4QkFBOEI7QUFDN0MsWUFBSSxrQkFBa0IsRUFBRSx1Q0FBdUM7QUFDL0QsWUFBSSxjQUFjLEVBQUUsbUNBQW1DO0FBQ3ZELFlBQUksY0FBYyxFQUFFLG1DQUFtQztBQUN2RCxZQUFJLGNBQWMsRUFBRSxtQ0FBbUM7QUFDdkQsWUFBSSxzQkFBc0IsRUFBRSwyQ0FBMkM7QUFDdkUsWUFBSSxlQUFlLEVBQUUsb0NBQW9DO0FBQ3pELFlBQUksdUJBQXVCLEVBQUUsd0RBQXdEO0FBQ3JGLFlBQUksYUFBYSxFQUFFLGtDQUFrQztBQUNyRCxZQUFJLHFCQUFxQixFQUFFLDBDQUEwQztBQUNyRSxZQUFJLGNBQWMsRUFBRSxtQ0FBbUM7QUFDdkQsWUFBSSxTQUFTLEVBQUUsOEJBQThCO0FBQzdDLFlBQUksbUJBQW1CLEVBQUUsd0NBQXdDO0FBQ2pFLFlBQUksbUJBQW1CLEVBQUUsd0NBQXdDO0FBQ2pFLFlBQUkscUJBQXFCLEVBQUUsMENBQTBDO0FBQ3JFLFlBQUksZUFBZSxFQUFFLGtEQUFrRDtBQUN2RSxTQUFHLENBQUM7QUFDSjtBQUVlO0FBQU0sSUFLbkIsNENBQWUsYUFBQyxVQUFrQjtBQUtwQyxJQUtFLDhDQUFpQixhQUFDLFVBQWtCO0FBS3BDLElBS0EsK0NBQWtCLGFBQUMsV0FBcUI7QUFLakIsSUFLdkIsMENBQWEsYUFBQyxVQUFrQjtBQUtoQztBQUNhO0FBRTBDO0FBQWtFO0FBQThGO0lBeE01TSxrQkFBa0Isd0JBRDlCLFVBQVUsRUFBRSxRQUNBLGtCQUFrQjs7Ozs7MEJBQUU7QUFBQyw2QkF2Q2xDO0FBQUUsRUF1Q3NDLGlCQUFpQjtBQUN4RCxTQURZLGtCQUFrQjtBQUFJIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgQ29yZG92YSwgSW9uaWNOYXRpdmVQbHVnaW4sIFBsdWdpbiB9IGZyb20gJ0Bpb25pYy1uYXRpdmUvY29yZSc7XG5cbi8qKlxuICogQG5hbWUgQW5kcm9pZCBQZXJtaXNzaW9uc1xuICogQHByZW1pZXIgYW5kcm9pZC1wZXJtaXNzaW9uc1xuICogQGRlc2NyaXB0aW9uXG4gKiBUaGlzIHBsdWdpbiBpcyBkZXNpZ25lZCB0byBzdXBwb3J0IEFuZHJvaWQgbmV3IHBlcm1pc3Npb25zIGNoZWNraW5nIG1lY2hhbmlzbS5cbiAqXG4gKiBZb3UgY2FuIGZpbmQgYWxsIHBlcm1pc3Npb25zIGhlcmU6IGh0dHBzOi8vZGV2ZWxvcGVyLmFuZHJvaWQuY29tL3JlZmVyZW5jZS9hbmRyb2lkL01hbmlmZXN0LnBlcm1pc3Npb24uaHRtbFxuICpcbiAqIEB1c2FnZVxuICogYGBgXG4gKiBpbXBvcnQgeyBBbmRyb2lkUGVybWlzc2lvbnMgfSBmcm9tICdAaW9uaWMtbmF0aXZlL2FuZHJvaWQtcGVybWlzc2lvbnMvbmd4JztcbiAqXG4gKlxuICogY29uc3RydWN0b3IocHJpdmF0ZSBhbmRyb2lkUGVybWlzc2lvbnM6IEFuZHJvaWRQZXJtaXNzaW9ucykgeyB9XG4gKlxuICogLi4uXG4gKlxuICogdGhpcy5hbmRyb2lkUGVybWlzc2lvbnMuY2hlY2tQZXJtaXNzaW9uKHRoaXMuYW5kcm9pZFBlcm1pc3Npb25zLlBFUk1JU1NJT04uQ0FNRVJBKS50aGVuKFxuICogICByZXN1bHQgPT4gY29uc29sZS5sb2coJ0hhcyBwZXJtaXNzaW9uPycscmVzdWx0Lmhhc1Blcm1pc3Npb24pLFxuICogICBlcnIgPT4gdGhpcy5hbmRyb2lkUGVybWlzc2lvbnMucmVxdWVzdFBlcm1pc3Npb24odGhpcy5hbmRyb2lkUGVybWlzc2lvbnMuUEVSTUlTU0lPTi5DQU1FUkEpXG4gKiApO1xuICpcbiAqIHRoaXMuYW5kcm9pZFBlcm1pc3Npb25zLnJlcXVlc3RQZXJtaXNzaW9ucyhbdGhpcy5hbmRyb2lkUGVybWlzc2lvbnMuUEVSTUlTU0lPTi5DQU1FUkEsIHRoaXMuYW5kcm9pZFBlcm1pc3Npb25zLlBFUk1JU1NJT04uR0VUX0FDQ09VTlRTXSk7XG4gKlxuICogYGBgXG4gKlxuICogQW5kcm9pZCAyNiBhbmQgYWJvdmU6IGR1ZSB0byBBbmRyb2lkIDI2J3MgY2hhbmdlcyB0byBwZXJtaXNzaW9ucyBoYW5kbGluZyAocGVybWlzc2lvbnMgYXJlIHJlcXVlc3RlZCBhdCB0aW1lIG9mIHVzZSByYXRoZXIgdGhhbiBhdCBydW50aW1lLCkgaWYgeW91ciBhcHAgZG9lcyBub3QgaW5jbHVkZSBhbnkgZnVuY3Rpb25zIChlZy4gb3RoZXIgSW9uaWMgTmF0aXZlIHBsdWdpbnMpIHRoYXQgdXRpbGl6ZSBhIHBhcnRpY3VsYXIgcGVybWlzc2lvbiwgdGhlbiBgcmVxdWVzdFBlcm1pc3Npb24oKWAgYW5kIGByZXF1ZXN0UGVybWlzc2lvbnMoKWAgd2lsbCByZXNvbHZlIGltbWVkaWF0ZWx5IHdpdGggbm8gcHJvbXB0IHNob3duIHRvIHRoZSB1c2VyLiAgVGh1cywgeW91IG11c3QgaW5jbHVkZSBhIGZ1bmN0aW9uIHV0aWxpemluZyB0aGUgZmVhdHVyZSB5b3Ugd291bGQgbGlrZSB0byB1c2UgYmVmb3JlIHJlcXVlc3RpbmcgcGVybWlzc2lvbiBmb3IgaXQuXG4gKi9cbkBQbHVnaW4oe1xuICBwbHVnaW5OYW1lOiAnQW5kcm9pZFBlcm1pc3Npb25zJyxcbiAgcGx1Z2luOiAnY29yZG92YS1wbHVnaW4tYW5kcm9pZC1wZXJtaXNzaW9ucycsXG4gIHBsdWdpblJlZjogJ2NvcmRvdmEucGx1Z2lucy5wZXJtaXNzaW9ucycsXG4gIHJlcG86ICdodHRwczovL2dpdGh1Yi5jb20vTmVvTFNOL2NvcmRvdmEtcGx1Z2luLWFuZHJvaWQtcGVybWlzc2lvbnMnLFxuICBwbGF0Zm9ybXM6IFsnQW5kcm9pZCddLFxufSlcbkBJbmplY3RhYmxlKClcbmV4cG9ydCBjbGFzcyBBbmRyb2lkUGVybWlzc2lvbnMgZXh0ZW5kcyBJb25pY05hdGl2ZVBsdWdpbiB7XG4gIFBFUk1JU1NJT046IGFueSA9IHtcbiAgICBBQ0NFU1NfQ0hFQ0tJTl9QUk9QRVJUSUVTOiAnYW5kcm9pZC5wZXJtaXNzaW9uLkFDQ0VTU19DSEVDS0lOX1BST1BFUlRJRVMnLFxuICAgIEFDQ0VTU19DT0FSU0VfTE9DQVRJT046ICdhbmRyb2lkLnBlcm1pc3Npb24uQUNDRVNTX0NPQVJTRV9MT0NBVElPTicsXG4gICAgQUNDRVNTX0ZJTkVfTE9DQVRJT046ICdhbmRyb2lkLnBlcm1pc3Npb24uQUNDRVNTX0ZJTkVfTE9DQVRJT04nLFxuICAgIEFDQ0VTU19MT0NBVElPTl9FWFRSQV9DT01NQU5EUzogJ2FuZHJvaWQucGVybWlzc2lvbi5BQ0NFU1NfTE9DQVRJT05fRVhUUkFfQ09NTUFORFMnLFxuICAgIEFDQ0VTU19NT0NLX0xPQ0FUSU9OOiAnYW5kcm9pZC5wZXJtaXNzaW9uLkFDQ0VTU19NT0NLX0xPQ0FUSU9OJyxcbiAgICBBQ0NFU1NfTkVUV09SS19TVEFURTogJ2FuZHJvaWQucGVybWlzc2lvbi5BQ0NFU1NfTkVUV09SS19TVEFURScsXG4gICAgQUNDRVNTX1NVUkZBQ0VfRkxJTkdFUjogJ2FuZHJvaWQucGVybWlzc2lvbi5BQ0NFU1NfU1VSRkFDRV9GTElOR0VSJyxcbiAgICBBQ0NFU1NfV0lGSV9TVEFURTogJ2FuZHJvaWQucGVybWlzc2lvbi5BQ0NFU1NfV0lGSV9TVEFURScsXG4gICAgQUNDT1VOVF9NQU5BR0VSOiAnYW5kcm9pZC5wZXJtaXNzaW9uLkFDQ09VTlRfTUFOQUdFUicsXG4gICAgQUREX1ZPSUNFTUFJTDogJ2NvbS5hbmRyb2lkLnZvaWNlbWFpbC5wZXJtaXNzaW9uLkFERF9WT0lDRU1BSUwnLFxuICAgIEFVVEhFTlRJQ0FURV9BQ0NPVU5UUzogJ2FuZHJvaWQucGVybWlzc2lvbi5BVVRIRU5USUNBVEVfQUNDT1VOVFMnLFxuICAgIEJBVFRFUllfU1RBVFM6ICdhbmRyb2lkLnBlcm1pc3Npb24uQkFUVEVSWV9TVEFUUycsXG4gICAgQklORF9BQ0NFU1NJQklMSVRZX1NFUlZJQ0U6ICdhbmRyb2lkLnBlcm1pc3Npb24uQklORF9BQ0NFU1NJQklMSVRZX1NFUlZJQ0UnLFxuICAgIEJJTkRfQVBQV0lER0VUOiAnYW5kcm9pZC5wZXJtaXNzaW9uLkJJTkRfQVBQV0lER0VUJyxcbiAgICBCSU5EX0NBUlJJRVJfTUVTU0FHSU5HX1NFUlZJQ0U6ICdhbmRyb2lkLnBlcm1pc3Npb24uQklORF9DQVJSSUVSX01FU1NBR0lOR19TRVJWSUNFJyxcbiAgICBCSU5EX0RFVklDRV9BRE1JTjogJ2FuZHJvaWQucGVybWlzc2lvbi5CSU5EX0RFVklDRV9BRE1JTicsXG4gICAgQklORF9EUkVBTV9TRVJWSUNFOiAnYW5kcm9pZC5wZXJtaXNzaW9uLkJJTkRfRFJFQU1fU0VSVklDRScsXG4gICAgQklORF9JTlBVVF9NRVRIT0Q6ICdhbmRyb2lkLnBlcm1pc3Npb24uQklORF9JTlBVVF9NRVRIT0QnLFxuICAgIEJJTkRfTkZDX1NFUlZJQ0U6ICdhbmRyb2lkLnBlcm1pc3Npb24uQklORF9ORkNfU0VSVklDRScsXG4gICAgQklORF9OT1RJRklDQVRJT05fTElTVEVORVJfU0VSVklDRTogJ2FuZHJvaWQucGVybWlzc2lvbi5CSU5EX05PVElGSUNBVElPTl9MSVNURU5FUl9TRVJWSUNFJyxcbiAgICBCSU5EX1BSSU5UX1NFUlZJQ0U6ICdhbmRyb2lkLnBlcm1pc3Npb24uQklORF9QUklOVF9TRVJWSUNFJyxcbiAgICBCSU5EX1JFTU9URVZJRVdTOiAnYW5kcm9pZC5wZXJtaXNzaW9uLkJJTkRfUkVNT1RFVklFV1MnLFxuICAgIEJJTkRfVEVYVF9TRVJWSUNFOiAnYW5kcm9pZC5wZXJtaXNzaW9uLkJJTkRfVEVYVF9TRVJWSUNFJyxcbiAgICBCSU5EX1RWX0lOUFVUOiAnYW5kcm9pZC5wZXJtaXNzaW9uLkJJTkRfVFZfSU5QVVQnLFxuICAgIEJJTkRfVk9JQ0VfSU5URVJBQ1RJT046ICdhbmRyb2lkLnBlcm1pc3Npb24uQklORF9WT0lDRV9JTlRFUkFDVElPTicsXG4gICAgQklORF9WUE5fU0VSVklDRTogJ2FuZHJvaWQucGVybWlzc2lvbi5CSU5EX1ZQTl9TRVJWSUNFJyxcbiAgICBCSU5EX1dBTExQQVBFUjogJ2FuZHJvaWQucGVybWlzc2lvbi5CSU5EX1dBTExQQVBFUicsXG4gICAgQkxVRVRPT1RIOiAnYW5kcm9pZC5wZXJtaXNzaW9uLkJMVUVUT09USCcsXG4gICAgQkxVRVRPT1RIX0FETUlOOiAnYW5kcm9pZC5wZXJtaXNzaW9uLkJMVUVUT09USF9BRE1JTicsXG4gICAgQkxVRVRPT1RIX1BSSVZJTEVHRUQ6ICdhbmRyb2lkLnBlcm1pc3Npb24uQkxVRVRPT1RIX1BSSVZJTEVHRUQnLFxuICAgIEJPRFlfU0VOU09SUzogJ2FuZHJvaWQucGVybWlzc2lvbi5CT0RZX1NFTlNPUlMnLFxuICAgIEJSSUNLOiAnYW5kcm9pZC5wZXJtaXNzaW9uLkJSSUNLJyxcbiAgICBCUk9BRENBU1RfUEFDS0FHRV9SRU1PVkVEOiAnYW5kcm9pZC5wZXJtaXNzaW9uLkJST0FEQ0FTVF9QQUNLQUdFX1JFTU9WRUQnLFxuICAgIEJST0FEQ0FTVF9TTVM6ICdhbmRyb2lkLnBlcm1pc3Npb24uQlJPQURDQVNUX1NNUycsXG4gICAgQlJPQURDQVNUX1NUSUNLWTogJ2FuZHJvaWQucGVybWlzc2lvbi5CUk9BRENBU1RfU1RJQ0tZJyxcbiAgICBCUk9BRENBU1RfV0FQX1BVU0g6ICdhbmRyb2lkLnBlcm1pc3Npb24uQlJPQURDQVNUX1dBUF9QVVNIJyxcbiAgICBDQUxMX1BIT05FOiAnYW5kcm9pZC5wZXJtaXNzaW9uLkNBTExfUEhPTkUnLFxuICAgIENBTExfUFJJVklMRUdFRDogJ2FuZHJvaWQucGVybWlzc2lvbi5DQUxMX1BSSVZJTEVHRUQnLFxuICAgIENBTUVSQTogJ2FuZHJvaWQucGVybWlzc2lvbi5DQU1FUkEnLFxuICAgIENBUFRVUkVfQVVESU9fT1VUUFVUOiAnYW5kcm9pZC5wZXJtaXNzaW9uLkNBUFRVUkVfQVVESU9fT1VUUFVUJyxcbiAgICBDQVBUVVJFX1NFQ1VSRV9WSURFT19PVVRQVVQ6ICdhbmRyb2lkLnBlcm1pc3Npb24uQ0FQVFVSRV9TRUNVUkVfVklERU9fT1VUUFVUJyxcbiAgICBDQVBUVVJFX1ZJREVPX09VVFBVVDogJ2FuZHJvaWQucGVybWlzc2lvbi5DQVBUVVJFX1ZJREVPX09VVFBVVCcsXG4gICAgQ0hBTkdFX0NPTVBPTkVOVF9FTkFCTEVEX1NUQVRFOiAnYW5kcm9pZC5wZXJtaXNzaW9uLkNIQU5HRV9DT01QT05FTlRfRU5BQkxFRF9TVEFURScsXG4gICAgQ0hBTkdFX0NPTkZJR1VSQVRJT046ICdhbmRyb2lkLnBlcm1pc3Npb24uQ0hBTkdFX0NPTkZJR1VSQVRJT04nLFxuICAgIENIQU5HRV9ORVRXT1JLX1NUQVRFOiAnYW5kcm9pZC5wZXJtaXNzaW9uLkNIQU5HRV9ORVRXT1JLX1NUQVRFJyxcbiAgICBDSEFOR0VfV0lGSV9NVUxUSUNBU1RfU1RBVEU6ICdhbmRyb2lkLnBlcm1pc3Npb24uQ0hBTkdFX1dJRklfTVVMVElDQVNUX1NUQVRFJyxcbiAgICBDSEFOR0VfV0lGSV9TVEFURTogJ2FuZHJvaWQucGVybWlzc2lvbi5DSEFOR0VfV0lGSV9TVEFURScsXG4gICAgQ0xFQVJfQVBQX0NBQ0hFOiAnYW5kcm9pZC5wZXJtaXNzaW9uLkNMRUFSX0FQUF9DQUNIRScsXG4gICAgQ0xFQVJfQVBQX1VTRVJfREFUQTogJ2FuZHJvaWQucGVybWlzc2lvbi5DTEVBUl9BUFBfVVNFUl9EQVRBJyxcbiAgICBDT05UUk9MX0xPQ0FUSU9OX1VQREFURVM6ICdhbmRyb2lkLnBlcm1pc3Npb24uQ09OVFJPTF9MT0NBVElPTl9VUERBVEVTJyxcbiAgICBERUxFVEVfQ0FDSEVfRklMRVM6ICdhbmRyb2lkLnBlcm1pc3Npb24uREVMRVRFX0NBQ0hFX0ZJTEVTJyxcbiAgICBERUxFVEVfUEFDS0FHRVM6ICdhbmRyb2lkLnBlcm1pc3Npb24uREVMRVRFX1BBQ0tBR0VTJyxcbiAgICBERVZJQ0VfUE9XRVI6ICdhbmRyb2lkLnBlcm1pc3Npb24uREVWSUNFX1BPV0VSJyxcbiAgICBESUFHTk9TVElDOiAnYW5kcm9pZC5wZXJtaXNzaW9uLkRJQUdOT1NUSUMnLFxuICAgIERJU0FCTEVfS0VZR1VBUkQ6ICdhbmRyb2lkLnBlcm1pc3Npb24uRElTQUJMRV9LRVlHVUFSRCcsXG4gICAgRFVNUDogJ2FuZHJvaWQucGVybWlzc2lvbi5EVU1QJyxcbiAgICBFWFBBTkRfU1RBVFVTX0JBUjogJ2FuZHJvaWQucGVybWlzc2lvbi5FWFBBTkRfU1RBVFVTX0JBUicsXG4gICAgRkFDVE9SWV9URVNUOiAnYW5kcm9pZC5wZXJtaXNzaW9uLkZBQ1RPUllfVEVTVCcsXG4gICAgRkxBU0hMSUdIVDogJ2FuZHJvaWQucGVybWlzc2lvbi5GTEFTSExJR0hUJyxcbiAgICBGT1JDRV9CQUNLOiAnYW5kcm9pZC5wZXJtaXNzaW9uLkZPUkNFX0JBQ0snLFxuICAgIEdFVF9BQ0NPVU5UUzogJ2FuZHJvaWQucGVybWlzc2lvbi5HRVRfQUNDT1VOVFMnLFxuICAgIEdFVF9QQUNLQUdFX1NJWkU6ICdhbmRyb2lkLnBlcm1pc3Npb24uR0VUX1BBQ0tBR0VfU0laRScsXG4gICAgR0VUX1RBU0tTOiAnYW5kcm9pZC5wZXJtaXNzaW9uLkdFVF9UQVNLUycsXG4gICAgR0VUX1RPUF9BQ1RJVklUWV9JTkZPOiAnYW5kcm9pZC5wZXJtaXNzaW9uLkdFVF9UT1BfQUNUSVZJVFlfSU5GTycsXG4gICAgR0xPQkFMX1NFQVJDSDogJ2FuZHJvaWQucGVybWlzc2lvbi5HTE9CQUxfU0VBUkNIJyxcbiAgICBIQVJEV0FSRV9URVNUOiAnYW5kcm9pZC5wZXJtaXNzaW9uLkhBUkRXQVJFX1RFU1QnLFxuICAgIElOSkVDVF9FVkVOVFM6ICdhbmRyb2lkLnBlcm1pc3Npb24uSU5KRUNUX0VWRU5UUycsXG4gICAgSU5TVEFMTF9MT0NBVElPTl9QUk9WSURFUjogJ2FuZHJvaWQucGVybWlzc2lvbi5JTlNUQUxMX0xPQ0FUSU9OX1BST1ZJREVSJyxcbiAgICBJTlNUQUxMX1BBQ0tBR0VTOiAnYW5kcm9pZC5wZXJtaXNzaW9uLklOU1RBTExfUEFDS0FHRVMnLFxuICAgIElOU1RBTExfU0hPUlRDVVQ6ICdjb20uYW5kcm9pZC5sYXVuY2hlci5wZXJtaXNzaW9uLklOU1RBTExfU0hPUlRDVVQnLFxuICAgIElOVEVSTkFMX1NZU1RFTV9XSU5ET1c6ICdhbmRyb2lkLnBlcm1pc3Npb24uSU5URVJOQUxfU1lTVEVNX1dJTkRPVycsXG4gICAgSU5URVJORVQ6ICdhbmRyb2lkLnBlcm1pc3Npb24uSU5URVJORVQnLFxuICAgIEtJTExfQkFDS0dST1VORF9QUk9DRVNTRVM6ICdhbmRyb2lkLnBlcm1pc3Npb24uS0lMTF9CQUNLR1JPVU5EX1BST0NFU1NFUycsXG4gICAgTE9DQVRJT05fSEFSRFdBUkU6ICdhbmRyb2lkLnBlcm1pc3Npb24uTE9DQVRJT05fSEFSRFdBUkUnLFxuICAgIE1BTkFHRV9BQ0NPVU5UUzogJ2FuZHJvaWQucGVybWlzc2lvbi5NQU5BR0VfQUNDT1VOVFMnLFxuICAgIE1BTkFHRV9BUFBfVE9LRU5TOiAnYW5kcm9pZC5wZXJtaXNzaW9uLk1BTkFHRV9BUFBfVE9LRU5TJyxcbiAgICBNQU5BR0VfRE9DVU1FTlRTOiAnYW5kcm9pZC5wZXJtaXNzaW9uLk1BTkFHRV9ET0NVTUVOVFMnLFxuICAgIE1BU1RFUl9DTEVBUjogJ2FuZHJvaWQucGVybWlzc2lvbi5NQVNURVJfQ0xFQVInLFxuICAgIE1FRElBX0NPTlRFTlRfQ09OVFJPTDogJ2FuZHJvaWQucGVybWlzc2lvbi5NRURJQV9DT05URU5UX0NPTlRST0wnLFxuICAgIE1PRElGWV9BVURJT19TRVRUSU5HUzogJ2FuZHJvaWQucGVybWlzc2lvbi5NT0RJRllfQVVESU9fU0VUVElOR1MnLFxuICAgIE1PRElGWV9QSE9ORV9TVEFURTogJ2FuZHJvaWQucGVybWlzc2lvbi5NT0RJRllfUEhPTkVfU1RBVEUnLFxuICAgIE1PVU5UX0ZPUk1BVF9GSUxFU1lTVEVNUzogJ2FuZHJvaWQucGVybWlzc2lvbi5NT1VOVF9GT1JNQVRfRklMRVNZU1RFTVMnLFxuICAgIE1PVU5UX1VOTU9VTlRfRklMRVNZU1RFTVM6ICdhbmRyb2lkLnBlcm1pc3Npb24uTU9VTlRfVU5NT1VOVF9GSUxFU1lTVEVNUycsXG4gICAgTkZDOiAnYW5kcm9pZC5wZXJtaXNzaW9uLk5GQycsXG4gICAgUEVSU0lTVEVOVF9BQ1RJVklUWTogJ2FuZHJvaWQucGVybWlzc2lvbi5QRVJTSVNURU5UX0FDVElWSVRZJyxcbiAgICBQUk9DRVNTX09VVEdPSU5HX0NBTExTOiAnYW5kcm9pZC5wZXJtaXNzaW9uLlBST0NFU1NfT1VUR09JTkdfQ0FMTFMnLFxuICAgIFJFQURfQ0FMRU5EQVI6ICdhbmRyb2lkLnBlcm1pc3Npb24uUkVBRF9DQUxFTkRBUicsXG4gICAgUkVBRF9DQUxMX0xPRzogJ2FuZHJvaWQucGVybWlzc2lvbi5SRUFEX0NBTExfTE9HJyxcbiAgICBSRUFEX0NPTlRBQ1RTOiAnYW5kcm9pZC5wZXJtaXNzaW9uLlJFQURfQ09OVEFDVFMnLFxuICAgIFJFQURfRVhURVJOQUxfU1RPUkFHRTogJ2FuZHJvaWQucGVybWlzc2lvbi5SRUFEX0VYVEVSTkFMX1NUT1JBR0UnLFxuICAgIFJFQURfRlJBTUVfQlVGRkVSOiAnYW5kcm9pZC5wZXJtaXNzaW9uLlJFQURfRlJBTUVfQlVGRkVSJyxcbiAgICBSRUFEX0hJU1RPUllfQk9PS01BUktTOiAnY29tLmFuZHJvaWQuYnJvd3Nlci5wZXJtaXNzaW9uLlJFQURfSElTVE9SWV9CT09LTUFSS1MnLFxuICAgIFJFQURfSU5QVVRfU1RBVEU6ICdhbmRyb2lkLnBlcm1pc3Npb24uUkVBRF9JTlBVVF9TVEFURScsXG4gICAgUkVBRF9MT0dTOiAnYW5kcm9pZC5wZXJtaXNzaW9uLlJFQURfTE9HUycsXG4gICAgUkVBRF9QSE9ORV9TVEFURTogJ2FuZHJvaWQucGVybWlzc2lvbi5SRUFEX1BIT05FX1NUQVRFJyxcbiAgICBSRUFEX1BST0ZJTEU6ICdhbmRyb2lkLnBlcm1pc3Npb24uUkVBRF9QUk9GSUxFJyxcbiAgICBSRUFEX1NNUzogJ2FuZHJvaWQucGVybWlzc2lvbi5SRUFEX1NNUycsXG4gICAgUkVBRF9TT0NJQUxfU1RSRUFNOiAnYW5kcm9pZC5wZXJtaXNzaW9uLlJFQURfU09DSUFMX1NUUkVBTScsXG4gICAgUkVBRF9TWU5DX1NFVFRJTkdTOiAnYW5kcm9pZC5wZXJtaXNzaW9uLlJFQURfU1lOQ19TRVRUSU5HUycsXG4gICAgUkVBRF9TWU5DX1NUQVRTOiAnYW5kcm9pZC5wZXJtaXNzaW9uLlJFQURfU1lOQ19TVEFUUycsXG4gICAgUkVBRF9VU0VSX0RJQ1RJT05BUlk6ICdhbmRyb2lkLnBlcm1pc3Npb24uUkVBRF9VU0VSX0RJQ1RJT05BUlknLFxuICAgIFJFQURfVk9JQ0VNQUlMOiAnY29tLmFuZHJvaWQudm9pY2VtYWlsLnBlcm1pc3Npb24uUkVBRF9WT0lDRU1BSUwnLFxuICAgIFJFQk9PVDogJ2FuZHJvaWQucGVybWlzc2lvbi5SRUJPT1QnLFxuICAgIFJFQ0VJVkVfQk9PVF9DT01QTEVURUQ6ICdhbmRyb2lkLnBlcm1pc3Npb24uUkVDRUlWRV9CT09UX0NPTVBMRVRFRCcsXG4gICAgUkVDRUlWRV9NTVM6ICdhbmRyb2lkLnBlcm1pc3Npb24uUkVDRUlWRV9NTVMnLFxuICAgIFJFQ0VJVkVfU01TOiAnYW5kcm9pZC5wZXJtaXNzaW9uLlJFQ0VJVkVfU01TJyxcbiAgICBSRUNFSVZFX1dBUF9QVVNIOiAnYW5kcm9pZC5wZXJtaXNzaW9uLlJFQ0VJVkVfV0FQX1BVU0gnLFxuICAgIFJFQ09SRF9BVURJTzogJ2FuZHJvaWQucGVybWlzc2lvbi5SRUNPUkRfQVVESU8nLFxuICAgIFJFT1JERVJfVEFTS1M6ICdhbmRyb2lkLnBlcm1pc3Npb24uUkVPUkRFUl9UQVNLUycsXG4gICAgUkVTVEFSVF9QQUNLQUdFUzogJ2FuZHJvaWQucGVybWlzc2lvbi5SRVNUQVJUX1BBQ0tBR0VTJyxcbiAgICBTRU5EX1JFU1BPTkRfVklBX01FU1NBR0U6ICdhbmRyb2lkLnBlcm1pc3Npb24uU0VORF9SRVNQT05EX1ZJQV9NRVNTQUdFJyxcbiAgICBTRU5EX1NNUzogJ2FuZHJvaWQucGVybWlzc2lvbi5TRU5EX1NNUycsXG4gICAgU0VUX0FDVElWSVRZX1dBVENIRVI6ICdhbmRyb2lkLnBlcm1pc3Npb24uU0VUX0FDVElWSVRZX1dBVENIRVInLFxuICAgIFNFVF9BTEFSTTogJ2NvbS5hbmRyb2lkLmFsYXJtLnBlcm1pc3Npb24uU0VUX0FMQVJNJyxcbiAgICBTRVRfQUxXQVlTX0ZJTklTSDogJ2FuZHJvaWQucGVybWlzc2lvbi5TRVRfQUxXQVlTX0ZJTklTSCcsXG4gICAgU0VUX0FOSU1BVElPTl9TQ0FMRTogJ2FuZHJvaWQucGVybWlzc2lvbi5TRVRfQU5JTUFUSU9OX1NDQUxFJyxcbiAgICBTRVRfREVCVUdfQVBQOiAnYW5kcm9pZC5wZXJtaXNzaW9uLlNFVF9ERUJVR19BUFAnLFxuICAgIFNFVF9PUklFTlRBVElPTjogJ2FuZHJvaWQucGVybWlzc2lvbi5TRVRfT1JJRU5UQVRJT04nLFxuICAgIFNFVF9QT0lOVEVSX1NQRUVEOiAnYW5kcm9pZC5wZXJtaXNzaW9uLlNFVF9QT0lOVEVSX1NQRUVEJyxcbiAgICBTRVRfUFJFRkVSUkVEX0FQUExJQ0FUSU9OUzogJ2FuZHJvaWQucGVybWlzc2lvbi5TRVRfUFJFRkVSUkVEX0FQUExJQ0FUSU9OUycsXG4gICAgU0VUX1BST0NFU1NfTElNSVQ6ICdhbmRyb2lkLnBlcm1pc3Npb24uU0VUX1BST0NFU1NfTElNSVQnLFxuICAgIFNFVF9USU1FOiAnYW5kcm9pZC5wZXJtaXNzaW9uLlNFVF9USU1FJyxcbiAgICBTRVRfVElNRV9aT05FOiAnYW5kcm9pZC5wZXJtaXNzaW9uLlNFVF9USU1FX1pPTkUnLFxuICAgIFNFVF9XQUxMUEFQRVI6ICdhbmRyb2lkLnBlcm1pc3Npb24uU0VUX1dBTExQQVBFUicsXG4gICAgU0VUX1dBTExQQVBFUl9ISU5UUzogJ2FuZHJvaWQucGVybWlzc2lvbi5TRVRfV0FMTFBBUEVSX0hJTlRTJyxcbiAgICBTSUdOQUxfUEVSU0lTVEVOVF9QUk9DRVNTRVM6ICdhbmRyb2lkLnBlcm1pc3Npb24uU0lHTkFMX1BFUlNJU1RFTlRfUFJPQ0VTU0VTJyxcbiAgICBTVEFUVVNfQkFSOiAnYW5kcm9pZC5wZXJtaXNzaW9uLlNUQVRVU19CQVInLFxuICAgIFNVQlNDUklCRURfRkVFRFNfUkVBRDogJ2FuZHJvaWQucGVybWlzc2lvbi5TVUJTQ1JJQkVEX0ZFRURTX1JFQUQnLFxuICAgIFNVQlNDUklCRURfRkVFRFNfV1JJVEU6ICdhbmRyb2lkLnBlcm1pc3Npb24uU1VCU0NSSUJFRF9GRUVEU19XUklURScsXG4gICAgU1lTVEVNX0FMRVJUX1dJTkRPVzogJ2FuZHJvaWQucGVybWlzc2lvbi5TWVNURU1fQUxFUlRfV0lORE9XJyxcbiAgICBUUkFOU01JVF9JUjogJ2FuZHJvaWQucGVybWlzc2lvbi5UUkFOU01JVF9JUicsXG4gICAgVU5JTlNUQUxMX1NIT1JUQ1VUOiAnY29tLmFuZHJvaWQubGF1bmNoZXIucGVybWlzc2lvbi5VTklOU1RBTExfU0hPUlRDVVQnLFxuICAgIFVQREFURV9ERVZJQ0VfU1RBVFM6ICdhbmRyb2lkLnBlcm1pc3Npb24uVVBEQVRFX0RFVklDRV9TVEFUUycsXG4gICAgVVNFX0NSRURFTlRJQUxTOiAnYW5kcm9pZC5wZXJtaXNzaW9uLlVTRV9DUkVERU5USUFMUycsXG4gICAgVVNFX1NJUDogJ2FuZHJvaWQucGVybWlzc2lvbi5VU0VfU0lQJyxcbiAgICBWSUJSQVRFOiAnYW5kcm9pZC5wZXJtaXNzaW9uLlZJQlJBVEUnLFxuICAgIFdBS0VfTE9DSzogJ2FuZHJvaWQucGVybWlzc2lvbi5XQUtFX0xPQ0snLFxuICAgIFdSSVRFX0FQTl9TRVRUSU5HUzogJ2FuZHJvaWQucGVybWlzc2lvbi5XUklURV9BUE5fU0VUVElOR1MnLFxuICAgIFdSSVRFX0NBTEVOREFSOiAnYW5kcm9pZC5wZXJtaXNzaW9uLldSSVRFX0NBTEVOREFSJyxcbiAgICBXUklURV9DQUxMX0xPRzogJ2FuZHJvaWQucGVybWlzc2lvbi5XUklURV9DQUxMX0xPRycsXG4gICAgV1JJVEVfQ09OVEFDVFM6ICdhbmRyb2lkLnBlcm1pc3Npb24uV1JJVEVfQ09OVEFDVFMnLFxuICAgIFdSSVRFX0VYVEVSTkFMX1NUT1JBR0U6ICdhbmRyb2lkLnBlcm1pc3Npb24uV1JJVEVfRVhURVJOQUxfU1RPUkFHRScsXG4gICAgV1JJVEVfR1NFUlZJQ0VTOiAnYW5kcm9pZC5wZXJtaXNzaW9uLldSSVRFX0dTRVJWSUNFUycsXG4gICAgV1JJVEVfSElTVE9SWV9CT09LTUFSS1M6ICdjb20uYW5kcm9pZC5icm93c2VyLnBlcm1pc3Npb24uV1JJVEVfSElTVE9SWV9CT09LTUFSS1MnLFxuICAgIFdSSVRFX1BST0ZJTEU6ICdhbmRyb2lkLnBlcm1pc3Npb24uV1JJVEVfUFJPRklMRScsXG4gICAgV1JJVEVfU0VDVVJFX1NFVFRJTkdTOiAnYW5kcm9pZC5wZXJtaXNzaW9uLldSSVRFX1NFQ1VSRV9TRVRUSU5HUycsXG4gICAgV1JJVEVfU0VUVElOR1M6ICdhbmRyb2lkLnBlcm1pc3Npb24uV1JJVEVfU0VUVElOR1MnLFxuICAgIFdSSVRFX1NNUzogJ2FuZHJvaWQucGVybWlzc2lvbi5XUklURV9TTVMnLFxuICAgIFdSSVRFX1NPQ0lBTF9TVFJFQU06ICdhbmRyb2lkLnBlcm1pc3Npb24uV1JJVEVfU09DSUFMX1NUUkVBTScsXG4gICAgV1JJVEVfU1lOQ19TRVRUSU5HUzogJ2FuZHJvaWQucGVybWlzc2lvbi5XUklURV9TWU5DX1NFVFRJTkdTJyxcbiAgICBXUklURV9VU0VSX0RJQ1RJT05BUlk6ICdhbmRyb2lkLnBlcm1pc3Npb24uV1JJVEVfVVNFUl9ESUNUSU9OQVJZJyxcbiAgICBXUklURV9WT0lDRU1BSUw6ICdjb20uYW5kcm9pZC52b2ljZW1haWwucGVybWlzc2lvbi5XUklURV9WT0lDRU1BSUwnLFxuICB9O1xuXG4gIC8qKlxuICAgKiBDaGVjayBwZXJtaXNzaW9uXG4gICAqIEBwYXJhbSB7c3RyaW5nfSBwZXJtaXNzaW9uIFRoZSBuYW1lIG9mIHRoZSBwZXJtaXNzaW9uXG4gICAqIEByZXR1cm4ge1Byb21pc2U8QW5kcm9pZFBlcm1pc3Npb25SZXNwb25zZT59IFJldHVybnMgYSBwcm9taXNlXG4gICAqL1xuICBAQ29yZG92YSgpXG4gIGNoZWNrUGVybWlzc2lvbihwZXJtaXNzaW9uOiBzdHJpbmcpOiBQcm9taXNlPEFuZHJvaWRQZXJtaXNzaW9uUmVzcG9uc2U+IHtcbiAgICByZXR1cm47XG4gIH1cblxuICAvKipcbiAgICogUmVxdWVzdCBwZXJtaXNzaW9uXG4gICAqIEBwYXJhbSB7c3RyaW5nfSBwZXJtaXNzaW9uIFRoZSBuYW1lIG9mIHRoZSBwZXJtaXNzaW9uIHRvIHJlcXVlc3RcbiAgICogQHJldHVybiB7UHJvbWlzZTxBbmRyb2lkUGVybWlzc2lvblJlc3BvbnNlPn1cbiAgICovXG4gIEBDb3Jkb3ZhKClcbiAgcmVxdWVzdFBlcm1pc3Npb24ocGVybWlzc2lvbjogc3RyaW5nKTogUHJvbWlzZTxBbmRyb2lkUGVybWlzc2lvblJlc3BvbnNlPiB7XG4gICAgcmV0dXJuO1xuICB9XG5cbiAgLyoqXG4gICAqIFJlcXVlc3QgcGVybWlzc2lvbnNcbiAgICogQHBhcmFtIHtzdHJpbmdbXX0gcGVybWlzc2lvbnMgQW4gYXJyYXkgd2l0aCBwZXJtaXNzaW9uc1xuICAgKiBAcmV0dXJuIHtQcm9taXNlPGFueT59IFJldHVybnMgYSBwcm9taXNlXG4gICAqL1xuICBAQ29yZG92YSgpXG4gIHJlcXVlc3RQZXJtaXNzaW9ucyhwZXJtaXNzaW9uczogc3RyaW5nW10pOiBQcm9taXNlPGFueT4ge1xuICAgIHJldHVybjtcbiAgfVxuXG4gIC8qKlxuICAgKiBUaGlzIGZ1bmN0aW9uIHN0aWxsIHdvcmtzIG5vdywgd2lsbCBub3Qgc3VwcG9ydCBpbiB0aGUgZnV0dXJlLlxuICAgKiBAcGFyYW0ge3N0cmluZ30gcGVybWlzc2lvbiBUaGUgbmFtZSBvZiB0aGUgcGVybWlzc2lvblxuICAgKiBAcmV0dXJuIHtQcm9taXNlPEFuZHJvaWRQZXJtaXNzaW9uUmVzcG9uc2U+fSBSZXR1cm5zIGEgcHJvbWlzZVxuICAgKi9cbiAgQENvcmRvdmEoKVxuICBoYXNQZXJtaXNzaW9uKHBlcm1pc3Npb246IHN0cmluZyk6IFByb21pc2U8QW5kcm9pZFBlcm1pc3Npb25SZXNwb25zZT4ge1xuICAgIHJldHVybjtcbiAgfVxufVxuXG5leHBvcnQgaW50ZXJmYWNlIEFuZHJvaWRQZXJtaXNzaW9uUmVzcG9uc2Uge1xuICBoYXNQZXJtaXNzaW9uOiBib29sZWFuO1xufVxuIl19

/***/ }),

/***/ "./node_modules/@ionic-native/device-accounts/__ivy_ngcc__/ngx/index.js":
/*!******************************************************************************!*\
  !*** ./node_modules/@ionic-native/device-accounts/__ivy_ngcc__/ngx/index.js ***!
  \******************************************************************************/
/*! exports provided: DeviceAccounts */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DeviceAccounts", function() { return DeviceAccounts; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _ionic_native_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic-native/core */ "./node_modules/@ionic-native/core/__ivy_ngcc__/index.js");




var DeviceAccounts = /** @class */ (function (_super) {
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"])(DeviceAccounts, _super);
    function DeviceAccounts() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    DeviceAccounts.prototype.get = function () { return Object(_ionic_native_core__WEBPACK_IMPORTED_MODULE_2__["cordova"])(this, "get", {}, arguments); };
    DeviceAccounts.prototype.getByType = function (type) { return Object(_ionic_native_core__WEBPACK_IMPORTED_MODULE_2__["cordova"])(this, "getByType", {}, arguments); };
    DeviceAccounts.prototype.getEmails = function () { return Object(_ionic_native_core__WEBPACK_IMPORTED_MODULE_2__["cordova"])(this, "getEmails", {}, arguments); };
    DeviceAccounts.prototype.getEmail = function () { return Object(_ionic_native_core__WEBPACK_IMPORTED_MODULE_2__["cordova"])(this, "getEmail", {}, arguments); };
    DeviceAccounts.prototype.getPermissions = function () { return Object(_ionic_native_core__WEBPACK_IMPORTED_MODULE_2__["cordova"])(this, "getPermissions", {}, arguments); };
    DeviceAccounts.prototype.getPermissionsByType = function (type) { return Object(_ionic_native_core__WEBPACK_IMPORTED_MODULE_2__["cordova"])(this, "getPermissionsByType", {}, arguments); };
    DeviceAccounts.pluginName = "DeviceAccounts";
    DeviceAccounts.plugin = "cordova-device-accounts-v2";
    DeviceAccounts.pluginRef = "DeviceAccounts";
    DeviceAccounts.repo = "https://github.com/xUnholy/cordova-device-accounts-v2";
    DeviceAccounts.platforms = ["Android"];
DeviceAccounts.ɵfac = function DeviceAccounts_Factory(t) { return ɵDeviceAccounts_BaseFactory(t || DeviceAccounts); };
DeviceAccounts.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineInjectable"]({ token: DeviceAccounts, factory: function (t) { return DeviceAccounts.ɵfac(t); } });
var ɵDeviceAccounts_BaseFactory = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetInheritedFactory"](DeviceAccounts);
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵsetClassMetadata"](DeviceAccounts, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"]
    }], null, null); })();
    return DeviceAccounts;
}(_ionic_native_core__WEBPACK_IMPORTED_MODULE_2__["IonicNativePlugin"]));


//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3NyYy9AaW9uaWMtbmF0aXZlL3BsdWdpbnMvZGV2aWNlLWFjY291bnRzL25neC9pbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUMzQyxPQUFPLDhCQUFzQyxNQUFNLG9CQUFvQixDQUFDOztBQUN4RTtBQUVxQixJQXVDZSxrQ0FBaUI7QUFBQztBQUU5QjtBQUNnQjtBQUNwQyxJQUVGLDRCQUFHO0FBS0ksSUFLUCxrQ0FBUyxhQUFDLElBQVk7QUFLckIsSUFJRCxrQ0FBUztBQUtZLElBSXJCLGlDQUFRO0FBS2MsSUFJdEIsdUNBQWM7QUFLYyxJQUs1Qiw2Q0FBb0IsYUFBQyxJQUFZO0FBSU47QUFBa0Q7QUFBMEQ7QUFBaUQ7QUFBbUY7SUF6RGhRLGNBQWMsd0JBRDFCLFVBQVUsRUFBRSxRQUNBLGNBQWM7Ozs7OzBCQUFFO0FBQUMseUJBM0M5QjtBQUFFLEVBMkNrQyxpQkFBaUI7QUFDcEQsU0FEWSxjQUFjO0FBQUkiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBDb3Jkb3ZhLCBJb25pY05hdGl2ZVBsdWdpbiwgUGx1Z2luIH0gZnJvbSAnQGlvbmljLW5hdGl2ZS9jb3JlJztcblxuZXhwb3J0IGludGVyZmFjZSBBbmRyb2lkQWNjb3VudCB7XG4gIC8qKiBBY2NvdW50IGNyZWF0b3IgKi9cbiAgQ1JFQVRPUjogQW5kcm9pZEFjY291bnQ7XG5cbiAgLyoqIEFjY291bnQgbmFtZSAqL1xuICBuYW1lOiBzdHJpbmc7XG5cbiAgLyoqIEFjY291bnQgdHlwZSAqL1xuICB0eXBlOiBzdHJpbmc7XG59XG5cbi8qKlxuICogQG5hbWUgRGV2aWNlIEFjY291bnRzXG4gKiBAZGVzY3JpcHRpb25cbiAqIEdldHMgdGhlIEdvb2dsZSBhY2NvdW50cyBhc3NvY2lhdGVkIHdpdGggdGhlIEFuZHJvaWQgZGV2aWNlXG4gKlxuICogQHVzYWdlXG4gKiBgYGB0eXBlc2NyaXB0XG4gKiBpbXBvcnQgeyBEZXZpY2VBY2NvdW50cyB9IGZyb20gJ0Bpb25pYy1uYXRpdmUvZGV2aWNlLWFjY291bnRzL25neCc7XG4gKlxuICogY29uc3RydWN0b3IocHJpdmF0ZSBkZXZpY2VBY2NvdW50czogRGV2aWNlQWNjb3VudHMpIHsgfVxuICpcbiAqIC4uLlxuICpcbiAqIHRoaXMuZGV2aWNlQWNjb3VudHMuZ2V0KClcbiAqICAgLnRoZW4oYWNjb3VudHMgPT4gY29uc29sZS5sb2coYWNjb3VudHMpKVxuICogICAuY2F0Y2goZXJyb3IgPT4gY29uc29sZS5lcnJvcihlcnJvcikpO1xuICpcbiAqIGBgYFxuICogQGludGVyZmFjZXNcbiAqIEFuZHJvaWRBY2NvdW50XG4gKi9cbkBQbHVnaW4oe1xuICBwbHVnaW5OYW1lOiAnRGV2aWNlQWNjb3VudHMnLFxuICBwbHVnaW46ICdjb3Jkb3ZhLWRldmljZS1hY2NvdW50cy12MicsXG4gIHBsdWdpblJlZjogJ0RldmljZUFjY291bnRzJyxcbiAgcmVwbzogJ2h0dHBzOi8vZ2l0aHViLmNvbS94VW5ob2x5L2NvcmRvdmEtZGV2aWNlLWFjY291bnRzLXYyJyxcbiAgcGxhdGZvcm1zOiBbJ0FuZHJvaWQnXSxcbn0pXG5ASW5qZWN0YWJsZSgpXG5leHBvcnQgY2xhc3MgRGV2aWNlQWNjb3VudHMgZXh0ZW5kcyBJb25pY05hdGl2ZVBsdWdpbiB7XG4gIC8qKlxuICAgKiAgR2V0cyBhbGwgYWNjb3VudHMgcmVnaXN0ZXJlZCBvbiB0aGUgQW5kcm9pZCBEZXZpY2VcbiAgICogQHJldHVybnMge1Byb21pc2U8QW5kcm9pZEFjY291bnRbXT59XG4gICAqL1xuICBAQ29yZG92YSgpXG4gIGdldCgpOiBQcm9taXNlPEFuZHJvaWRBY2NvdW50W10+IHtcbiAgICByZXR1cm47XG4gIH1cblxuICAvKipcbiAgICogIEdldCBhbGwgYWNjb3VudHMgcmVnaXN0ZXJlZCBvbiBBbmRyb2lkIGRldmljZSBmb3IgcmVxdWVzdGVkIHR5cGVcbiAgICogQHBhcmFtIHtzdHJpbmd9IHR5cGVcbiAgICogQHJldHVybnMge1Byb21pc2U8QW5kcm9pZEFjY291bnRbXT59XG4gICAqL1xuICBAQ29yZG92YSgpXG4gIGdldEJ5VHlwZSh0eXBlOiBzdHJpbmcpOiBQcm9taXNlPEFuZHJvaWRBY2NvdW50W10+IHtcbiAgICByZXR1cm47XG4gIH1cblxuICAvKipcbiAgICogIEdldCBhbGwgZW1haWxzIHJlZ2lzdGVyZWQgb24gQW5kcm9pZCBkZXZpY2UgKGFjY291bnRzIHdpdGggJ2NvbS5nb29nbGUnIHR5cGUpXG4gICAqIEByZXR1cm5zIHtQcm9taXNlPHN0cmluZ1tdPn1cbiAgICovXG4gIEBDb3Jkb3ZhKClcbiAgZ2V0RW1haWxzKCk6IFByb21pc2U8c3RyaW5nW10+IHtcbiAgICByZXR1cm47XG4gIH1cblxuICAvKipcbiAgICogIEdldCB0aGUgZmlyc3QgZW1haWwgcmVnaXN0ZXJlZCBvbiBBbmRyb2lkIGRldmljZVxuICAgKiBAcmV0dXJucyB7UHJvbWlzZTxzdHJpbmc+fVxuICAgKi9cbiAgQENvcmRvdmEoKVxuICBnZXRFbWFpbCgpOiBQcm9taXNlPHN0cmluZz4ge1xuICAgIHJldHVybjtcbiAgfVxuXG4gIC8qKlxuICAgKiAgR2V0IHBlcm1pc3Npb25zIGZvciBhY2Nlc3MgdG8gZW1haWwgcmVnaXN0ZXJlZCBvbiBBbmRyb2lkIGRldmljZSA4LjArXG4gICAqIEByZXR1cm5zIHtQcm9taXNlPHN0cmluZz59XG4gICAqL1xuICBAQ29yZG92YSgpXG4gIGdldFBlcm1pc3Npb25zKCk6IFByb21pc2U8c3RyaW5nPiB7XG4gICAgcmV0dXJuO1xuICB9XG5cbiAgLyoqXG4gICAqICBHZXQgcGVybWlzc2lvbnMgZm9yIGFjY2VzcyB0byBlbWFpbCByZWdpc3RlcmVkIG9uIEFuZHJvaWQgZGV2aWNlIDguMCsgZm9yIHJlcXVlc3RlZCB0eXBlXG4gICAqIEBwYXJhbSB7c3RyaW5nfSB0eXBlXG4gICAqIEByZXR1cm5zIHtQcm9taXNlPHN0cmluZz59XG4gICAqL1xuICBAQ29yZG92YSgpXG4gIGdldFBlcm1pc3Npb25zQnlUeXBlKHR5cGU6IHN0cmluZyk6IFByb21pc2U8c3RyaW5nPiB7XG4gICAgcmV0dXJuO1xuICB9XG59XG4iXX0=

/***/ }),

/***/ "./node_modules/@ionic-native/device/__ivy_ngcc__/ngx/index.js":
/*!*********************************************************************!*\
  !*** ./node_modules/@ionic-native/device/__ivy_ngcc__/ngx/index.js ***!
  \*********************************************************************/
/*! exports provided: Device */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Device", function() { return Device; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _ionic_native_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic-native/core */ "./node_modules/@ionic-native/core/__ivy_ngcc__/index.js");




var Device = /** @class */ (function (_super) {
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"])(Device, _super);
    function Device() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Object.defineProperty(Device.prototype, "cordova", {
        get: function () { return Object(_ionic_native_core__WEBPACK_IMPORTED_MODULE_2__["cordovaPropertyGet"])(this, "cordova"); },
        set: function (value) { Object(_ionic_native_core__WEBPACK_IMPORTED_MODULE_2__["cordovaPropertySet"])(this, "cordova", value); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Device.prototype, "model", {
        get: function () { return Object(_ionic_native_core__WEBPACK_IMPORTED_MODULE_2__["cordovaPropertyGet"])(this, "model"); },
        set: function (value) { Object(_ionic_native_core__WEBPACK_IMPORTED_MODULE_2__["cordovaPropertySet"])(this, "model", value); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Device.prototype, "platform", {
        get: function () { return Object(_ionic_native_core__WEBPACK_IMPORTED_MODULE_2__["cordovaPropertyGet"])(this, "platform"); },
        set: function (value) { Object(_ionic_native_core__WEBPACK_IMPORTED_MODULE_2__["cordovaPropertySet"])(this, "platform", value); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Device.prototype, "uuid", {
        get: function () { return Object(_ionic_native_core__WEBPACK_IMPORTED_MODULE_2__["cordovaPropertyGet"])(this, "uuid"); },
        set: function (value) { Object(_ionic_native_core__WEBPACK_IMPORTED_MODULE_2__["cordovaPropertySet"])(this, "uuid", value); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Device.prototype, "version", {
        get: function () { return Object(_ionic_native_core__WEBPACK_IMPORTED_MODULE_2__["cordovaPropertyGet"])(this, "version"); },
        set: function (value) { Object(_ionic_native_core__WEBPACK_IMPORTED_MODULE_2__["cordovaPropertySet"])(this, "version", value); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Device.prototype, "manufacturer", {
        get: function () { return Object(_ionic_native_core__WEBPACK_IMPORTED_MODULE_2__["cordovaPropertyGet"])(this, "manufacturer"); },
        set: function (value) { Object(_ionic_native_core__WEBPACK_IMPORTED_MODULE_2__["cordovaPropertySet"])(this, "manufacturer", value); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Device.prototype, "isVirtual", {
        get: function () { return Object(_ionic_native_core__WEBPACK_IMPORTED_MODULE_2__["cordovaPropertyGet"])(this, "isVirtual"); },
        set: function (value) { Object(_ionic_native_core__WEBPACK_IMPORTED_MODULE_2__["cordovaPropertySet"])(this, "isVirtual", value); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Device.prototype, "serial", {
        get: function () { return Object(_ionic_native_core__WEBPACK_IMPORTED_MODULE_2__["cordovaPropertyGet"])(this, "serial"); },
        set: function (value) { Object(_ionic_native_core__WEBPACK_IMPORTED_MODULE_2__["cordovaPropertySet"])(this, "serial", value); },
        enumerable: true,
        configurable: true
    });
    Device.pluginName = "Device";
    Device.plugin = "cordova-plugin-device";
    Device.pluginRef = "device";
    Device.repo = "https://github.com/apache/cordova-plugin-device";
    Device.platforms = ["Android", "Browser", "iOS", "macOS", "Windows"];
Device.ɵfac = function Device_Factory(t) { return ɵDevice_BaseFactory(t || Device); };
Device.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineInjectable"]({ token: Device, factory: function (t) { return Device.ɵfac(t); } });
var ɵDevice_BaseFactory = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetInheritedFactory"](Device);
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵsetClassMetadata"](Device, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"]
    }], null, null); })();
    return Device;
}(_ionic_native_core__WEBPACK_IMPORTED_MODULE_2__["IonicNativePlugin"]));


//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3NyYy9AaW9uaWMtbmF0aXZlL3BsdWdpbnMvZGV2aWNlL25neC9pbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUMzQyxPQUFPLDZEQUE4QyxNQUFNLG9CQUFvQixDQUFDOztBQUNoRjtBQUllLElBd0JhLDBCQUFpQjtBQUFDO0FBQ3hCO0FBRVA7QUFFZCwwQkFGQywyQkFBTztBQUFJO0FBR2dEO0FBQ1g7QUFBMEI7QUFFckU7QUFBUSwwQkFDYix5QkFBSztBQUFJO0FBR0s7QUFHbUM7QUFDdEM7QUFHWDtBQUFRLDBCQU5SLDRCQUFRO0FBQUk7QUFHVjtBQUlEO0FBQ0s7QUFFTztBQUFRLDBCQU5yQix3QkFBSTtBQUFJO0FBSVY7QUFHZ0I7QUFDSztBQUVFO0FBQVEsMEJBTjdCLDJCQUFPO0FBQUk7QUFJUDtBQUdEO0FBQ0s7QUFFSTtBQUFRLDBCQU5wQixnQ0FBWTtBQUFJO0FBR0Q7QUFJUjtBQUNLO0FBRU87QUFBUSwwQkFOM0IsNkJBQVM7QUFBSTtBQUdLO0FBR3dDO0FBQTBCO0FBQTJCO0FBQVEsMEJBRnZILDBCQUFNO0FBQUk7QUFFcUQ7QUFBOEU7QUFBMEI7QUFBMkI7QUFBUTtBQUFrQztBQUE2QztBQUFpQztBQUFxRTtJQXBDcFgsTUFBTSx3QkFEbEIsVUFBVSxFQUFFLFFBQ0EsTUFBTTs7Ozs7MEJBQUU7QUFBQyxpQkE5QnRCO0FBQUUsRUE4QjBCLGlCQUFpQjtBQUM1QyxTQURZLE1BQU07QUFBSSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IENvcmRvdmFQcm9wZXJ0eSwgSW9uaWNOYXRpdmVQbHVnaW4sIFBsdWdpbiB9IGZyb20gJ0Bpb25pYy1uYXRpdmUvY29yZSc7XG5cbmRlY2xhcmUgY29uc3Qgd2luZG93OiBhbnk7XG5cbi8qKlxuICogQG5hbWUgRGV2aWNlXG4gKiBAcHJlbWllciBkZXZpY2VcbiAqIEBkZXNjcmlwdGlvblxuICogQWNjZXNzIGluZm9ybWF0aW9uIGFib3V0IHRoZSB1bmRlcmx5aW5nIGRldmljZSBhbmQgcGxhdGZvcm0uXG4gKlxuICogQHVzYWdlXG4gKiBgYGB0eXBlc2NyaXB0XG4gKiBpbXBvcnQgeyBEZXZpY2UgfSBmcm9tICdAaW9uaWMtbmF0aXZlL2RldmljZS9uZ3gnO1xuICpcbiAqIGNvbnN0cnVjdG9yKHByaXZhdGUgZGV2aWNlOiBEZXZpY2UpIHsgfVxuICpcbiAqIC4uLlxuICpcbiAqIGNvbnNvbGUubG9nKCdEZXZpY2UgVVVJRCBpczogJyArIHRoaXMuZGV2aWNlLnV1aWQpO1xuICogYGBgXG4gKi9cbkBQbHVnaW4oe1xuICBwbHVnaW5OYW1lOiAnRGV2aWNlJyxcbiAgcGx1Z2luOiAnY29yZG92YS1wbHVnaW4tZGV2aWNlJyxcbiAgcGx1Z2luUmVmOiAnZGV2aWNlJyxcbiAgcmVwbzogJ2h0dHBzOi8vZ2l0aHViLmNvbS9hcGFjaGUvY29yZG92YS1wbHVnaW4tZGV2aWNlJyxcbiAgcGxhdGZvcm1zOiBbJ0FuZHJvaWQnLCAnQnJvd3NlcicsICdpT1MnLCAnbWFjT1MnLCAnV2luZG93cyddLFxufSlcbkBJbmplY3RhYmxlKClcbmV4cG9ydCBjbGFzcyBEZXZpY2UgZXh0ZW5kcyBJb25pY05hdGl2ZVBsdWdpbiB7XG4gIC8qKiBHZXQgdGhlIHZlcnNpb24gb2YgQ29yZG92YSBydW5uaW5nIG9uIHRoZSBkZXZpY2UuICovXG4gIEBDb3Jkb3ZhUHJvcGVydHkoKVxuICBjb3Jkb3ZhOiBzdHJpbmc7XG5cbiAgLyoqXG4gICAqIFRoZSBkZXZpY2UubW9kZWwgcmV0dXJucyB0aGUgbmFtZSBvZiB0aGUgZGV2aWNlJ3MgbW9kZWwgb3IgcHJvZHVjdC4gVGhlIHZhbHVlIGlzIHNldFxuICAgKiBieSB0aGUgZGV2aWNlIG1hbnVmYWN0dXJlciBhbmQgbWF5IGJlIGRpZmZlcmVudCBhY3Jvc3MgdmVyc2lvbnMgb2YgdGhlIHNhbWUgcHJvZHVjdC5cbiAgICovXG4gIEBDb3Jkb3ZhUHJvcGVydHkoKVxuICBtb2RlbDogc3RyaW5nO1xuXG4gIC8qKiBHZXQgdGhlIGRldmljZSdzIG9wZXJhdGluZyBzeXN0ZW0gbmFtZS4gKi9cbiAgQENvcmRvdmFQcm9wZXJ0eSgpXG4gIHBsYXRmb3JtOiBzdHJpbmc7XG5cbiAgLyoqIEdldCB0aGUgZGV2aWNlJ3MgVW5pdmVyc2FsbHkgVW5pcXVlIElkZW50aWZpZXIgKFVVSUQpLiAqL1xuICBAQ29yZG92YVByb3BlcnR5KClcbiAgdXVpZDogc3RyaW5nO1xuXG4gIC8qKiBHZXQgdGhlIG9wZXJhdGluZyBzeXN0ZW0gdmVyc2lvbi4gKi9cbiAgQENvcmRvdmFQcm9wZXJ0eSgpXG4gIHZlcnNpb246IHN0cmluZztcblxuICAvKiogR2V0IHRoZSBkZXZpY2UncyBtYW51ZmFjdHVyZXIuICovXG4gIEBDb3Jkb3ZhUHJvcGVydHkoKVxuICBtYW51ZmFjdHVyZXI6IHN0cmluZztcblxuICAvKiogV2hldGhlciB0aGUgZGV2aWNlIGlzIHJ1bm5pbmcgb24gYSBzaW11bGF0b3IuICovXG4gIEBDb3Jkb3ZhUHJvcGVydHkoKVxuICBpc1ZpcnR1YWw6IGJvb2xlYW47XG5cbiAgLyoqIEdldCB0aGUgZGV2aWNlIGhhcmR3YXJlIHNlcmlhbCBudW1iZXIuICovXG4gIEBDb3Jkb3ZhUHJvcGVydHkoKVxuICBzZXJpYWw6IHN0cmluZztcbn1cbiJdfQ==

/***/ }),

/***/ "./node_modules/@ionic-native/unique-device-id/__ivy_ngcc__/ngx/index.js":
/*!*******************************************************************************!*\
  !*** ./node_modules/@ionic-native/unique-device-id/__ivy_ngcc__/ngx/index.js ***!
  \*******************************************************************************/
/*! exports provided: UniqueDeviceID */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UniqueDeviceID", function() { return UniqueDeviceID; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _ionic_native_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic-native/core */ "./node_modules/@ionic-native/core/__ivy_ngcc__/index.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");




var UniqueDeviceID = /** @class */ (function (_super) {
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"])(UniqueDeviceID, _super);
    function UniqueDeviceID() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    UniqueDeviceID.prototype.get = function () { return Object(_ionic_native_core__WEBPACK_IMPORTED_MODULE_1__["cordova"])(this, "get", {}, arguments); };
    UniqueDeviceID.pluginName = "UniqueDeviceID";
    UniqueDeviceID.plugin = "cordova-plugin-uniquedeviceid";
    UniqueDeviceID.pluginRef = "window.plugins.uniqueDeviceID";
    UniqueDeviceID.repo = "https://github.com/Paldom/UniqueDeviceID";
    UniqueDeviceID.platforms = ["Android", "iOS", "Windows Phone 8"];
UniqueDeviceID.ɵfac = function UniqueDeviceID_Factory(t) { return ɵUniqueDeviceID_BaseFactory(t || UniqueDeviceID); };
UniqueDeviceID.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineInjectable"]({ token: UniqueDeviceID, factory: function (t) { return UniqueDeviceID.ɵfac(t); } });
var ɵUniqueDeviceID_BaseFactory = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵgetInheritedFactory"](UniqueDeviceID);
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵsetClassMetadata"](UniqueDeviceID, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_2__["Injectable"]
    }], null, null); })();
    return UniqueDeviceID;
}(_ionic_native_core__WEBPACK_IMPORTED_MODULE_1__["IonicNativePlugin"]));


//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3NyYy9AaW9uaWMtbmF0aXZlL3BsdWdpbnMvdW5pcXVlLWRldmljZS1pZC9uZ3gvaW5kZXgudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sOEJBQXNDLE1BQU0sb0JBQW9CLENBQUM7QUFDeEUsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQzs7QUFDM0M7QUFJUyxJQXdCMkIsa0NBQWlCO0FBQUM7QUFFOUI7QUFDYztBQUFNLElBRzFDLDRCQUFHO0FBSW1CO0FBQWtEO0FBQTZEO0FBQWdFO0FBQXNFO0lBVmhRLGNBQWMsd0JBRDFCLFVBQVUsRUFBRSxRQUNBLGNBQWM7Ozs7OzBCQUFFO0FBQUMseUJBOUI5QjtBQUFFLEVBOEJrQyxpQkFBaUI7QUFDcEQsU0FEWSxjQUFjO0FBQUkiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb3Jkb3ZhLCBJb25pY05hdGl2ZVBsdWdpbiwgUGx1Z2luIH0gZnJvbSAnQGlvbmljLW5hdGl2ZS9jb3JlJztcbmltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuLyoqXG4gKiBAbmFtZSBVbmlxdWUgRGV2aWNlIElEXG4gKiBAZGVzY3JpcHRpb25cbiAqIFRoaXMgcGx1Z2luIHByb2R1Y2VzIGEgdW5pcXVlLCBjcm9zcy1pbnN0YWxsLCBhcHAtc3BlY2lmaWMgZGV2aWNlIGlkLlxuICpcbiAqIEB1c2FnZVxuICogYGBgdHlwZXNjcmlwdFxuICogaW1wb3J0IHsgVW5pcXVlRGV2aWNlSUQgfSBmcm9tICdAaW9uaWMtbmF0aXZlL3VuaXF1ZS1kZXZpY2UtaWQvbmd4JztcbiAqXG4gKiBjb25zdHJ1Y3Rvcihwcml2YXRlIHVuaXF1ZURldmljZUlEOiBVbmlxdWVEZXZpY2VJRCkgeyB9XG4gKlxuICogLi4uXG4gKlxuICogdGhpcy51bmlxdWVEZXZpY2VJRC5nZXQoKVxuICogICAudGhlbigodXVpZDogYW55KSA9PiBjb25zb2xlLmxvZyh1dWlkKSlcbiAqICAgLmNhdGNoKChlcnJvcjogYW55KSA9PiBjb25zb2xlLmxvZyhlcnJvcikpO1xuICpcbiAqIGBgYFxuICovXG5AUGx1Z2luKHtcbiAgcGx1Z2luTmFtZTogJ1VuaXF1ZURldmljZUlEJyxcbiAgcGx1Z2luOiAnY29yZG92YS1wbHVnaW4tdW5pcXVlZGV2aWNlaWQnLFxuICBwbHVnaW5SZWY6ICd3aW5kb3cucGx1Z2lucy51bmlxdWVEZXZpY2VJRCcsXG4gIHJlcG86ICdodHRwczovL2dpdGh1Yi5jb20vUGFsZG9tL1VuaXF1ZURldmljZUlEJyxcbiAgcGxhdGZvcm1zOiBbJ0FuZHJvaWQnLCAnaU9TJywgJ1dpbmRvd3MgUGhvbmUgOCddLFxufSlcbkBJbmplY3RhYmxlKClcbmV4cG9ydCBjbGFzcyBVbmlxdWVEZXZpY2VJRCBleHRlbmRzIElvbmljTmF0aXZlUGx1Z2luIHtcbiAgLyoqXG4gICAqIEdldHMgYSB1bmlxdWUsIGNyb3NzLWluc3RhbGwsIGFwcC1zcGVjaWZpYyBkZXZpY2UgaWQuXG4gICAqIEByZXR1cm4ge1Byb21pc2U8c3RyaW5nPn0gUmV0dXJucyBhIHByb21pc2UgdGhhdCByZXNvbHZlcyB3aGVuIHNvbWV0aGluZyBoYXBwZW5zXG4gICAqL1xuICBAQ29yZG92YSgpXG4gIGdldCgpOiBQcm9taXNlPHN0cmluZz4ge1xuICAgIHJldHVybjtcbiAgfVxufVxuIl19

/***/ }),

/***/ "./node_modules/angular-svg-round-progressbar/dist/index.js":
/*!******************************************************************!*\
  !*** ./node_modules/angular-svg-round-progressbar/dist/index.js ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __exportStar = (this && this.__exportStar) || function(m, exports) {
    for (var p in m) if (p !== "default" && !exports.hasOwnProperty(p)) __createBinding(exports, m, p);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RoundProgressModule = void 0;
var core_1 = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
var round_progress_component_1 = __webpack_require__(/*! ./round-progress.component */ "./node_modules/angular-svg-round-progressbar/dist/round-progress.component.js");
var round_progress_ease_1 = __webpack_require__(/*! ./round-progress.ease */ "./node_modules/angular-svg-round-progressbar/dist/round-progress.ease.js");
var round_progress_config_1 = __webpack_require__(/*! ./round-progress.config */ "./node_modules/angular-svg-round-progressbar/dist/round-progress.config.js");
var round_progress_service_1 = __webpack_require__(/*! ./round-progress.service */ "./node_modules/angular-svg-round-progressbar/dist/round-progress.service.js");
var i0 = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
var RoundProgressModule = /** @class */ (function () {
    function RoundProgressModule() {
    }
    RoundProgressModule.ɵmod = i0.ɵɵdefineNgModule({ type: RoundProgressModule });
    RoundProgressModule.ɵinj = i0.ɵɵdefineInjector({ factory: function RoundProgressModule_Factory(t) { return new (t || RoundProgressModule)(); }, providers: [round_progress_service_1.RoundProgressService, round_progress_ease_1.RoundProgressEase, round_progress_config_1.ROUND_PROGRESS_DEFAULTS_PROVIDER] });
    return RoundProgressModule;
}());
exports.RoundProgressModule = RoundProgressModule;
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && i0.ɵɵsetNgModuleScope(RoundProgressModule, { declarations: [round_progress_component_1.RoundProgressComponent], exports: [round_progress_component_1.RoundProgressComponent] }); })();
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(RoundProgressModule, [{
        type: core_1.NgModule,
        args: [{
                declarations: [round_progress_component_1.RoundProgressComponent],
                exports: [round_progress_component_1.RoundProgressComponent],
                providers: [round_progress_service_1.RoundProgressService, round_progress_ease_1.RoundProgressEase, round_progress_config_1.ROUND_PROGRESS_DEFAULTS_PROVIDER]
            }]
    }], null, null); })();
__exportStar(__webpack_require__(/*! ./round-progress.component */ "./node_modules/angular-svg-round-progressbar/dist/round-progress.component.js"), exports);
__exportStar(__webpack_require__(/*! ./round-progress.service */ "./node_modules/angular-svg-round-progressbar/dist/round-progress.service.js"), exports);
__exportStar(__webpack_require__(/*! ./round-progress.ease */ "./node_modules/angular-svg-round-progressbar/dist/round-progress.ease.js"), exports);
__exportStar(__webpack_require__(/*! ./round-progress.config */ "./node_modules/angular-svg-round-progressbar/dist/round-progress.config.js"), exports);
//# sourceMappingURL=index.js.map

/***/ }),

/***/ "./node_modules/angular-svg-round-progressbar/dist/round-progress.component.js":
/*!*************************************************************************************!*\
  !*** ./node_modules/angular-svg-round-progressbar/dist/round-progress.component.js ***!
  \*************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
exports.RoundProgressComponent = void 0;
var core_1 = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
var round_progress_service_1 = __webpack_require__(/*! ./round-progress.service */ "./node_modules/angular-svg-round-progressbar/dist/round-progress.service.js");
var round_progress_config_1 = __webpack_require__(/*! ./round-progress.config */ "./node_modules/angular-svg-round-progressbar/dist/round-progress.config.js");
var round_progress_ease_1 = __webpack_require__(/*! ./round-progress.ease */ "./node_modules/angular-svg-round-progressbar/dist/round-progress.ease.js");
var i0 = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
var i1 = __webpack_require__(/*! ./round-progress.service */ "./node_modules/angular-svg-round-progressbar/dist/round-progress.service.js");
var i2 = __webpack_require__(/*! ./round-progress.ease */ "./node_modules/angular-svg-round-progressbar/dist/round-progress.ease.js");
var _c0 = ["path"];
var RoundProgressComponent = /** @class */ (function () {
    function RoundProgressComponent(_service, _easing, _defaults, _ngZone) {
        this._service = _service;
        this._easing = _easing;
        this._defaults = _defaults;
        this._ngZone = _ngZone;
        /** Radius of the circle. */
        this.radius = this._defaults.radius;
        /** Name of the easing function to use when animating. */
        this.animation = this._defaults.animation;
        /** Time in millisconds by which to delay the animation. */
        this.animationDelay = this._defaults.animationDelay;
        /** Duration of the animation. */
        this.duration = this._defaults.duration;
        /** Width of the circle's stroke. */
        this.stroke = this._defaults.stroke;
        /** Color of the circle. */
        this.color = this._defaults.color;
        /** Background color of the circle. */
        this.background = this._defaults.background;
        /** Whether the circle should take up the width of its parent. */
        this.responsive = this._defaults.responsive;
        /** Whether the circle is filling up clockwise. */
        this.clockwise = this._defaults.clockwise;
        /** Whether to render a semicircle. */
        this.semicircle = this._defaults.semicircle;
        /** Whether the tip of the progress should be rounded off. */
        this.rounded = this._defaults.rounded;
        /** Emits when a new value has been rendered. */
        this.onRender = new core_1.EventEmitter();
        this._lastAnimationId = 0;
    }
    /** Animates a change in the current value. */
    RoundProgressComponent.prototype._animateChange = function (from, to) {
        var _this = this;
        if (typeof from !== 'number') {
            from = 0;
        }
        to = this._clamp(to);
        from = this._clamp(from);
        var self = this;
        var changeInValue = to - from;
        var duration = self.duration;
        // Avoid firing change detection for each of the animation frames.
        self._ngZone.runOutsideAngular(function () {
            var start = function () {
                var startTime = self._service.getTimestamp();
                var id = ++self._lastAnimationId;
                requestAnimationFrame(function animation() {
                    var currentTime = Math.min(self._service.getTimestamp() - startTime, duration);
                    var value = self._easing[self.animation](currentTime, from, changeInValue, duration);
                    self._setPath(value);
                    self.onRender.emit(value);
                    if (id === self._lastAnimationId && currentTime < duration) {
                        requestAnimationFrame(animation);
                    }
                });
            };
            if (_this.animationDelay > 0) {
                setTimeout(start, _this.animationDelay);
            }
            else {
                start();
            }
        });
    };
    /** Sets the path dimensions. */
    RoundProgressComponent.prototype._setPath = function (value) {
        if (this._path) {
            var arc = this._service.getArc(value, this.max, this.radius - this.stroke / 2, this.radius, this.semicircle);
            this._path.nativeElement.setAttribute('d', arc);
        }
    };
    /** Clamps a value between the maximum and 0. */
    RoundProgressComponent.prototype._clamp = function (value) {
        return Math.max(0, Math.min(value || 0, this.max));
    };
    /** Determines the SVG transforms for the <path> node. */
    RoundProgressComponent.prototype.getPathTransform = function () {
        var diameter = this._getDiameter();
        if (this.semicircle) {
            return this.clockwise ?
                "translate(0, " + diameter + ") rotate(-90)" :
                "translate(" + (diameter + ',' + diameter) + ") rotate(90) scale(-1, 1)";
        }
        else if (!this.clockwise) {
            return "scale(-1, 1) translate(-" + diameter + " 0)";
        }
    };
    /** Resolves a color through the service. */
    RoundProgressComponent.prototype.resolveColor = function (color) {
        return this._service.resolveColor(color);
    };
    /** Change detection callback. */
    RoundProgressComponent.prototype.ngOnChanges = function (changes) {
        if (changes.current) {
            this._animateChange(changes.current.previousValue, changes.current.currentValue);
        }
        else {
            this._setPath(this.current);
        }
    };
    /** Diameter of the circle. */
    RoundProgressComponent.prototype._getDiameter = function () {
        return this.radius * 2;
    };
    /** The CSS height of the wrapper element. */
    RoundProgressComponent.prototype._getElementHeight = function () {
        if (!this.responsive) {
            return (this.semicircle ? this.radius : this._getDiameter()) + 'px';
        }
    };
    /** Viewbox for the SVG element. */
    RoundProgressComponent.prototype._getViewBox = function () {
        var diameter = this._getDiameter();
        return "0 0 " + diameter + " " + (this.semicircle ? this.radius : diameter);
    };
    /** Bottom padding for the wrapper element. */
    RoundProgressComponent.prototype._getPaddingBottom = function () {
        if (this.responsive) {
            return this.semicircle ? '50%' : '100%';
        }
    };
    RoundProgressComponent.ɵfac = function RoundProgressComponent_Factory(t) { return new (t || RoundProgressComponent)(i0.ɵɵdirectiveInject(i1.RoundProgressService), i0.ɵɵdirectiveInject(i2.RoundProgressEase), i0.ɵɵdirectiveInject(round_progress_config_1.ROUND_PROGRESS_DEFAULTS), i0.ɵɵdirectiveInject(i0.NgZone)); };
    RoundProgressComponent.ɵcmp = i0.ɵɵdefineComponent({ type: RoundProgressComponent, selectors: [["round-progress"]], viewQuery: function RoundProgressComponent_Query(rf, ctx) { if (rf & 1) {
            i0.ɵɵviewQuery(_c0, true);
        } if (rf & 2) {
            var _t;
            i0.ɵɵqueryRefresh(_t = i0.ɵɵloadQuery()) && (ctx._path = _t.first);
        } }, hostAttrs: ["role", "progressbar"], hostVars: 10, hostBindings: function RoundProgressComponent_HostBindings(rf, ctx) { if (rf & 2) {
            i0.ɵɵattribute("aria-valuemin", ctx.current)("aria-valuemax", ctx.max);
            i0.ɵɵstyleProp("width", ctx.responsive ? "" : ctx._getDiameter() + "px")("height", ctx._getElementHeight())("padding-bottom", ctx._getPaddingBottom());
            i0.ɵɵclassProp("responsive", ctx.responsive);
        } }, inputs: { current: "current", max: "max", radius: "radius", animation: "animation", animationDelay: "animationDelay", duration: "duration", stroke: "stroke", color: "color", background: "background", responsive: "responsive", clockwise: "clockwise", semicircle: "semicircle", rounded: "rounded" }, outputs: { onRender: "onRender" }, features: [i0.ɵɵNgOnChangesFeature], decls: 4, vars: 15, consts: [["xmlns", "http://www.w3.org/2000/svg"], ["fill", "none"], ["path", ""]], template: function RoundProgressComponent_Template(rf, ctx) { if (rf & 1) {
            i0.ɵɵnamespaceSVG();
            i0.ɵɵelementStart(0, "svg", 0);
            i0.ɵɵelement(1, "circle", 1);
            i0.ɵɵelement(2, "path", 1, 2);
            i0.ɵɵelementEnd();
        } if (rf & 2) {
            i0.ɵɵattribute("viewBox", ctx._getViewBox());
            i0.ɵɵadvance(1);
            i0.ɵɵstyleProp("stroke", ctx.resolveColor(ctx.background))("stroke-width", ctx.stroke);
            i0.ɵɵattribute("cx", ctx.radius)("cy", ctx.radius)("r", ctx.radius - ctx.stroke / 2);
            i0.ɵɵadvance(1);
            i0.ɵɵstyleProp("stroke-width", ctx.stroke)("stroke", ctx.resolveColor(ctx.color))("stroke-linecap", ctx.rounded ? "round" : "");
            i0.ɵɵattribute("transform", ctx.getPathTransform());
        } }, styles: ["[_nghost-%COMP%] {\n      display: block;\n      position: relative;\n      overflow: hidden;\n    }\n    .responsive[_nghost-%COMP%] {\n      width: 100%;\n      padding-bottom: 100%;\n    }\n    .responsive[_nghost-%COMP%]    > svg[_ngcontent-%COMP%] {\n      position: absolute;\n      width: 100%;\n      height: 100%;\n      top: 0;\n      left: 0;\n    }"], changeDetection: 0 });
    return RoundProgressComponent;
}());
exports.RoundProgressComponent = RoundProgressComponent;
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(RoundProgressComponent, [{
        type: core_1.Component,
        args: [{
                selector: 'round-progress',
                changeDetection: core_1.ChangeDetectionStrategy.OnPush,
                template: "\n    <svg xmlns=\"http://www.w3.org/2000/svg\" [attr.viewBox]=\"_getViewBox()\">\n      <circle\n        fill=\"none\"\n        [attr.cx]=\"radius\"\n        [attr.cy]=\"radius\"\n        [attr.r]=\"radius - stroke / 2\"\n        [style.stroke]=\"resolveColor(background)\"\n        [style.stroke-width]=\"stroke\"/>\n\n      <path\n        #path\n        fill=\"none\"\n        [style.stroke-width]=\"stroke\"\n        [style.stroke]=\"resolveColor(color)\"\n        [style.stroke-linecap]=\"rounded ? 'round' : ''\"\n        [attr.transform]=\"getPathTransform()\"/>\n    </svg>\n  ",
                host: {
                    'role': 'progressbar',
                    '[attr.aria-valuemin]': 'current',
                    '[attr.aria-valuemax]': 'max',
                    '[style.width]': "responsive ? '' : _getDiameter() + 'px'",
                    '[style.height]': '_getElementHeight()',
                    '[style.padding-bottom]': '_getPaddingBottom()',
                    '[class.responsive]': 'responsive'
                },
                styles: ["\n    :host {\n      display: block;\n      position: relative;\n      overflow: hidden;\n    }\n    :host(.responsive) {\n      width: 100%;\n      padding-bottom: 100%;\n    }\n    :host(.responsive) > svg {\n      position: absolute;\n      width: 100%;\n      height: 100%;\n      top: 0;\n      left: 0;\n    }\n  "]
            }]
    }], function () { return [{ type: i1.RoundProgressService }, { type: i2.RoundProgressEase }, { type: undefined, decorators: [{
                type: core_1.Inject,
                args: [round_progress_config_1.ROUND_PROGRESS_DEFAULTS]
            }] }, { type: i0.NgZone }]; }, { _path: [{
            type: core_1.ViewChild,
            args: ['path', { static: false }]
        }], current: [{
            type: core_1.Input
        }], max: [{
            type: core_1.Input
        }], radius: [{
            type: core_1.Input
        }], animation: [{
            type: core_1.Input
        }], animationDelay: [{
            type: core_1.Input
        }], duration: [{
            type: core_1.Input
        }], stroke: [{
            type: core_1.Input
        }], color: [{
            type: core_1.Input
        }], background: [{
            type: core_1.Input
        }], responsive: [{
            type: core_1.Input
        }], clockwise: [{
            type: core_1.Input
        }], semicircle: [{
            type: core_1.Input
        }], rounded: [{
            type: core_1.Input
        }], onRender: [{
            type: core_1.Output
        }] }); })();
//# sourceMappingURL=round-progress.component.js.map

/***/ }),

/***/ "./node_modules/angular-svg-round-progressbar/dist/round-progress.config.js":
/*!**********************************************************************************!*\
  !*** ./node_modules/angular-svg-round-progressbar/dist/round-progress.config.js ***!
  \**********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
exports.ROUND_PROGRESS_DEFAULTS_PROVIDER = exports.ROUND_PROGRESS_DEFAULTS = void 0;
var core_1 = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
exports.ROUND_PROGRESS_DEFAULTS = new core_1.InjectionToken('ROUND_PROGRESS_DEFAULTS');
exports.ROUND_PROGRESS_DEFAULTS_PROVIDER = {
    provide: exports.ROUND_PROGRESS_DEFAULTS,
    useValue: {
        radius: 125,
        animation: 'easeOutCubic',
        animationDelay: null,
        duration: 500,
        stroke: 15,
        color: '#45CCCE',
        background: '#EAEAEA',
        responsive: false,
        clockwise: true,
        semicircle: false,
        rounded: false
    }
};
//# sourceMappingURL=round-progress.config.js.map

/***/ }),

/***/ "./node_modules/angular-svg-round-progressbar/dist/round-progress.ease.js":
/*!********************************************************************************!*\
  !*** ./node_modules/angular-svg-round-progressbar/dist/round-progress.ease.js ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
exports.RoundProgressEase = void 0;
var core_1 = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
var i0 = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
var RoundProgressEase = /** @class */ (function () {
    function RoundProgressEase() {
    }
    // t: current time (or position) of the neonate. This can be seconds or frames, steps,
    // seconds, ms, whatever – as long as the unit is the same as is used for the total time.
    // b: beginning value of the property.
    // c: change between the beginning and destination value of the property.
    // d: total time of the neonate.
    RoundProgressEase.prototype.linearEase = function (t, b, c, d) {
        return c * t / d + b;
    };
    RoundProgressEase.prototype.easeInQuad = function (t, b, c, d) {
        return c * (t /= d) * t + b;
    };
    RoundProgressEase.prototype.easeOutQuad = function (t, b, c, d) {
        return -c * (t /= d) * (t - 2) + b;
    };
    RoundProgressEase.prototype.easeInOutQuad = function (t, b, c, d) {
        if ((t /= d / 2) < 1) {
            return c / 2 * t * t + b;
        }
        return -c / 2 * ((--t) * (t - 2) - 1) + b;
    };
    RoundProgressEase.prototype.easeInCubic = function (t, b, c, d) {
        return c * (t /= d) * t * t + b;
    };
    RoundProgressEase.prototype.easeOutCubic = function (t, b, c, d) {
        return c * ((t = t / d - 1) * t * t + 1) + b;
    };
    RoundProgressEase.prototype.easeInOutCubic = function (t, b, c, d) {
        if ((t /= d / 2) < 1) {
            return c / 2 * t * t * t + b;
        }
        return c / 2 * ((t -= 2) * t * t + 2) + b;
    };
    RoundProgressEase.prototype.easeInQuart = function (t, b, c, d) {
        return c * (t /= d) * t * t * t + b;
    };
    RoundProgressEase.prototype.easeOutQuart = function (t, b, c, d) {
        return -c * ((t = t / d - 1) * t * t * t - 1) + b;
    };
    RoundProgressEase.prototype.easeInOutQuart = function (t, b, c, d) {
        if ((t /= d / 2) < 1) {
            return c / 2 * t * t * t * t + b;
        }
        return -c / 2 * ((t -= 2) * t * t * t - 2) + b;
    };
    RoundProgressEase.prototype.easeInQuint = function (t, b, c, d) {
        return c * (t /= d) * t * t * t * t + b;
    };
    RoundProgressEase.prototype.easeOutQuint = function (t, b, c, d) {
        return c * ((t = t / d - 1) * t * t * t * t + 1) + b;
    };
    RoundProgressEase.prototype.easeInOutQuint = function (t, b, c, d) {
        if ((t /= d / 2) < 1) {
            return c / 2 * t * t * t * t * t + b;
        }
        return c / 2 * ((t -= 2) * t * t * t * t + 2) + b;
    };
    RoundProgressEase.prototype.easeInSine = function (t, b, c, d) {
        return -c * Math.cos(t / d * (Math.PI / 2)) + c + b;
    };
    RoundProgressEase.prototype.easeOutSine = function (t, b, c, d) {
        return c * Math.sin(t / d * (Math.PI / 2)) + b;
    };
    RoundProgressEase.prototype.easeInOutSine = function (t, b, c, d) {
        return -c / 2 * (Math.cos(Math.PI * t / d) - 1) + b;
    };
    RoundProgressEase.prototype.easeInExpo = function (t, b, c, d) {
        return (t == 0) ? b : c * Math.pow(2, 10 * (t / d - 1)) + b;
    };
    RoundProgressEase.prototype.easeOutExpo = function (t, b, c, d) {
        return (t == d) ? b + c : c * (-Math.pow(2, -10 * t / d) + 1) + b;
    };
    RoundProgressEase.prototype.easeInOutExpo = function (t, b, c, d) {
        if (t == 0) {
            return b;
        }
        if (t == d) {
            return b + c;
        }
        if ((t /= d / 2) < 1) {
            return c / 2 * Math.pow(2, 10 * (t - 1)) + b;
        }
        return c / 2 * (-Math.pow(2, -10 * --t) + 2) + b;
    };
    RoundProgressEase.prototype.easeInCirc = function (t, b, c, d) {
        return -c * (Math.sqrt(1 - (t /= d) * t) - 1) + b;
    };
    RoundProgressEase.prototype.easeOutCirc = function (t, b, c, d) {
        return c * Math.sqrt(1 - (t = t / d - 1) * t) + b;
    };
    RoundProgressEase.prototype.easeInOutCirc = function (t, b, c, d) {
        if ((t /= d / 2) < 1) {
            return -c / 2 * (Math.sqrt(1 - t * t) - 1) + b;
        }
        return c / 2 * (Math.sqrt(1 - (t -= 2) * t) + 1) + b;
    };
    RoundProgressEase.prototype.easeInElastic = function (t, b, c, d) {
        var s = 1.70158;
        var p = d * 0.3;
        var a = c;
        if (t == 0) {
            return b;
        }
        if ((t /= d) == 1) {
            return b + c;
        }
        if (a < Math.abs(c)) {
            a = c;
            s = p / 4;
        }
        else {
            s = p / (2 * Math.PI) * Math.asin(c / a);
        }
        return -(a * Math.pow(2, 10 * (t--)) * Math.sin((t * d - s) * (2 * Math.PI) / p)) + b;
    };
    RoundProgressEase.prototype.easeOutElastic = function (t, b, c, d) {
        var s = 1.70158;
        var p = d * 0.3;
        var a = c;
        if (t == 0) {
            return b;
        }
        if ((t /= d) == 1) {
            return b + c;
        }
        if (a < Math.abs(c)) {
            a = c;
            s = p / 4;
        }
        else {
            s = p / (2 * Math.PI) * Math.asin(c / a);
        }
        return a * Math.pow(2, -10 * t) * Math.sin((t * d - s) * (2 * Math.PI) / p) + c + b;
    };
    RoundProgressEase.prototype.easeInOutElastic = function (t, b, c, d) {
        var s = 1.70158;
        var p = d * (0.3 * 1.5);
        var a = c;
        if (t == 0) {
            return b;
        }
        if ((t /= d / 2) == 2) {
            return b + c;
        }
        if (a < Math.abs(c)) {
            a = c;
            s = p / 4;
        }
        else {
            s = p / (2 * Math.PI) * Math.asin(c / a);
        }
        if (t < 1) {
            return -0.5 * (a * Math.pow(2, 10 * (t -= 1)) *
                Math.sin((t * d - s) * (2 * Math.PI) / p)) + b;
        }
        return a * Math.pow(2, -10 * (t -= 1)) *
            Math.sin((t * d - s) * (2 * Math.PI) / p) * 0.5 + c + b;
    };
    RoundProgressEase.prototype.easeInBack = function (t, b, c, d, s) {
        if (s === void 0) { s = 1.70158; }
        return c * (t /= d) * t * ((s + 1) * t - s) + b;
    };
    RoundProgressEase.prototype.easeOutBack = function (t, b, c, d, s) {
        if (s === void 0) { s = 1.70158; }
        return c * ((t = t / d - 1) * t * ((s + 1) * t + s) + 1) + b;
    };
    RoundProgressEase.prototype.easeInOutBack = function (t, b, c, d, s) {
        if (s === void 0) { s = 1.70158; }
        if ((t /= d / 2) < 1) {
            return c / 2 * (t * t * (((s *= 1.525) + 1) * t - s)) + b;
        }
        return c / 2 * ((t -= 2) * t * (((s *= 1.525) + 1) * t + s) + 2) + b;
    };
    RoundProgressEase.prototype.easeInBounce = function (t, b, c, d) {
        return c - this.easeOutBounce(d - t, 0, c, d) + b;
    };
    RoundProgressEase.prototype.easeOutBounce = function (t, b, c, d) {
        if ((t /= d) < (1 / 2.75)) {
            return c * (7.5625 * t * t) + b;
        }
        else if (t < (2 / 2.75)) {
            return c * (7.5625 * (t -= (1.5 / 2.75)) * t + 0.75) + b;
        }
        else if (t < (2.5 / 2.75)) {
            return c * (7.5625 * (t -= (2.25 / 2.75)) * t + 0.9375) + b;
        }
        return c * (7.5625 * (t -= (2.625 / 2.75)) * t + 0.984375) + b;
    };
    RoundProgressEase.prototype.easeInOutBounce = function (t, b, c, d) {
        if (t < d / 2) {
            return this.easeInBounce(t * 2, 0, c, d) * 0.5 + b;
        }
        return this.easeOutBounce(t * 2 - d, 0, c, d) * 0.5 + c * 0.5 + b;
    };
    RoundProgressEase.ɵfac = function RoundProgressEase_Factory(t) { return new (t || RoundProgressEase)(); };
    RoundProgressEase.ɵprov = i0.ɵɵdefineInjectable({ token: RoundProgressEase, factory: RoundProgressEase.ɵfac });
    return RoundProgressEase;
}());
exports.RoundProgressEase = RoundProgressEase;
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(RoundProgressEase, [{
        type: core_1.Injectable
    }], null, null); })();
//# sourceMappingURL=round-progress.ease.js.map

/***/ }),

/***/ "./node_modules/angular-svg-round-progressbar/dist/round-progress.service.js":
/*!***********************************************************************************!*\
  !*** ./node_modules/angular-svg-round-progressbar/dist/round-progress.service.js ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
exports.RoundProgressService = void 0;
var core_1 = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
var common_1 = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
var i0 = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
var DEGREE_IN_RADIANS = Math.PI / 180;
var RoundProgressService = /** @class */ (function () {
    function RoundProgressService(document) {
        this.supportsSvg = !!(document &&
            document.createElementNS &&
            document.createElementNS('http://www.w3.org/2000/svg', 'svg').createSVGRect);
        this._base = document && document.head.querySelector('base');
        this._hasPerf = typeof window !== 'undefined' &&
            window.performance &&
            window.performance.now &&
            typeof window.performance.now() === 'number';
    }
    /**
     * Resolves a SVG color against the page's `base` tag.
     */
    RoundProgressService.prototype.resolveColor = function (color) {
        if (this._base && this._base.href) {
            var hashIndex = color.indexOf('#');
            if (hashIndex > -1 && color.indexOf('url') > -1) {
                return color.slice(0, hashIndex) + location.href + color.slice(hashIndex);
            }
        }
        return color;
    };
    /**
     * Generates a timestamp.
     */
    RoundProgressService.prototype.getTimestamp = function () {
        return this._hasPerf ? window.performance.now() : Date.now();
    };
    /**
     * Generates the value for an SVG arc.
     * @param current       Current value.
     * @param total         Maximum value.
     * @param pathRadius    Radius of the SVG path.
     * @param elementRadius Radius of the SVG container.
     * @param isSemicircle  Whether the element should be a semicircle.
     */
    RoundProgressService.prototype.getArc = function (current, total, pathRadius, elementRadius, isSemicircle) {
        if (isSemicircle === void 0) { isSemicircle = false; }
        var value = Math.max(0, Math.min(current || 0, total));
        var maxAngle = isSemicircle ? 180 : 359.9999;
        var percentage = total === 0 ? maxAngle : (value / total) * maxAngle;
        var start = this._polarToCartesian(elementRadius, pathRadius, percentage);
        var end = this._polarToCartesian(elementRadius, pathRadius, 0);
        var arcSweep = (percentage <= 180 ? 0 : 1);
        return "M " + start + " A " + pathRadius + " " + pathRadius + " 0 " + arcSweep + " 0 " + end;
    };
    /**
     * Converts polar cooradinates to Cartesian.
     * @param elementRadius  Radius of the wrapper element.
     * @param pathRadius     Radius of the path being described.
     * @param angleInDegrees Degree to be converted.
     */
    RoundProgressService.prototype._polarToCartesian = function (elementRadius, pathRadius, angleInDegrees) {
        var angleInRadians = (angleInDegrees - 90) * DEGREE_IN_RADIANS;
        var x = elementRadius + (pathRadius * Math.cos(angleInRadians));
        var y = elementRadius + (pathRadius * Math.sin(angleInRadians));
        return x + ' ' + y;
    };
    RoundProgressService.ɵfac = function RoundProgressService_Factory(t) { return new (t || RoundProgressService)(i0.ɵɵinject(common_1.DOCUMENT, 8)); };
    RoundProgressService.ɵprov = i0.ɵɵdefineInjectable({ token: RoundProgressService, factory: RoundProgressService.ɵfac });
    return RoundProgressService;
}());
exports.RoundProgressService = RoundProgressService;
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(RoundProgressService, [{
        type: core_1.Injectable
    }], function () { return [{ type: undefined, decorators: [{
                type: core_1.Optional
            }, {
                type: core_1.Inject,
                args: [common_1.DOCUMENT]
            }] }]; }, null); })();
//# sourceMappingURL=round-progress.service.js.map

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/home/home.page.html":
/*!***************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/home/home.page.html ***!
  \***************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<!--<ion-header [translucent]=\"true\">\n  <ion-toolbar>\n    <ion-title>\n      New\n    </ion-title>\n  </ion-toolbar>\n</ion-header>-->\n\n<ion-content [fullscreen]=\"true\" class=\"manageBackground\" (click)=\"tapSim()\">\n  <!--<ion-header collapse=\"condense\">\n    <ion-toolbar>\n      <ion-title size=\"large\">New</ion-title>\n    </ion-toolbar>\n  </ion-header>-->\n\n  <!--<div id=\"container\">\n    <strong>Ready to create an app?</strong>\n    <p>Start with Ionic <a target=\"_blank\" rel=\"noopener noreferrer\" href=\"https://ionicframework.com/docs/components\">UI Components</a></p>\n  </div>-->\n<div class=\"myClass\">\n  <div id=\"container\">\n      <round-progress [current]=\"current\" [max]=\"360\" [duration]=\"duration\" [animation]=\"'linearEase'\" [clockwise]=\"true\"></round-progress>\n  </div>\n</div>\n  <!--<circle-progress [percent]=\"85\" [radius]=\"100\" [outerStrokeWidth]=\"16\" [innerStrokeWidth]=\"8\" [outerStrokeColor]=\"'#78C000'\" [innerStrokeColor]=\"'#C7E596'\" [animation]=\"true\" [animationDuration]=\"300\"></circle-progress>-->\n</ion-content>\n");

/***/ }),

/***/ "./src/app/home/home-routing.module.ts":
/*!*********************************************!*\
  !*** ./src/app/home/home-routing.module.ts ***!
  \*********************************************/
/*! exports provided: HomePageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePageRoutingModule", function() { return HomePageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _home_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./home.page */ "./src/app/home/home.page.ts");




const routes = [
    {
        path: '',
        component: _home_page__WEBPACK_IMPORTED_MODULE_3__["HomePage"],
    }
];
let HomePageRoutingModule = class HomePageRoutingModule {
};
HomePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], HomePageRoutingModule);



/***/ }),

/***/ "./src/app/home/home.module.ts":
/*!*************************************!*\
  !*** ./src/app/home/home.module.ts ***!
  \*************************************/
/*! exports provided: HomePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePageModule", function() { return HomePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _home_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./home.page */ "./src/app/home/home.page.ts");
/* harmony import */ var _home_routing_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./home-routing.module */ "./src/app/home/home-routing.module.ts");
/* harmony import */ var _ionic_native_unique_device_id_ngx__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic-native/unique-device-id/ngx */ "./node_modules/@ionic-native/unique-device-id/__ivy_ngcc__/ngx/index.js");
/* harmony import */ var _ionic_native_android_permissions_ngx__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ionic-native/android-permissions/ngx */ "./node_modules/@ionic-native/android-permissions/__ivy_ngcc__/ngx/index.js");
/* harmony import */ var _ionic_native_device_accounts_ngx__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @ionic-native/device-accounts/ngx */ "./node_modules/@ionic-native/device-accounts/__ivy_ngcc__/ngx/index.js");
/* harmony import */ var _ionic_native_device_ngx__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @ionic-native/device/ngx */ "./node_modules/@ionic-native/device/__ivy_ngcc__/ngx/index.js");
/* harmony import */ var angular_svg_round_progressbar__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! angular-svg-round-progressbar */ "./node_modules/angular-svg-round-progressbar/dist/index.js");
/* harmony import */ var angular_svg_round_progressbar__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/__webpack_require__.n(angular_svg_round_progressbar__WEBPACK_IMPORTED_MODULE_11__);
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");













let HomePageModule = class HomePageModule {
};
HomePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["IonicModule"],
            _home_routing_module__WEBPACK_IMPORTED_MODULE_6__["HomePageRoutingModule"],
            angular_svg_round_progressbar__WEBPACK_IMPORTED_MODULE_11__["RoundProgressModule"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_12__["HttpClientModule"]
        ],
        declarations: [_home_page__WEBPACK_IMPORTED_MODULE_5__["HomePage"]],
        providers: [
            _ionic_native_unique_device_id_ngx__WEBPACK_IMPORTED_MODULE_7__["UniqueDeviceID"],
            _ionic_native_android_permissions_ngx__WEBPACK_IMPORTED_MODULE_8__["AndroidPermissions"],
            _ionic_native_device_accounts_ngx__WEBPACK_IMPORTED_MODULE_9__["DeviceAccounts"],
            _ionic_native_device_ngx__WEBPACK_IMPORTED_MODULE_10__["Device"],
        ],
    })
], HomePageModule);



/***/ }),

/***/ "./src/app/home/home.page.scss":
/*!*************************************!*\
  !*** ./src/app/home/home.page.scss ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("#container {\n  text-align: center;\n  position: absolute;\n  left: 0;\n  right: 0;\n  top: 50%;\n  transform: translateY(-50%);\n  margin-left: 20%;\n}\n\n#container strong {\n  font-size: 20px;\n  line-height: 26px;\n}\n\n#container p {\n  font-size: 16px;\n  line-height: 22px;\n  color: #8c8c8c;\n  margin: 0;\n}\n\n#container a {\n  text-decoration: none;\n}\n\n.myClass {\n  background-color: black;\n  height: 100%;\n  width: 100%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvaG9tZS9ob21lLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGtCQUFBO0VBRUEsa0JBQUE7RUFDQSxPQUFBO0VBQ0EsUUFBQTtFQUNBLFFBQUE7RUFDQSwyQkFBQTtFQUNBLGdCQUFBO0FBQUY7O0FBR0E7RUFDRSxlQUFBO0VBQ0EsaUJBQUE7QUFBRjs7QUFHQTtFQUNFLGVBQUE7RUFDQSxpQkFBQTtFQUVBLGNBQUE7RUFFQSxTQUFBO0FBRkY7O0FBS0E7RUFDRSxxQkFBQTtBQUZGOztBQUtBO0VBQ0ksdUJBQUE7RUFDQSxZQUFBO0VBQ0EsV0FBQTtBQUZKIiwiZmlsZSI6InNyYy9hcHAvaG9tZS9ob21lLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIiNjb250YWluZXIge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG5cbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBsZWZ0OiAwO1xuICByaWdodDogMDtcbiAgdG9wOiA1MCU7XG4gIHRyYW5zZm9ybTogdHJhbnNsYXRlWSgtNTAlKTtcbiAgbWFyZ2luLWxlZnQ6IDIwJTtcbn1cblxuI2NvbnRhaW5lciBzdHJvbmcge1xuICBmb250LXNpemU6IDIwcHg7XG4gIGxpbmUtaGVpZ2h0OiAyNnB4O1xufVxuXG4jY29udGFpbmVyIHAge1xuICBmb250LXNpemU6IDE2cHg7XG4gIGxpbmUtaGVpZ2h0OiAyMnB4O1xuXG4gIGNvbG9yOiAjOGM4YzhjO1xuXG4gIG1hcmdpbjogMDtcbn1cblxuI2NvbnRhaW5lciBhIHtcbiAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xufVxuXG4ubXlDbGFzcyB7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogYmxhY2s7XG4gICAgaGVpZ2h0OiAxMDAlO1xuICAgIHdpZHRoOiAxMDAlO1xufVxuIl19 */");

/***/ }),

/***/ "./src/app/home/home.page.ts":
/*!***********************************!*\
  !*** ./src/app/home/home.page.ts ***!
  \***********************************/
/*! exports provided: HomePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePage", function() { return HomePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _ionic_native_unique_device_id_ngx__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic-native/unique-device-id/ngx */ "./node_modules/@ionic-native/unique-device-id/__ivy_ngcc__/ngx/index.js");
/* harmony import */ var _ionic_native_android_permissions_ngx__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic-native/android-permissions/ngx */ "./node_modules/@ionic-native/android-permissions/__ivy_ngcc__/ngx/index.js");
/* harmony import */ var _ionic_native_device_accounts_ngx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic-native/device-accounts/ngx */ "./node_modules/@ionic-native/device-accounts/__ivy_ngcc__/ngx/index.js");
/* harmony import */ var _ionic_native_device_ngx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic-native/device/ngx */ "./node_modules/@ionic-native/device/__ivy_ngcc__/ngx/index.js");
/* harmony import */ var angular_svg_round_progressbar__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! angular-svg-round-progressbar */ "./node_modules/angular-svg-round-progressbar/dist/index.js");
/* harmony import */ var angular_svg_round_progressbar__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(angular_svg_round_progressbar__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");









let HomePage = class HomePage {
    constructor(platform, uniqueDeviceID, androidPermissions, deviceAccounts, device, roundModel, http) {
        this.platform = platform;
        this.uniqueDeviceID = uniqueDeviceID;
        this.androidPermissions = androidPermissions;
        this.deviceAccounts = deviceAccounts;
        this.device = device;
        this.roundModel = roundModel;
        this.http = http;
        if (this.platform.is('android')) {
            this.current = "1";
            this.duration = "1";
            this.continueCheck = 1;
            this.beUrl = "https://colrai.herokuapp.com/";
            this.getPermission();
            //setTimeout(function() {
            //  this.incrementCounter(1);
            //}, 10);
            setTimeout(() => {
                // somecode
                this.incrementCounter(1);
            }, 10);
        }
    }
    getDeviceName() {
        this.DeviceName = this.device.model;
        console.log(this.DeviceName);
        this.registerDevice();
    }
    incrementCounter(amount) {
        this.current = parseInt(this.current) + amount;
        if ((parseInt(this.current) + amount) == 361) {
            this.current = "1";
        }
        if (this.continueCheck == 1) {
            setTimeout(() => {
                // somecode
                this.incrementCounter(1);
            }, 10);
        }
        //  this.roundModel.current +=amount;
    }
    getUniqueDeviceID() {
        this.uniqueDeviceID.get()
            .then((uuid) => {
            this.UniqueDeviceID = uuid;
            console.log(this.UniqueDeviceID);
            this.requestAccountPermission();
        })
            .catch((error) => {
            console.log(error);
            this.UniqueDeviceID = "Error! ${error}";
            return false;
        });
    }
    getAccounts() {
        this.deviceAccounts.get().then(accounts => {
            //this.deviceAccounts.getByType("EMAIL").then(email =>{
            //})
            console.log(accounts[0].name);
            this.DeviceEmailId = accounts[0].name;
            this.getDeviceName();
        }).catch(error => console.error(error));
    }
    getAccountsPermission() {
        this.deviceAccounts.getPermissions().then(onSuccess => {
            this.getAccounts();
        }).catch(error => {
            this.DeviceEmailId = "NOT PERMITTED";
        });
    }
    getPermission() {
        /*this.androidPermissions.checkPermission(
          this.androidPermissions.PERMISSION.READ_PHONE_STATE
        ).then(res => {
          if (res.hasPermission) {
            this.getUniqueDeviceID();
    
          } else {
            this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.READ_PHONE_STATE).then(res => {
              //alert("Persmission Granted Please Restart App!");
              this.getUniqueDeviceID();
            }).catch(error => {
              alert("Error! " + error);
            });
          }
        }).catch(error => {
          alert("Error! " + error);
        });*/
        this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.READ_PHONE_STATE).then(res => {
            //alert("Persmission Granted Please Restart App!");
            this.getUniqueDeviceID();
        }).catch(error => {
            alert("Error! " + error);
        });
    }
    requestAccountPermission() {
        let androidV = this.device.version;
        console.log(androidV);
        if (parseInt(androidV) >= 6) {
            return this.androidPermissions.requestPermissions([
                this.androidPermissions.PERMISSION.GET_ACCOUNTS,
            ])
                .then(permission => {
                if (permission.hasPermission) {
                    this.getAccountsPermission();
                }
            })
                .catch(error => console.error(error));
        }
        else {
            return this.androidPermissions.requestPermissions([
                this.androidPermissions.PERMISSION.GET_ACCOUNTS,
            ])
                .then(permission => {
                if (permission.hasPermission) {
                    this.getAccounts();
                }
            })
                .catch(error => console.error(error));
        }
    }
    tapSim() {
        if (this.continueCheck == 1) {
            this.continueCheck = 0;
            this.saveTrackingValue();
            console.log(this.current);
        }
        else {
            this.continueCheck = 1;
            setTimeout(() => {
                // somecode
                this.incrementCounter(1);
            }, 10);
        }
    }
    registerDevice() {
        let postData = {
            "deviceKey": this.UniqueDeviceID,
            "email": this.DeviceEmailId,
            "deviceName": this.DeviceName,
            "deviceType": "ANDROID"
        };
        const headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_8__["HttpHeaders"]({
            "Content-Type": 'application/json'
        });
        //headers.append("Accept", 'application/json');
        //headers.append();
        //    const requestOptions = new RequestOptions({ headers: headers });
        this.http.post(this.beUrl + "registerdevice/", JSON.stringify(postData), { headers })
            .subscribe(data => {
            console.log(data['_body']);
        }, error => {
            console.log(error);
        });
    }
    saveTrackingValue() {
        let postData = {
            "deviceKey": this.UniqueDeviceID,
            "email": this.DeviceEmailId,
            "deviceName": this.DeviceName,
            "deviceType": "ANDROID",
            "trackingValue": this.current
        };
        const headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_8__["HttpHeaders"]({
            "Content-Type": 'application/json'
        });
        //headers.append("Accept", 'application/json');
        //headers.append();
        //    const requestOptions = new RequestOptions({ headers: headers });
        this.http.post(this.beUrl + "insertvalues/", JSON.stringify(postData), { headers })
            .subscribe(data => {
            console.log(data['_body']);
        }, error => {
            console.log(error);
        });
    }
};
HomePage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"] },
    { type: _ionic_native_unique_device_id_ngx__WEBPACK_IMPORTED_MODULE_3__["UniqueDeviceID"] },
    { type: _ionic_native_android_permissions_ngx__WEBPACK_IMPORTED_MODULE_4__["AndroidPermissions"] },
    { type: _ionic_native_device_accounts_ngx__WEBPACK_IMPORTED_MODULE_5__["DeviceAccounts"] },
    { type: _ionic_native_device_ngx__WEBPACK_IMPORTED_MODULE_6__["Device"] },
    { type: angular_svg_round_progressbar__WEBPACK_IMPORTED_MODULE_7__["RoundProgressModule"] },
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_8__["HttpClient"] }
];
HomePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-home',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./home.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/home/home.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./home.page.scss */ "./src/app/home/home.page.scss")).default]
    })
], HomePage);



/***/ })

}]);
//# sourceMappingURL=home-home-module-es2015.js.map