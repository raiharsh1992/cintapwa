import { Component } from '@angular/core';
import { Platform } from '@ionic/angular';

import { UniqueDeviceID } from '@ionic-native/unique-device-id/ngx';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';

import { DeviceAccounts } from '@ionic-native/device-accounts/ngx';
import { Device } from '@ionic-native/device/ngx';

import { RoundProgressModule } from 'angular-svg-round-progressbar';

import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  UniqueDeviceID: string;
  DeviceEmailId: string;
  DeviceName: string;
  current: string;
  duration: string;
  continueCheck: number;
  beUrl: string;
  constructor(
    public platform: Platform,
    private uniqueDeviceID: UniqueDeviceID,
    private androidPermissions: AndroidPermissions,
    private deviceAccounts: DeviceAccounts,
    private device: Device,
    private roundModel: RoundProgressModule,
    public http: HttpClient
  ) {
    if (this.platform.is('android')) {
      this.current = "1";
      this.duration = "1";
      this.continueCheck = 1
      this.beUrl = "https://colrai.herokuapp.com/";
      this.getPermission();
      //setTimeout(function() {
      //  this.incrementCounter(1);
      //}, 10);
      setTimeout(() => {
        // somecode
        this.incrementCounter(1);
      }, 10);

    }
  }

  getDeviceName() {
    this.DeviceName = this.device.model;
    console.log(this.DeviceName);
    this.registerDevice();
  }

  incrementCounter(amount) {
    this.current = parseInt(this.current) + amount;
    if ((parseInt(this.current) + amount) == 361) {
      this.current = "1";
    }
    if (this.continueCheck == 1) {
      setTimeout(() => {
        // somecode
        this.incrementCounter(1);
      }, 10);
    }
    //  this.roundModel.current +=amount;
  }

  getUniqueDeviceID() {
    this.uniqueDeviceID.get()
      .then((uuid: any) => {
        this.UniqueDeviceID = uuid;
        console.log(this.UniqueDeviceID);
        this.requestAccountPermission();
      })
      .catch((error: any) => {
        console.log(error);
        this.UniqueDeviceID = "Error! ${error}";
        return false;
      });
  }

  getAccounts() {
    this.deviceAccounts.get().then(accounts => {
      //this.deviceAccounts.getByType("EMAIL").then(email =>{

      //})
      console.log(accounts[0].name);
      this.DeviceEmailId = accounts[0].name;
      this.getDeviceName();
    }).catch(error => console.error(error));
  }

  getAccountsPermission(){
      this.deviceAccounts.getPermissions().then(onSuccess =>{
        this.getAccounts()
      }).catch(error => {
        this.DeviceEmailId = "NOT PERMITTED"
      });
  }

  getPermission() {
    /*this.androidPermissions.checkPermission(
      this.androidPermissions.PERMISSION.READ_PHONE_STATE
    ).then(res => {
      if (res.hasPermission) {
        this.getUniqueDeviceID();

      } else {
        this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.READ_PHONE_STATE).then(res => {
          //alert("Persmission Granted Please Restart App!");
          this.getUniqueDeviceID();
        }).catch(error => {
          alert("Error! " + error);
        });
      }
    }).catch(error => {
      alert("Error! " + error);
    });*/

    this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.READ_PHONE_STATE).then(res => {
      //alert("Persmission Granted Please Restart App!");
      this.getUniqueDeviceID();
    }).catch(error => {
      alert("Error! " + error);
    });
  }

  requestAccountPermission() {
    let androidV = this.device.version;
    console.log(androidV);

    if(parseInt(androidV)>=6){
      return this.androidPermissions.requestPermissions(
        [
          this.androidPermissions.PERMISSION.GET_ACCOUNTS,
          //this.androidPermissions.PERMISSION.GET_ACCOUNTS_PRIVILEGED
        ])
        .then(permission => {
          if (permission.hasPermission) {
            this.getAccountsPermission();
          }
        })
        .catch(error => console.error(error));
    }
    else{
      return this.androidPermissions.requestPermissions(
        [
          this.androidPermissions.PERMISSION.GET_ACCOUNTS,
          //this.androidPermissions.PERMISSION.GET_ACCOUNTS_PRIVILEGED
        ])
        .then(permission => {
          if (permission.hasPermission) {
            this.getAccounts();
          }
        })
        .catch(error => console.error(error));
    }
  }

  tapSim() {
    if (this.continueCheck == 1) {
      this.continueCheck = 0;
      this.saveTrackingValue();
      console.log(this.current)
    }
    else {
      this.continueCheck = 1;
      setTimeout(() => {
        // somecode
        this.incrementCounter(1);
      }, 10);
    }
  }

  registerDevice() {
    let postData = {
      "deviceKey": this.UniqueDeviceID,
      "email": this.DeviceEmailId,
      "deviceName": this.DeviceName,
      "deviceType": "ANDROID"
    }
    const headers = new HttpHeaders({
      "Content-Type":'application/json'
    });
    //headers.append("Accept", 'application/json');
    //headers.append();
//    const requestOptions = new RequestOptions({ headers: headers });

    this.http.post(this.beUrl + "registerdevice/", JSON.stringify(postData), {headers})
      .subscribe(data => {
        console.log(data['_body']);
      }, error => {
        console.log(error);
      });
  }

  saveTrackingValue() {
    let postData = {
      "deviceKey": this.UniqueDeviceID,
      "email": this.DeviceEmailId,
      "deviceName": this.DeviceName,
      "deviceType": "ANDROID",
      "trackingValue": this.current
    }
    const headers = new HttpHeaders({
      "Content-Type":'application/json'
    });
    //headers.append("Accept", 'application/json');
    //headers.append();
//    const requestOptions = new RequestOptions({ headers: headers });

    this.http.post(this.beUrl + "insertvalues/", JSON.stringify(postData), {headers})
      .subscribe(data => {
        console.log(data['_body']);
      }, error => {
        console.log(error);
      });
  }

}
