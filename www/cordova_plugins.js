cordova.define('cordova/plugin_list', function(require, exports, module) {
  module.exports = [
    {
      "id": "cordova-plugin-statusbar.statusbar",
      "file": "plugins/cordova-plugin-statusbar/www/statusbar.js",
      "pluginId": "cordova-plugin-statusbar",
      "clobbers": [
        "window.StatusBar"
      ]
    },
    {
      "id": "cordova-plugin-device.device",
      "file": "plugins/cordova-plugin-device/www/device.js",
      "pluginId": "cordova-plugin-device",
      "clobbers": [
        "device"
      ]
    },
    {
      "id": "cordova-plugin-splashscreen.SplashScreen",
      "file": "plugins/cordova-plugin-splashscreen/www/splashscreen.js",
      "pluginId": "cordova-plugin-splashscreen",
      "clobbers": [
        "navigator.splashscreen"
      ]
    },
    {
      "id": "cordova-plugin-ionic-webview.IonicWebView",
      "file": "plugins/cordova-plugin-ionic-webview/src/www/util.js",
      "pluginId": "cordova-plugin-ionic-webview",
      "clobbers": [
        "Ionic.WebView"
      ]
    },
    {
      "id": "cordova-plugin-ionic-keyboard.keyboard",
      "file": "plugins/cordova-plugin-ionic-keyboard/www/android/keyboard.js",
      "pluginId": "cordova-plugin-ionic-keyboard",
      "clobbers": [
        "window.Keyboard"
      ]
    },
    {
      "id": "cordova-plugin-unique-device-id2.UniqueDeviceID",
      "file": "plugins/cordova-plugin-unique-device-id2/www/uniqueid.js",
      "pluginId": "cordova-plugin-unique-device-id2",
      "merges": [
        "window.plugins.uniqueDeviceID"
      ]
    },
    {
      "id": "cordova-plugin-dreamover-uid.uid",
      "file": "plugins/cordova-plugin-dreamover-uid/www/uid.js",
      "pluginId": "cordova-plugin-dreamover-uid",
      "clobbers": [
        "cordova.plugins.uid"
      ]
    },
    {
      "id": "cordova-plugin-android-permissions.Permissions",
      "file": "plugins/cordova-plugin-android-permissions/www/permissions.js",
      "pluginId": "cordova-plugin-android-permissions",
      "clobbers": [
        "cordova.plugins.permissions"
      ]
    },
    {
      "id": "com.danielsogl.cordova.deviceaccounts.DeviceAccounts",
      "file": "plugins/com.danielsogl.cordova.deviceaccounts/www/DeviceAccounts.js",
      "pluginId": "com.danielsogl.cordova.deviceaccounts",
      "clobbers": [
        "plugins.DeviceAccounts"
      ]
    },
    {
      "id": "cordova-device-accounts-v2.DeviceAccounts",
      "file": "plugins/cordova-device-accounts-v2/www/DeviceAccounts.js",
      "pluginId": "cordova-device-accounts-v2",
      "clobbers": [
        "DeviceAccounts"
      ]
    }
  ];
  module.exports.metadata = {
    "cordova-plugin-whitelist": "1.3.3",
    "cordova-plugin-statusbar": "2.4.2",
    "cordova-plugin-device": "2.0.2",
    "cordova-plugin-splashscreen": "5.0.2",
    "cordova-plugin-ionic-webview": "4.2.1",
    "cordova-plugin-ionic-keyboard": "2.2.0",
    "cordova-plugin-unique-device-id2": "2.0.0",
    "cordova-plugin-dreamover-uid": "1.3.0",
    "cordova-plugin-android-permissions": "1.0.2",
    "com.danielsogl.cordova.deviceaccounts": "1.0.0",
    "cordova-device-accounts-v2": "2.0.8"
  };
});